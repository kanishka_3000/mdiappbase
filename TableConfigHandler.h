/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef TABLECONFIGHANDLER_H
#define TABLECONFIGHANDLER_H

#include <QString>
#include <QFile>
#include <QDomNode>
#include <QDomElement>
#include <QMap>
#include <QVector>
#include <memory>
#include <Logger.h>

class FieldConfig
{
public:
    enum Field_Editability
    {
        NOT_EDITABLE,
        CHECk_BOX,
        SPIN_BOX
    };
    QString s_FiledName;
    QString s_DisplayName;
    int i_TmpColumn;
    int i_Column;
    bool b_IsVisibl;
    Field_Editability e_Editability;
};

class WindowConfig
{
public:\

    WindowConfig();
    void Init(QDomElement oInit);
    int GetFieldCount() const {return i_FieldCount;}
    QString sWindowName;
    std::shared_ptr<FieldConfig> GetFieldConfig(QString sFieldName) const;
    std::shared_ptr<FieldConfig> GetFieldConfig(int iIndex) const;
    QMap<QString, std::shared_ptr<FieldConfig>> map_Fields;

    QVector<std::shared_ptr<FieldConfig>> vec_VisibleFields;
private:
    int i_FieldCount;
};


class TableConfigHandler
{
public:
    TableConfigHandler();

    void Initialize(QString sFile);
    WindowConfig GetWindowConfig(QString sWndID);
    void SetLogger(Logger* pCallback){p_Logger = pCallback;}
protected:

    QMap<QString,WindowConfig> map_Doms;
    Logger* p_Logger;
};

#endif // TABLECONFIGHANDLER_H
