/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "TableStore.h"
#include <Connection.h>
#include <QSqlQuery>
TableStore::TableStore(QObject *parent):
    QObject(parent)
{

}

void TableStore::AddTable(QString sTableName)
{
    if(map_Tables.contains(sTableName))
    {
        map_Tables.value(sTableName)->select();
        return;
    }

    QSqlTableModel* pTable = new QSqlTableModel(this,*Connection::GetInstance()->GetPrimaryDb());
    pTable->setTable(sTableName);
    pTable->select();
    map_Tables.insert(sTableName,pTable);
}

QSqlTableModel *TableStore::GetTableModel(QString sTableName)
{
    AddTable(sTableName);
    return map_Tables.value(sTableName);
}

