/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef STRINGDATAPROCESSOR_H
#define STRINGDATAPROCESSOR_H
#include <QMap>
#include <QVariant>
namespace feature{
class StringDataProcessorDataSource
{
public:
    virtual QMap<QString, QVariant>& GetData() const= 0;
};

class StringDataProcessor
{
public:
    StringDataProcessor();
    void ProcessString(QString& sProcessString);
    void Bind(const StringDataProcessorDataSource& rSource);
private:
    QMap<QString, QString> map_Params;
};
}
#endif // STRINGDATAPROCESSOR_H
