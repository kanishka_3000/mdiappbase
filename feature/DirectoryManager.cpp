/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "DirectoryManager.h"

feature::DirectoryManager::DirectoryManager(QString sRoot, QString sBaseDir, QStringList sSubDirs, QObject *parent) :
    QObject(parent),s_Root(sRoot), s_BaseDir(sBaseDir), s_SubDirs(sSubDirs)
{
    s_BaseDir = s_Root += QString(QDir::separator()) += s_BaseDir;

    QStringList sTmpList;
    foreach(QString sDir, sSubDirs)
    {
        QString sSubDir = s_BaseDir + QString(QDir::separator()) + sDir;
        sTmpList.push_back(sSubDir);
    }
    s_SubDirs = sTmpList;
}

bool feature::DirectoryManager::Exists(QString sDir)
{
    QDir oDir(s_BaseDir + QString(QDir::separator()) + sDir);

    return oDir.exists();
}

bool feature::DirectoryManager::Create()
{
    QDir oDir(s_BaseDir);
    if(oDir.exists())
        return false;

    bool bSucecss = true;
    foreach(QString sDir, s_SubDirs)
    {
        bool bComplete = oDir.mkpath(sDir);
        if(!bComplete)
            bSucecss = false;
    }

    return bSucecss;
}

//void feature::DirectoryManager::CopyAll(QString sSource, QString sDestination)
//{

//}

bool feature::DirectoryManager::Open(QString sSource)
{
    if(!Exists(sSource))
    {
        return false;
    }
    QString sPath = /*QString("file://") +*/ s_BaseDir + QString(QDir::separator()) + sSource;
    return QDesktopServices::openUrl(QUrl(sPath));
}

