/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTITYSTRINGLISTSOURCEADAPTER_H
#define ENTITYSTRINGLISTSOURCEADAPTER_H
#include <entity/Entity.h>
#include <feature/StringDataProcessor.h>
namespace feature
{
class EntityStringListSourceAdapter:public feature::StringDataProcessorDataSource
{
public:
    EntityStringListSourceAdapter(std::shared_ptr<Entity> pEntity);

private:
    std::shared_ptr<Entity> p_Entity;

    // StringDataProcessorDataSource interface
public:
    virtual QMap<QString, QVariant> &GetData() const;
};
}
#endif // ENTITYSTRINGLISTSOURCEADAPTER_H
