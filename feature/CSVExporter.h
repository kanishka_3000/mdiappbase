/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef CSVEXPORTER_H
#define CSVEXPORTER_H

#include <QFile>
#include <QStringList>
#include <memory>
namespace feature
{

class CSVExporter
{
public:
    class CSVDataRow
    {
    public:
        virtual ~CSVDataRow(){}
        virtual QString GetData(int iColumn) = 0;
    };

    class CSVDataSource
    {
    public:
        virtual ~CSVDataSource(){}
        virtual QStringList GetHeader() const= 0;
        virtual int GetItemCount() const = 0;
        virtual std::shared_ptr<CSVDataRow> GetDataRow(int iRow) const= 0;
    };
public:
    CSVExporter();
    virtual ~CSVExporter();
    void Init( CSVDataSource* pDataSource);
    QString Export(QString sPath, QString sFileName);

private:
    QString GetFileLine(QStringList& sList);
    void ConvertToStringList(std::shared_ptr<CSVDataRow> pRow, int iColumnCount, QStringList& sStringList);
    CSVDataSource* p_DataSource;

};
}
#endif // CSVEXPORTER_H
