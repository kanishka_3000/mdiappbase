/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef DIRECTORYMANAGER_H
#define DIRECTORYMANAGER_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
namespace feature
{
class DirectoryManager : public QObject
{
    Q_OBJECT
public:
    explicit DirectoryManager(QString sRoot, QString sBaseDir, QStringList sSubDirs, QObject *parent = 0);
    //Checks if base directory exists
    bool Exists(QString sDir);
    bool Create();
    //void CopyAll(QString sSource, QString sDestination);
    bool Open(QString sSource);
signals:

public slots:
private:
    QString s_Root;
    QString s_BaseDir;
    QStringList s_SubDirs;

};
}
#endif // DIRECTORYMANAGER_H
