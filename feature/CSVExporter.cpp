/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "CSVExporter.h"
#include <QDir>
feature::CSVExporter::CSVExporter()
{
    p_DataSource = nullptr;

}

feature::CSVExporter::~CSVExporter()
{

}

void feature::CSVExporter::Init(feature::CSVExporter::CSVDataSource *pDataSource)
{
    p_DataSource = pDataSource;
}

QString feature::CSVExporter::Export(QString sPath, QString sFileName)
{
    Q_ASSERT(p_DataSource != nullptr);
    QString sFile = sPath + QString(QDir::separator()) + sFileName;
    sFile+=".csv";
    QFile oFile(sFile);
    if(!oFile.open(QIODevice::ReadWrite))
    {
        return "";
    }
    QStringList sHeader = p_DataSource->GetHeader();
    QString sRow = GetFileLine(sHeader);

    oFile.write(qPrintable(sRow), sRow.size());

    int iCount = p_DataSource->GetItemCount();
    int iColumnCount = sHeader.count();
    for(int iRow = 0; iRow < iCount ; iRow++)
    {
        std::shared_ptr<CSVDataRow> pRow = p_DataSource->GetDataRow(iRow);
        QStringList sStringList;
        ConvertToStringList(pRow, iColumnCount, sStringList);
        sRow = GetFileLine(sStringList);
        oFile.write(qPrintable(sRow), sRow.size());
    }
    oFile.flush();
    return sFile;
}

QString feature::CSVExporter::GetFileLine(QStringList &sList)
{
    QString sOutput;
    int iSize  = sList.size();
    int iCurrent  = 0;
    foreach(QString sStr, sList)
    {
        sOutput += (QString("\"") + sStr +  QString("\""));
        iCurrent++;
        if(iCurrent == iSize)
        {
            sOutput+="\n";
        }
        else
        {
            sOutput+=",";
        }
    }
    return sOutput;
}

void feature::CSVExporter::ConvertToStringList(std::shared_ptr<feature::CSVExporter::CSVDataRow> pRow,
                                               int iColumnCount, QStringList &sStringList)
{
    for(int i = 0; i < iColumnCount; i++)
    {
        QString sRow = pRow->GetData(i);
        sStringList.push_back(sRow);
    }
}

