/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "EntityStringListSourceAdapter.h"

feature::EntityStringListSourceAdapter::EntityStringListSourceAdapter(std::shared_ptr<Entity> pEntity)
{
    p_Entity = pEntity;
}



QMap<QString, QVariant> &feature::EntityStringListSourceAdapter::GetData() const
{
    return p_Entity->map_Data;
}
