/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "StringDataProcessor.h"

feature::StringDataProcessor::StringDataProcessor()
{

}

void feature::StringDataProcessor::ProcessString(QString &sProcessString)
{
    for(QMap<QString, QString>::iterator itr = map_Params.begin(); itr != map_Params.end(); itr++)
    {
        QString sKeyStr = itr.key();
        sKeyStr = QString("@%1").arg(sKeyStr);
        QString sValue = itr.value();
        sProcessString.replace(sKeyStr , sValue, Qt::CaseInsensitive);
    }
}

void feature::StringDataProcessor::Bind(const StringDataProcessorDataSource &rSource)
{
    QMap<QString, QVariant>& mapData = rSource.GetData();
    for(QMap<QString, QVariant>::iterator itr = mapData.begin(); itr != mapData.end(); itr++)
    {
        map_Params.insert(itr.key(), itr.value().toString());
    }
}

