/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef TABLESTORE_H
#define TABLESTORE_H
#include <QObject>
#include <QSqlTableModel>
#include <QMap>
class TableStore: public QObject
{

    Q_OBJECT
public:
    TableStore(QObject* parent);
    void AddTable(QString sTableName);
    QSqlTableModel *GetTableModel(QString sTableName);
private:
    QMap<QString, QSqlTableModel*> map_Tables;

};

#endif // TABLESTORE_H
