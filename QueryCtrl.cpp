#include "QueryCtrl.h"
#include <Connection.h>
#include <QDebug>
#include <Application.h>
#include <GenericWnd.h>
#include <QSqlRecord>
#include <globaldefs.h>
#include <qtrpt.h>
#include <QSqlQuery>
#include <entity/Entity.h>
#include <QMessageBox>
#include <QSqlError>
#include <QMenu>
#include <Util.h>
#include <QFileDialog>
//*************************************************************************************
QueryCtrl::QueryCtrl(QWidget *parent) :
    GenericCtrl(parent),b_EnableDelete(true),b_EnableEdit(true),b_EnableNew(true)
{
    setupUi(this);
    p_Model = NULL;
    b_EnableDelete = false;
    b_EnableExport = false;
    b_EnableEdit = true;
    b_EnableNew = true;
    b_EnableReload = true;
    b_AutoSelect = true;
    p_CommandHanlder = nullptr;
    b_Summery = false;
    s_OrderColumn = "";
    p_QueryDataSource = nullptr;
}
//*************************************************************************************
QueryCtrl::~QueryCtrl()
{
    delete p_QueryDataSource;
}
//*************************************************************************************
void QueryCtrl::HandleCustomCommands()
{
    int iCommandCount = p_CommandHanlder->GetCommandCount();
    for(int i = 0 ; i < iCommandCount; i++)
    {
        QPushButton* pPushButton = new QPushButton(this);
        p_CommandHanlder->FixButton(i, pPushButton);
        p_ButtonArray->layout()->addWidget(pPushButton);
    }
    QString sText;
    p_CommandHanlder->UpdateEditCommandName(sText);
    if((!sText.isEmpty()) && (sText != btn_Edit->text()))
    {
        btn_Edit->setText(sText);
    }
    sText = "";
    p_CommandHanlder->UpdateNewCommandName(sText);
    if((!sText.isEmpty()) && (sText != btn_New->text()))
    {
        btn_New->setText(sText);
    }
}

void QueryCtrl::AdjustCommandVisiblity()
{
    btn_Delete->setVisible(b_EnableDelete);
    btn_Edit->setVisible(b_EnableEdit);
    btn_New->setVisible(b_EnableNew);
    p_ActDelete->setVisible(b_EnableDelete);
    p_ActEdit->setVisible(b_EnableEdit);

    //Export is disabled until capability is complete
    p_ActExport->setVisible(b_EnableExport);
    btn_Export->setVisible(b_EnableExport);
    btn_Reload->setVisible(b_EnableReload);
}

void QueryCtrl::Initialize(bool bRelationship)
{
    b_RelationshipQuery = bRelationship;
    p_Logger->WriteLog("QueryCtrl::Initialize Relation ship = %d, TableName = %s ", b_RelationshipQuery, qPrintable(s_Table));
    if(!b_RelationshipQuery)
    {
        p_Model = new QSqlTableModel(this,*Connection::GetInstance()->GetPrimaryDb());
        ((QSqlTableModel*)p_Model)->setTable(s_Table);

        if(b_AutoSelect)
            ((QSqlTableModel*)p_Model)->select();
        //qDebug() <<p_Model->query().lastError().text();
        //qDebug() << "success" << bSuccess;
    }
    else
    {
        p_Model = new QSqlQueryModel(this);
        //Build relationship query
    }
    TableConfigHandler* pConfi = Application::GetApplication()->GetTableConfigHander();
    p_Logger->WriteLog("Config name for query control %s", qPrintable(s_ConfigName));
    QString sWindowConfig;
    if(!s_ConfigName.isEmpty())
    {
        sWindowConfig = s_ConfigName;
    }
    else
    {
        sWindowConfig = p_Parent->s_Title;
    }
    o_Config = pConfi->GetWindowConfig(sWindowConfig);
    p_Logger->WriteLog("Field count of config %d", o_Config.GetFieldCount());
    p_TableView->setModel(p_Model);


    p_TableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    int iColumnCount = p_Model->columnCount();
    p_Logger->WriteLog("Column count of model %d", iColumnCount);
    for(int i =0 ;i < iColumnCount; i++)
    {
        QString sHeader =p_Model->headerData(i,Qt::Horizontal).toString();
        p_Logger->WriteLog("Seaching for field for %s", qPrintable(sHeader));
        std::shared_ptr<FieldConfig> pField = o_Config.GetFieldConfig(sHeader);
        if((!s_OrderColumn.isEmpty()) && s_OrderColumn == sHeader)
        {
            p_Model->sort(i);
        }
        if(pField == NULL)
        {
            p_TableView->hideColumn(i);
            continue;
        }
        p_Logger->WriteLog("Field Displayname = %s, FieldName =  %s", qPrintable(pField->s_DisplayName), qPrintable(pField->s_FiledName));
        pField->i_TmpColumn = i;
        if(!pField->b_IsVisibl)
        {
            //This is redundant since we are removing the columns that are not
            //available in the table config itself.
             p_TableView->hideColumn(i);
             continue;
        }
        p_Model->setHeaderData(i, Qt::Horizontal, pField->s_DisplayName);
    }



    AdjustCommandVisiblity();
    if(p_CommandHanlder)
    {
        HandleCustomCommands();
    }
    AdjustWidgetWidthToSame<QPushButton>(p_ButtonArray);
    p_QueryDataSource = new QueryDataSource(p_Model);


}

void QueryCtrl::SetRelationship(QString sEntity1, QString sEntity2, QString sKeyValue)
{
    QString sQuery = Entity::BuildRelationshipQuery(sEntity1,sEntity2,sKeyValue);
    p_Model->setQuery(sQuery,*Connection::GetInstance()->GetPrimaryDb());

}

//*************************************************************************************
void QueryCtrl::OnCreateCtrl()
{
    connect(p_TableView,SIGNAL(doubleClicked(QModelIndex)),this, SLOT(OnDoubleClicked(QModelIndex)));
    Initialize();

}
//*************************************************************************************
void QueryCtrl::OnContextMenuEdit()
{

    QString sValue = GetSelectedKey();
    if(sValue.isEmpty())
    {
        QMessageBox::warning(this,tr("Invalid"),tr("Invalid Selection Problem"));
        return;
    }
    NotifyEditRequest(sValue);
}
//*************************************************************************************
void QueryCtrl::OnContextMenuDelete()
{
    QMessageBox::StandardButton eButton =  QMessageBox::question(this,tr("Delete"),tr("Are you sure you want to delete data.? <br/> This operation is not reversible"));
    if(eButton != QMessageBox::Yes)
    {
        return;
    }

    QString sValue = GetSelectedKey();
    Connection::GetInstance()->DeleteRow(s_Table,s_Key,sValue);
    p_Model->query().exec();
}

void QueryCtrl::OnContextMenuExport()
{
    QString sHome = QDir::homePath();
    QString sPath = QFileDialog::getExistingDirectory(this, "Save Export", sHome);
    if(sPath.isEmpty())
    {
        QMessageBox::information(this, "Error", "Select valid path <br/> abort");
        return;
    }
    feature::CSVExporter oExporter;
    oExporter.Init(p_QueryDataSource);

    QDateTime oToday = QDateTime::currentDateTime();
    QString  sExport = oExporter.Export(sPath, QString("LE%1").arg(Util::GetUnique()));
    QMessageBox::information(this, "Export", QString("Exported to %1").arg(sExport));
}

void QueryCtrl::OnContextMenuNew()
{
    emit NotifyNewRequest();
}
//*************************************************************************************
void QueryCtrl::Reload()
{
    ((QSqlTableModel*)p_Model)->select();
}

void QueryCtrl::OnContextMenuReload()
{
     Reload();
}
//*************************************************************************************
void QueryCtrl::GetReportData(const int iRowcount, const QString context, QVariant &vData, const int ival)
{
    qDebug() << iRowcount << "::" << context << "::" << ival;
    if(context == "Report Title")
    {
        vData = p_Parent->windowTitle();
        return;
    }
    (void)ival;
   std::shared_ptr<FieldConfig> pField =  o_Config.GetFieldConfig(context);
   if(!pField)
       return;
   QModelIndex oIdx = p_Model->index(iRowcount, pField->i_TmpColumn);
   vData =  p_Model->data(oIdx);
   return;


}
//*************************************************************************************
void QueryCtrl::ParseFilterCrieteria(QString& sQuery, QList<FilterCrieteria> lstFilters)
{
    bool bFirst = true;
    foreach(FilterCrieteria oCri, lstFilters)
    {
        if(!bFirst)
        {
            sQuery+= " AND ";

        }
         bFirst = false;
       sQuery += std::get<0>(oCri);
       int iVal = std::get<2>(oCri);
       QString sQueryValue = std::get<1>(oCri);
       if(iVal == FILTER_TYPE_LIKE)
       {
           sQuery += QString(" like '%");
       }
       else if(iVal == FILTER_TYPE_BETWEEN)
       {
           sQuery+= QString(" BETWEEN '");
           QStringList sParams = sQueryValue.split("|");
           QString sTemp = QString("%1' AND '%2").arg(sParams.at(0)).arg(sParams.at(1));
           sQuery+=sTemp;
           sQueryValue = "";
       }
       else
       {
           sQuery += QString(" = '");
       }
       sQuery += sQueryValue;
       if(iVal == FILTER_TYPE_LIKE)
       {
           sQuery+=QString("%");
       }
       sQuery += QString("' ");
    }
}

void QueryCtrl::OnFilter(QList<FilterCrieteria> lstFilters)
{

    QString sQuery;
    ParseFilterCrieteria(sQuery, lstFilters);
    p_Parent->GetApplication()->GetLogger()->WriteLog(sQuery);
    //qDebug() << sQuery;
    if(!b_RelationshipQuery)
    {
         ((QSqlTableModel*)p_Model)->setFilter(sQuery);
    }
    if(b_Summery)
    {
        QString sSummeryQuery = QString("select sum(%1) from %2 where %3").arg(s_SummeryField).arg(s_Table).arg(sQuery);
        QVariant oOutput;
        Connection::GetInstance()->GetSummery(sSummeryQuery, oOutput);
        QString sSummery = QString("%1: %2").arg(s_SummeryTitle).arg(oOutput.toString());
        lbl_Summery->setText(sSummery);
    }
}
//*************************************************************************************
void QueryCtrl::OnDoubleClicked(QModelIndex oModelIndex)
{

    int iRow = oModelIndex.row();
     QSqlRecord oRow = p_Model->record(iRow);

     QString sKey = oRow.value(s_Key).toString();
     NotifyDoubleClicked(sKey);

}

void QueryCtrl::SetEnableReload(bool value)
{
    b_EnableReload = value;
    btn_Reload->setVisible(value);
}

void QueryCtrl::SetEnabledSummery(QString sSummeryField, QString sTitle)
{
    b_Summery = true;
    s_SummeryField = sSummeryField;
    s_SummeryTitle = sTitle;
}

void QueryCtrl::SetCommandHanlder(QueryCtrlCommandHandler *value)
{
    p_CommandHanlder = value;
}

void QueryCtrl::SetAutoSelect(bool value)
{
    b_AutoSelect = value;
}

void QueryCtrl::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu oMenu;
    oMenu.addAction(p_ActEdit);

    //oMenu.addAction(p_ActExport);
    if(p_CommandHanlder)
    {
        QString sText;
        p_CommandHanlder->UpdateEditCommandName(sText);
        if((!sText.isEmpty()) && sText != p_ActEdit->text())
        {
            p_ActEdit->setText(sText);
        }

        oMenu.addSeparator();
        int iCommandCount = p_CommandHanlder->GetCommandCount();
        for(int i = 0; i < iCommandCount; i++)
        {
            oMenu.addAction(p_CommandHanlder->GetAction(i));
        }
    }

    oMenu.exec(event->globalPos());
}


void QueryCtrl::SetEnableNew(bool value)
{
    b_EnableNew = value;
}

void QueryCtrl::SetEnableExport(bool value)
{
    b_EnableExport = value;
}
//*************************************************************************************
void QueryCtrl::SetEnableEdit(bool value)
{
    b_EnableEdit = value;
}

//*************************************************************************************
void QueryCtrl::SetEnableDelete(bool value)
{
    b_EnableDelete = value;
}
//*************************************************************************************
QString QueryCtrl::GetSelectedKey()
{
    QItemSelectionModel* pSelecton = p_TableView->selectionModel();
    QModelIndexList oList = pSelecton->selectedRows();
    if(oList.size() != 1)
    {
        p_Logger->WriteLog("Selected item count is not 1");
        return "";
    }
    QModelIndex oIndex = oList.first();
    int iRow = oIndex.row();
    QSqlRecord oRow = p_Model->record(iRow);
    QString sValue = oRow.value(s_Key).toString();

    return sValue;
}

void QueryCtrl::SetConfigName(QString sConfigName)
{
    s_ConfigName = sConfigName;
}
//*************************************************************************************
