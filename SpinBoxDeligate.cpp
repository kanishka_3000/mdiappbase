/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include <SpinBoxDeligate.h>
#include <QSpinBox>

SpinBoxDeligate::SpinBoxDeligate(QObject *parent):
    QStyledItemDelegate(parent)
{

}

QWidget *SpinBoxDeligate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    (void)option;(void)index;
    QSpinBox* pSpinBox = new QSpinBox(parent);
    pSpinBox->setValue(1);
    pSpinBox->setMinimum(0);
    return pSpinBox;
}

void SpinBoxDeligate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int value = index.model()->data(index, Qt::DisplayRole).toInt();

      QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
      spinBox->setValue(value);
}

void SpinBoxDeligate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
     spinBox->interpretText();
     int value = spinBox->value();

     model->setData(index, value, Qt::EditRole);
}

void SpinBoxDeligate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    (void)index;
     editor->setGeometry(option.rect);
}
