/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef QUERYCTRL_H
#define QUERYCTRL_H

#include <QWidget>
#include <ui_QueryCtrl.h>
#include <GenericCtrl.h>
#include <QSqlTableModel>
#include <TableConfigHandler.h>
#include <GenericFilterCtrl.h>
#include <QueryDataSource.h>
class QueryCtrlCommandHandler
{
public:
    virtual int GetCommandCount(){return 0;}
    virtual QAction* GetAction(int iActionNo){(void)iActionNo; return nullptr;}
    virtual void FixButton(int iActionNo, QPushButton *pButton) {(void)iActionNo; (void)pButton;}
    virtual void UpdateEditCommandName(QString& rEditButtnName){(void)rEditButtnName;}
    virtual void UpdateNewCommandName(QString& rNewCommandName){(void)rNewCommandName;}
};

class QueryCtrl : public GenericCtrl,public Ui::QueryCtrl
{
    Q_OBJECT

public:
    explicit QueryCtrl(QWidget *parent = 0);
    ~QueryCtrl();
    virtual void SetTableName(QString sTable){s_Table = sTable;}
    virtual void SetOrderColumn(QString sOrderColumn){s_OrderColumn = sOrderColumn;}
    virtual void SetKeyName(QString sKey){s_Key = sKey;}
    virtual void Initialize(bool bRelationship = false);
    virtual void SetRelationship(QString sEntity1, QString sEntity2, QString sKeyValue);

    virtual void OnCreateCtrl();
    QString GetSelectedKey();
    void SetConfigName(QString sConfigName);

    void SetEnableDelete(bool value);
    void SetEnableEdit(bool value);
    void SetEnableNew(bool value);
    void SetEnableExport(bool value);

    void SetAutoSelect(bool value);
    void SetCommandHanlder(QueryCtrlCommandHandler *value);

    void SetEnableReload(bool value);
    void SetEnabledSummery(QString sSummeryField, QString sTitle);

protected:
    void contextMenuEvent(QContextMenuEvent *event) override;

    void HandleCustomCommands();

    void AdjustCommandVisiblity();
    void ParseFilterCrieteria(QString &sQuery, QList<FilterCrieteria> lstFilters);

public slots:
    virtual void OnContextMenuEdit();
    virtual void OnContextMenuDelete();
    virtual void OnContextMenuExport();
    virtual void OnContextMenuNew();
    virtual void OnContextMenuReload();
    void GetReportData(const int iRowcount, const QString constext, QVariant& vData, const int ival);
    virtual void OnFilter(QList<FilterCrieteria> lstFilters);
    virtual void OnDoubleClicked(QModelIndex oModelIndex);
     void Reload();
signals:
    void NotifyEditRequest(QString sKeyData);
    void NotifyNewRequest();
    void NotifyDoubleClicked(QString sKeyData);
private:
    QString s_Table;
    QString s_Key;
    QSqlQueryModel* p_Model;
    WindowConfig o_Config;
    QString s_OrderColumn;

    QString s_ConfigName;
    bool b_RelationshipQuery;
    bool b_EnableDelete;
    bool b_EnableEdit;
    bool b_EnableNew;
    bool b_EnableExport;
    bool b_EnableReload;
    bool b_AutoSelect;
    bool b_Summery;
    QString s_SummeryField;
    QString s_SummeryTitle;
    QueryCtrlCommandHandler* p_CommandHanlder;
    QueryDataSource* p_QueryDataSource;
};

#endif // QUERYCTRL_H
