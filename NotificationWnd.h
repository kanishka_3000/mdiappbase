/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONWND_H
#define NOTIFICATIONWND_H
#include <GenericWnd.h>
#include <NotificationCtrl.h>
class NotificationWnd: public GenericWnd
{
    Q_OBJECT
public:
    NotificationWnd(MainWnd* pMainWnd, QString sTitle);
    virtual void Initialize(NotificationPopulator* pPopulator);
private:
    NotificationCtrl* p_NotificationCtrl;
};

#endif // NOTIFICATIONWND_H
