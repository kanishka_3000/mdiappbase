#include "MainWnd.h"
#include <GenericWnd.h>
#include <MyTestWindow.h>
#include <Connection.h>
#include <Application.h>
#include <globaldefs.h>
#include <QMessageBox>
#include <QDate>
#include <User.h>
#include <Role.h>
#include <ActionStore.h>
#include <QCloseEvent>
#include <QDesktopServices>
MainWnd::MainWnd(QWidget *parent)
    : QMainWindow(parent)
{
	setupUi(this);
    p_MDIArea->setActivationOrder(QMdiArea::WindowOrder::StackingOrder);

}

MainWnd::~MainWnd()
{
    QApplication::quit();
}

void MainWnd::Initilize(Application *pApplication)
{
    p_Application = pApplication;
    if(p_Application->GetConfig()->value(SHOW_MAXIMIZED).toBool())
    {
        showMaximized();
    }
    std::shared_ptr<Role> pRole = pApplication->GetUser()->p_Role;
    if(!pRole->HasPriviledge(PRI_ADD_USERS))
    {
        p_ActAdd_User->setEnabled(false);
    }
    if(!pRole->HasPriviledge(PRI_MANAGE_USERS))
    {
        p_ActManage_Users->setEnabled(false);
    }

    ActionStore* pActionStore = pApplication->GetActionStore();
    QList<QAction*> lstActions = findChildren<QAction*>();
    foreach(QAction* pAction, lstActions)
    {
        bool bValidForStore = pAction->property(VALID_FOR_ST).toBool();
        if(!bValidForStore)
            continue;

        pActionStore->AddAction(pAction->text(),pAction);
    }
    connect(pAct_Close, SIGNAL(triggered(bool)),this,SLOT(close()));\
    connect(p_ActCascade_Windows, SIGNAL(triggered(bool)), this, SLOT(OnCascadeWindows()));
    connect(p_ActCloseAll, SIGNAL(triggered(bool)),this, SLOT(OnMenuCloseAllWindows()));
    connect(p_ActUser_Guide,SIGNAL(triggered(bool)),this, SLOT(OnUserGuide()));
    QString sTitle = pApplication->GetConfig()->value(ORG_NAME).toString();
    setWindowTitle(pApplication->GetApplicationName() + " - " + sTitle);
    o_About.SetAppName(pApplication->GetApplicationName(), pApplication->GetVersion());
    o_About.setWindowIcon(QIcon(pApplication->GetLoginIcon()));
}


//void MainWnd::OnMainToolBarClicked(QAction* pAction)
//{
//	GenericWnd* pWnd = NULL;


//	if(pWnd)
//	{
//        pWnd->Initialize();//This should be moved to application class for centralization
//		pWnd->show();//Show
//    }
//}

void MainWnd::OnMenuChangePassword()
{
    Application::GetApplication()->CreateWnd(WND_CH_PW);
}

void MainWnd::OnMenuAboutBox()
{

    o_About.show();
}

void MainWnd::OnMenuPreferenceWnd()
{
    Application::GetApplication()->CreateWnd(WND_PREF);
}

void MainWnd::OnMenuAddUser()
{
    Application::GetApplication()->CreateWnd(WND_AD_USR);
}

void MainWnd::OnMenuManageUsers()
{
    Application::GetApplication()->CreateWnd(WND_MG_USERS);
}

void MainWnd::OnCascadeWindows()
{
    p_MDIArea->cascadeSubWindows();
}

void MainWnd::OnMenuCloseAllWindows()
{
    p_MDIArea->closeAllSubWindows();
}

void MainWnd::OnUserGuide()
{
    QString sPath = QString("file:///") + QDir::currentPath() + QString(QDir::separator())+ QString(USERGUIDE_PATH) + QString(QDir::separator()) + "userguide.pdf";
    if(!QDesktopServices::openUrl(QUrl(sPath,  QUrl::TolerantMode)))
    {
        QMessageBox::information(this, tr("ERROR"), tr("No appropriate support application "
                                                       "installed, Please try installing PDF"
                                                       " reader from https://get.adobe.com/reader/"));
    }
}

QMdiSubWindow* MainWnd::_AddSubWindow(GenericWnd* pWnd)
{
	return p_MDIArea->addSubWindow(pWnd);
	
	
}

void MainWnd::_RemoveSubWindow( GenericWnd* pWnd )
{
    p_MDIArea->removeSubWindow(pWnd);
}

void MainWnd::closeEvent(QCloseEvent *event)
{
    if(QMessageBox::Yes == QMessageBox::question(this, tr("Quit"),tr("Are you sure you want to exit?")))
    {
        event->accept();
    }
    else
    {
        event->ignore();
    }
}
