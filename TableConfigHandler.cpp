#include "TableConfigHandler.h"
#include <QDomDocument>
#include <QDebug>
#include <QFileInfo>
#include <iostream>
TableConfigHandler::TableConfigHandler()
    :p_Logger(nullptr)
{

}

void TableConfigHandler::Initialize(QString sFile)
{
    QFile file(sFile);
    QFileInfo oInfo(file);

    if(p_Logger)
        p_Logger->WriteLog("config path %s", qPrintable(oInfo.absoluteFilePath()));

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {

         return ;
    }

    QDomDocument doc;
    QString errorStr;
    int errorLine;
    int errorColumn;
    if (!doc.setContent(&file, false, &errorStr, &errorLine,
                            &errorColumn))
    {
            qDebug() << "Error: Parse error at line " << errorLine << ", ";

            return ;

    }

    QDomElement root = doc.documentElement();
    //qDebug() << root.tagName();
    QDomNodeList  oWindows = root.elementsByTagName("Window");

    int iSize = oWindows.size();
    for(int iCur = 0; iCur < iSize; iCur++)
    {
        QDomElement oChild = oWindows.at(iCur).toElement();
        QString sWindowName = oChild.attribute("name");

        p_Logger->WriteLog("Window Configs %s", qPrintable(sWindowName));
        WindowConfig oWndow;
        oWndow.Init(oChild);

        map_Doms.insert(sWindowName,oWndow);
    }
}

WindowConfig TableConfigHandler::GetWindowConfig(QString sWndID)
{
    return map_Doms.value(sWndID);
}




WindowConfig::WindowConfig()
{
     i_FieldCount =0;
}

void WindowConfig::Init(QDomElement oInit)
{
    QDomNodeList oFields = oInit.elementsByTagName("field");
    int iSize = oFields.size();
    for(int i = 0 ;i < iSize; i++)
    {
        QDomElement oChild = oFields.at(i).toElement();
       // qDebug() << oChild.tagName();
        QString sFieldName = oChild.attribute("name");
        QString sDisplayName = oChild.attribute("display");
        QString sVisible = oChild.attribute("visible");
        QString sEditability = oChild.attribute("editability");

        //qDebug() << sDisplayName << sFieldName<< sVisible;
        std::shared_ptr<FieldConfig> pField(new FieldConfig);
        pField->s_FiledName = sFieldName;
        pField->s_DisplayName = sDisplayName;
        pField->b_IsVisibl = sVisible.toInt();
        pField->i_Column = i;
        pField->e_Editability = static_cast<FieldConfig::Field_Editability>(sEditability.toInt());
        if(pField->b_IsVisibl)
            i_FieldCount++;

        map_Fields.insert(sFieldName,pField);

        vec_VisibleFields.append(pField);
    }
}

std::shared_ptr<FieldConfig> WindowConfig::GetFieldConfig(QString sFieldName) const
{
    if(!map_Fields.contains(sFieldName))
        return NULL;

    return map_Fields.value(sFieldName);
}

std::shared_ptr<FieldConfig> WindowConfig::GetFieldConfig(int iIndex) const
{
    if(iIndex > vec_VisibleFields.size())
        return NULL;

    return vec_VisibleFields.at(iIndex);
}
