/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "DataSetupManager.h"

DataSetupManager::DataSetupManager():
    p_Connection(nullptr), p_Logger(nullptr)
{

}

void DataSetupManager::Setup(Connection* pConnection, Logger *pLogger)
{
    p_Connection = pConnection;
    p_Logger = pLogger;
}

void DataSetupManager::ResetData()
{
    QStringList sTables =  p_Connection->GetTables();
    foreach(QString sTable, sTables)
    {
        if(!IsUserDataTable(sTable))
        {
            p_Logger->WriteLog("%s is not a user data table", qPrintable(sTable));
            continue;
        }
        p_Logger->WriteLog("%s is a user data table,claring table", qPrintable(sTable));
        p_Connection->EmptyTable(sTable);
    }
}

bool DataSetupManager::IsUserDataTable(QString sTableName)
{
    return set_UserDataData.contains(sTableName);
}

void DataSetupManager::AddUserDataTable(QString sTableName)
{
    set_UserDataData.insert(sTableName);
}

