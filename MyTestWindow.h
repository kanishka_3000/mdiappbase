#ifndef MYTESTWINDOW_H
#define MYTESTWINDOW_H

#include <QWidget>
#include "ui_MyTestWindow.h"
#include <GenericWnd.h>
class MyTestWindow : public GenericWnd,public Ui::MyTestWindow
{
	Q_OBJECT

public:
	MyTestWindow(MainWnd *parent,QString sTitle );
	~MyTestWindow();
};

#endif // MYTESTWINDOW_H
