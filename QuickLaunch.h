/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef QUICKLAUNCH_H
#define QUICKLAUNCH_H

#include <QWidget>
#include <GenericWnd.h>
#include <ui_QuickLaunch.h>
class QuickLaunch : public GenericWnd,public Ui::QuickLaunch
{
    Q_OBJECT

public:
    explicit QuickLaunch(MainWnd *parent, QString sTitle);
    ~QuickLaunch();

     virtual void OnCreate();

private:

};

#endif // QUICKLAUNCH_H
