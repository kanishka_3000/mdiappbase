#ifndef ABOUTAPP_H
#define ABOUTAPP_H

#include "ui_AboutApp.h"

class AboutApp : public QDialog, private Ui::AboutApp
{
    Q_OBJECT

public:
    explicit AboutApp(QWidget *parent = 0);
    void SetAppName(QString sApp, QString sVersion);
};

#endif // ABOUTAPP_H
