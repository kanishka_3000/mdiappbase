/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "FieldCompleter.h"
#include <TableStore.h>
#include <QSqlTableModel>
#include <QCompleter>
#include <QDebug>
#include <QAbstractProxyModel>
FieldCompleter::FieldCompleter(Application *parent, QString sTableName) : QObject(parent)
{
    s_TableName = sTableName;
    p_TableStore = parent->GetTableStore();
    p_Table = nullptr;
    p_Destination = nullptr;
    p_Completer = nullptr;
}

void FieldCompleter::SetSource(QLineEdit *pSource, QString sFieldName)
{
    p_Table = p_TableStore->GetTableModel(s_TableName);
    int iIndex = p_Table->fieldIndex(sFieldName);
    p_Completer= new QCompleter(pSource);
    p_Completer->setCompletionMode(QCompleter::PopupCompletion);
    p_Completer->setCaseSensitivity(Qt::CaseInsensitive);
    p_Completer->setModel(p_Table);
    p_Completer->setCompletionColumn(iIndex);
    pSource->setCompleter(p_Completer);
    connect(p_Completer,SIGNAL(activated(QModelIndex)),this,SLOT(OnSelected(QModelIndex)));
    p_Table->select();
}

void FieldCompleter::SetDestination(QLineEdit *pDestination, QString sFieldName)
{
    p_Destination = pDestination;
    i_DesIndex = p_Table->fieldIndex(sFieldName);
}

void FieldCompleter::OnSelected(QModelIndex oIndex)
{

    QModelIndex oIdx = ((QAbstractProxyModel*)p_Completer->completionModel())->mapToSource(oIndex);

    QModelIndex oIdxoriginal = p_Table->index(oIdx.row(),i_DesIndex);
    QVariant oData= p_Table->data(oIdxoriginal);
    p_Destination->setText(oData.toString());

}

