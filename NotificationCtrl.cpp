/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "NotificationCtrl.h"
#include <QTextCharFormat>
#include <NotificationPanel.h>

NotificationCtrl::NotificationCtrl(QWidget *parent) :
    GenericCtrl(parent),i_CurrentYear(-1),i_CurrentMonth(-1)
{
    setupUi(this);
    p_SpacerItem = new QSpacerItem(5, 5);

}

NotificationCtrl::~NotificationCtrl()
{
    delete p_SpacerItem;
}

void NotificationCtrl::OnCreateCtrl()
{
    connect(p_Calender, SIGNAL(currentPageChanged(int,int)),this, SLOT(OnPageChanged(int,int)));
    connect(p_Calender,SIGNAL(activated(QDate)),this,SLOT(OnDateSelected(QDate)));
    p_NotificationWidget->layout()->setContentsMargins(1,1,1,1);
    p_NotificationWidget->layout()->setSpacing(1);
}

void NotificationCtrl::Initialize(NotificationPopulator *pPopulator)
{
    p_Populator = pPopulator;
    i_CurrentMonth = p_Calender->selectedDate().month();
    i_CurrentYear = p_Calender->selectedDate().year();
    Populate();
}

void NotificationCtrl::Populate()
{
    QList<QDate> lstDates;
    QList<QString> lstNotifications;
    p_Populator->GetData(i_CurrentYear,i_CurrentMonth,lstDates,lstNotifications);

    foreach(QDate oDate, lstDates)
    {
        QTextCharFormat oFormat = p_Calender->dateTextFormat(oDate);
        oFormat.setForeground(Qt::green);
        oFormat.setFontUnderline(true);
        p_Calender->setDateTextFormat(oDate,oFormat);
    }
    p_NotificationWidget->layout()->addItem(p_SpacerItem);
    foreach(QString sNotification, lstNotifications)
    {
        NotificationPanel* pPanel = new NotificationPanel(this);
        lst_CurrentPanels.push_back(pPanel);
        pPanel->SetNotification(sNotification);
        p_NotificationWidget->layout()->addWidget(pPanel);
    }

}

void NotificationCtrl::RemoveCurrentPanels()
{
    foreach(NotificationPanel* pPanels, lst_CurrentPanels)
    {
        delete pPanels;
    }
    lst_CurrentPanels.clear();
     p_NotificationWidget->layout()->removeItem(p_SpacerItem);
//    QLayout* pLayout = p_NotificationWidget->layout();
//    delete pLayout;
//    pLayout = new QVBoxLayout(this);
//    p_NotificationWidget->setLayout(pLayout);
}

void NotificationCtrl::OnPageChanged(int iYear, int iMonth)
{
    if(i_CurrentMonth == iMonth && i_CurrentYear == iYear)
    {
        return;
    }
    i_CurrentYear = iYear;
    i_CurrentMonth = iMonth;
    RemoveCurrentPanels();
    Populate();
}

void NotificationCtrl::OnDateSelected(QDate date)
{
    p_Populator->OnDateSelected(date);
}
