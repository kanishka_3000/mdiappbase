/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "QueryDataSource.h"

QueryDataSource::QueryDataSource(QSqlQueryModel *pModel)
{
    p_Model = pModel;
}

QStringList QueryDataSource::GetHeader() const
{
    QStringList sList;
    int iCount = p_Model->columnCount();

    for(int i = 0; i < iCount ; i++)
    {
        sList.push_back(p_Model->headerData(i, Qt::Orientation::Horizontal).toString());
    }
    return sList;
}

int QueryDataSource::GetItemCount() const
{
   return p_Model->rowCount();
}

std::shared_ptr<feature::CSVExporter::CSVDataRow> QueryDataSource::GetDataRow(int iRow) const
{
    QSqlRecord pRecord = p_Model->record(iRow);
    std::shared_ptr<feature::CSVExporter::CSVDataRow> pRow(new QueryDataItem(pRecord));
    return pRow;
}


QueryDataItem::QueryDataItem(QSqlRecord pRecord)
{

    int iCount = pRecord.count();
    for(int i = 0; i < iCount; i++)
    {
        o_List.push_back(pRecord.value(i).toString());
    }
}

QString QueryDataItem::GetData(int iColumn)
{
    return o_List.at(iColumn);
}
