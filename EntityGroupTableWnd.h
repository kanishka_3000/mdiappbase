/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTITYGROUPTABLEWND_H
#define ENTITYGROUPTABLEWND_H

#include <QWidget>
#include <GenericWnd.h>
#include <ui_EntityGroupTableWnd.h>
class EntityGroupTableCtrl;
struct GroupingParameters
{
    QString s_GroupEntity;//Table that holds the group information
    QString s_FLDGroupDescription;//Field in group table that describes each group
    QString s_FLDGroupID;//Key of the group table


    QString s_DataTable;//Data table
    QString s_DataTableKey;
    QString s_GroupColumn;//Data table's column holding group
};

class EntityGroupTableWnd : public GenericWnd, public Ui::EntityGroupTableWnd
{
    Q_OBJECT

public:
    explicit EntityGroupTableWnd(MainWnd* pWnd, QString sTitle);
    ~EntityGroupTableWnd();
    virtual void OnCreate();
    virtual void Initialize(GroupingParameters& oGroupingParams, bool bAcceptChange = true);

    QList<std::shared_ptr<Entity>> GetChekedItems();
    void SetAcceptChanges(bool bAccept){b_AcceptChanges = bAccept;}
    void Reload();
signals:
    void NotifiyDataSelected(QList<std::shared_ptr<Entity>> pEntities, bool bSelectNClose);

public slots:
    void OnSelect();
    void OnSelectAndClose();
    virtual void OnAddNew(int iGrouID);
    virtual void OnEdit(std::shared_ptr<Entity> pEntity);
    virtual void OnCopy(std::shared_ptr<Entity> pEntity);

protected:
    virtual void OnAddNew(std::shared_ptr<Entity> pGroupEntity){(void)pGroupEntity;}
    bool b_AcceptChanges;
    QMap<int,std::shared_ptr<Entity>> map_Groups;

};

#endif // ENTITYGROUPTABLEWND_H
