#include "MainWnd.h"
#include <QtWidgets/QApplication>
#include <Application.h>
#include <ObjectFactory.h>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    ObjectFactory oFactory;
    Application oApp;

    oApp.Initialize(&a,&oFactory);

	return a.exec();
}
