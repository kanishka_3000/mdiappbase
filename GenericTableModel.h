/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef GENERICTABLEMODEL_H
#define GENERICTABLEMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <TableConfigHandler.h>
#include <QVector>
#include <QVariant>
class GenericTableDataCalback
{
public:
    //Overriding class is expected to fill rData using a correct cast of pData
    //Refering Window Configs to identify fields if necessary
    virtual void GetRow(const WindowConfig* pWindowConfig,std::shared_ptr<void> pData,QVector<QVariant>& rData) = 0;
    //Allows user to apply the data to the internal structure
    virtual void OnDataChanged(const WindowConfig* pWindowConfig, int iColumn, std::shared_ptr<void> pData,QVariant oValue){(void)pWindowConfig; (void)iColumn;(void)pData;(void)oValue;}
};

class GenericTableModel : public QAbstractTableModel
{
public:

    GenericTableModel(QObject* pParent, WindowConfig* pWindowConfig);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual int	rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex & index) const;
    //Set the data reporting model
    void SetDataCalback(GenericTableDataCalback* pCalback){p_DataCallback = pCalback;}
    //Enter the data
    virtual void InsertRow(QString sKey, std::shared_ptr<void> pData);

    virtual void RemoveRow(QString sKey);

    std::shared_ptr<void> GetData(int iRow);
    void GetCheckedKeys(QSet<QString> &setData);

    void GetCheckedData(QList<std::shared_ptr<void> > &lstData);

    void AddNewRow(std::shared_ptr<void> pData, bool bExist, QString sKey);
    void AddNewRow(QString sKey, std::shared_ptr<void> pData);


    void SetReadOnly(bool value);

    void Clear();

protected:
    GenericTableDataCalback* p_DataCallback;
private:
    WindowConfig o_Config;
    QMap<QString,std::shared_ptr<void>> map_Data;
    QList<QString> lst_DataIndex;
    QMap<QString, int> map_Index;
    QVector<int> vec_CheckedItems;
    int i_RowCount;
    mutable QVector<QVariant> vec_CurrentData;
    mutable int i_CurrentRow;
    bool b_ReadOnly;
};

#endif // GENERICTABLEMODEL_H
