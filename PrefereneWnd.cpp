#include "PrefereneWnd.h"
#include "ui_PrefereneWnd.h"
#include <QDir>
PrefereneWnd::PrefereneWnd(MainWnd *parent,QString sTitle) :
     GenericWnd(parent,sTitle)
{
    setupUi(this);
    SetPrefName(txt_OrgName, ORG_NAME);
    SetPrefName(txt_OrgContact, ORG_CONTACT);
    SetPrefName(txt_OrgEmail, ORG_EMAIL);
    SetPrefName(txt_OrgMedia, ORG_SOCIAL);
    SetPrefName(txt_Orgwebsite, ORG_WEB);
    SetPrefName(txt_OrgAddress, ORG_ADDRESS);
    SetPrefName(txt_Mobile, ORG_MOBILE);
    SetPrefName(chk_Footer, EBL_FOOTER);
    SetPrefName(txt_RptHeader, HEADER_FILE);
    SetPrefName(txt_Fax, ORG_FAX);
    SetPrefName(spin_MaxWndcount, MAX_WND_INST);

    connect(btn_OpenDirect,SIGNAL(clicked()),tab_System,SLOT(OnOpenSecDbConfig()));
    connect(btn_CopyDB,SIGNAL(clicked()),tab_System, SLOT(OnCopyDB()));
    connect(btn_Load,SIGNAL(clicked(bool)),this,SLOT(OnLoadStyle()));
    connect(btn_Clear, SIGNAL(clicked(bool)), this, SLOT(OnClearData()));
    connect(btn_Embed, SIGNAL(clicked(bool)),this, SLOT(OnChangeHeader()));
}

PrefereneWnd::~PrefereneWnd()
{

}

void PrefereneWnd::OnPreCreate(Application *pApplication)
{
    AddCustomTabs();
    GenericWnd::OnPreCreate(pApplication);
}

void PrefereneWnd::OnSave()
{
    QList<BasePreferenceTab*> pTabList = findChildren<BasePreferenceTab*>();
    foreach(BasePreferenceTab* pTab, pTabList)
    {
        pTab->OnSave(p_Application->GetConfig());
    }
    RemoveWindow();
    // deleteLater();
}

void PrefereneWnd::OnLoadStyle()
{
    QString sStyle = cmb_Styles->currentText();
    p_Application->SetStyleSheet(sStyle);
}

void PrefereneWnd::OnClearData()
{
    if(QMessageBox::Yes == QMessageBox::question(this, tr("Reset"),tr("THIS WILL ERASE YOUR DATA, <br/>"
                                                                      " IT IS NOT POSSIBLE TO RECOVER THIS DATA, <BR/> "
                                                                      " ARE YOU SURE YOU WANT TO PROCEED?")))
    {
       p_Application->ResetData();
    }
}

void PrefereneWnd::OnChangeHeader()
{
    QString sFileName = QFileDialog::getOpenFileName(this, "Set Header", QDir::homePath(), "Images (*.png)");

    QFile oFile(sFileName);
//    if(!oFile.open(QIODevice::ReadOnly))
//    {
//        QMessageBox::warning(this, tr("Invalid file"), tr("Invalid file"));
//        return;
//    }

    QImage oImage(sFileName);
    int iHeight = oImage.height();
    int iWidth = oImage.width();
    if(iHeight != 324 || iWidth != 699)
    {
        QMessageBox::warning(this, tr("Incorrect header size"), tr("Header should be 699px width, 324px height"));
        return;
    }
    QString sDesFile = QDir::currentPath() + QDir::separator() + "Reports/Title.png";
    QFile oDesfile(sDesFile);
    oDesfile.rename(QDateTime::currentDateTime().toString("YYYmmddHHMMss"));
    txt_RptHeader->setText(sFileName);
    if(oFile.copy(sDesFile))
    {
        QMessageBox::information(this, tr("File Embeded Successfully"), tr("File Embeded Successfully"));
    }
    else
    {
        QMessageBox::information(this, tr("File Embeded Failed"), oFile.errorString());
    }


}

void PrefereneWnd::OnCreate()
{
    QDir oDir("style","*.css *.qss");
    QStringList oStyles = oDir.entryList();
    cmb_Styles->addItems(oStyles);

    QList<BasePreferenceTab*> pTabList = findChildren<BasePreferenceTab*>();
    foreach(BasePreferenceTab* pTab, pTabList)
    {
        pTab->Initialize(p_Application->GetConfig());
    }

    ReSize(366,412);
}

void PrefereneWnd::SetPrefName(QWidget *pWidget, QString sPrefName)
{
    pWidget->setProperty(PREF_NAME,sPrefName);
}

void PrefereneWnd::AddTab(BasePreferenceTab *pTab, QString sTitle)
{
    p_MainTab->addTab(pTab, sTitle);
}
