/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "NotificationPanel.h"


NotificationPanel::NotificationPanel(QWidget *parent) :
GenericCtrl(parent)
{
    setupUi(this);
}

NotificationPanel::~NotificationPanel()
{

}

void NotificationPanel::SetNotification(QString sNotificaion)
{
    txt_NotificationText->setText(sNotificaion);
}
