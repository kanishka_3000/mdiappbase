/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTITIYREPORABLEMODEL_H
#define ENTITIYREPORABLEMODEL_H

#include <QObject>
#include <GenericTableModel.h>
#include <reporting/ReportableModel.h>
#include <Entity.h>
#include <assert.h>
#include <vector>
class EntityReportData;

class EntityDataResolver
{
public:
    EntityDataResolver(){}
    virtual void Resolve(std::shared_ptr<EntityReportData> pData) = 0;
};

class EntityReportData: public reporting::ReportData
{
public:
    EntityReportData(std::shared_ptr<Entity> pEntity);
    QVariant GetReportDataItem(QString sColumn);
    void AddEntity(std::shared_ptr<Entity> pEntity);
    std::shared_ptr<Entity> GetPrimaryEntity();
private:
    std::shared_ptr<Entity> p_Data;
    std::vector<std::shared_ptr<Entity>> vec_Data;
};

class EntitiyReporableModel : public GenericTableModel, public reporting::ReportableModel
{
    Q_OBJECT
public:
    explicit EntitiyReporableModel(QObject* pParent, WindowConfig* pWindowConfig, EntityDataResolver* pResolver = nullptr);
    virtual std::shared_ptr<reporting::ReportData> GetReportDataRow(int iRow);
    virtual int GetRowCount();
signals:

public slots:
private:
    EntityDataResolver* p_Resolver;
};

#endif // ENTITIYREPORABLEMODEL_H
