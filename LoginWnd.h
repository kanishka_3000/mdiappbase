/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef LOGINEND_H
#define LOGINEND_H

#include <QDialog>
#include <Application.h>
namespace Ui {
class LoginWnd;
}
class User;
class LoginWnd : public QDialog
{
    Q_OBJECT

public:
    explicit LoginWnd(Application* pApp,QWidget *parent = 0);
    ~LoginWnd();
    void OnLogin(QString sPassword, QString sUsername);
public slots:
    virtual void OnSubmit();

signals:
    void NotifyLoginSucess(std::shared_ptr<User> pUser);
private:
    Ui::LoginWnd *ui;
};

#endif // LOGIN_H
