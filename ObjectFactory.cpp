#include "ObjectFactory.h"
#include <MainWnd.h>
#include <EntityFactory.h>
#include <DataSetupManager.h>
ObjectFactory::ObjectFactory()
{

}

MainWnd *ObjectFactory::CreateMainWnd()
{
    return new MainWnd();
}

EntityFactory *ObjectFactory::CreateEntityFactory()
{
    return new EntityFactory();
}

DataSetupManager *ObjectFactory::GetSetupManager()
{
    return new DataSetupManager();
}

