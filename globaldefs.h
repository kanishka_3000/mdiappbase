#ifndef GLOBALDEFS
#define GLOBALDEFS
//General

#define LOG_FILE_DURATION 10

#define DB_NAME "Application"
#define PREF_NAME  "pref_name"
#define FIELD       "field_name"
#define VALID_FOR_ST  "ValidForStore"
#define FILTER_TYPE  "filter_type"
#define CMB_IS_USERDATA "isuserdata"
#define ENUM_NAME "enum_name"
#define BUDDY "buddy_widget"
//Entity IDs

#define ENTITY_GENERIC "Generic"
#define ENTITY_USER  "Login"
#define ENTITY_ROLE  "Role"
#define ENTITY_PRIVILEDGE  "Priviledge"

//Field IDs

#define FLD_USERNAME "username"
#define FLD_PASSWORD "password"
#define FLD_ROLENAME "rolename"
#define FLD_PRICODE "pricode"

#define WND_CH_PW  100
#define WND_PREF   1
#define WND_AD_USR  2
#define WND_MG_USERS 3
#define WND_QLAUNCH 4
#define WND_NOTIFICATIONS 5
//Priviledges

#define PRI_ADD_USERS  1
#define PRI_MANAGE_USERS 2

#define LIST_SELECTED_MENU "SelectedLaunch"

//Preferences

#define DB_LOCATION "DBLocation"
#define LOG_LOCATION "LogLocation"
#define DB_REP_LOCTION "DBRepLocation"
#define REP_DB "ReplicateDB"
#define SHOW_QL "ShowQL"
#define STYLES "EnableStyles"
#define SHOW_MAXIMIZED "ShowMaximized"
#define STYLE_SHEET "prefstyle"
#define CALENDER "calander"
#define DATE_FORMAT "yyyy-MM-dd"
#define ORG_NAME "orgname"
#define ORG_ADDRESS "orgaddress"
#define ORG_CONTACT "orgcontact"
#define ORG_SOCIAL "orgsocial"
#define ORG_EMAIL "orgemail"
#define ORG_WEB "orgweb"
#define ORG_MOBILE "mobile"
#define ORG_FAX "fax"
#define ORG_PARTNERS "parners"
#define MAX_WND_INST "maxwndinstances"
#define STANDALONE "StandAlone"
#define  SERVER "Server"
#define DBUSERNAME "dbusername"
#define DBPASSWORD "dbpassword"
#define EBL_FOOTER  "enablefooter"
#define HEADER_FILE "header_file"
//Filter Types

#define FILTER_TYPE_EXACT 1
#define FILTER_TYPE_LIKE 0
#define FILTER_TYPE_BETWEEN 2
//Enum names
#define E_STATE "state"

#define E_ORDER_STATE "orderstate"

#define MAX_WND_COUNT  10

#endif // GLOBALDEFS

