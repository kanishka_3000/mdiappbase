/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "EntitiyReporableModel.h"

EntitiyReporableModel::EntitiyReporableModel(QObject *pParent, WindowConfig *pWindowConfig, EntityDataResolver *pResolver) :
    GenericTableModel(pParent, pWindowConfig),p_Resolver(pResolver)
{

}

std::shared_ptr<reporting::ReportData> EntitiyReporableModel::GetReportDataRow(int iRow)
{
    std::shared_ptr<void> pData = GetData(iRow);
    std::shared_ptr<Entity> pEntity = std::static_pointer_cast<Entity>(pData);


    std::shared_ptr<EntityReportData> pReportItem(new EntityReportData(pEntity));
    if(p_Resolver)
    {
        p_Resolver->Resolve(pReportItem);
    }
    return pReportItem;
}

int EntitiyReporableModel::GetRowCount()
{
    return rowCount();
}

EntityReportData::EntityReportData(std::shared_ptr<Entity> pEntity)
{
    AddEntity(pEntity);
}

QVariant EntityReportData::GetReportDataItem(QString sColumn)
{
    for(auto itr = vec_Data.begin(); itr != vec_Data.end(); itr++)
    {
        std::shared_ptr<Entity> pEntity = *itr;
        QVariant oData = pEntity->GetData(sColumn);
        if(oData != QVariant())
            return oData;
    }
    return "";
}

void EntityReportData::AddEntity(std::shared_ptr<Entity> pEntity)
{
    vec_Data.push_back(pEntity);
}

std::shared_ptr<Entity> EntityReportData::GetPrimaryEntity()
{
    return vec_Data.at(0);
}
