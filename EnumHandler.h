/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENUMHANDLER_H
#define ENUMHANDLER_H

#include <QMap>
#include <QString>
#include <memory>
#include <globaldefs.h>
class Enamuration
{
public:
    virtual ~Enamuration(){}
    void AddEnumData(QString sValue, int iValue);

    int GetValue(QString sValue);
    QString GetValue(int iValue);

    QMap<QString,int> GetDataValuePair(){return map_StToint;}
private:
    QMap<QString,int> map_StToint;
    QMap<int,QString> map_intToSt;
};

class EnumHandler
{
public:
    ~EnumHandler();
    void AddEnumData(QString sEnum, QString sValue, int iValue);
    Enamuration* GetEnum(QString sEnum);

    void FillInitialData();

public:
    EnumHandler();
    QMap<QString, Enamuration*> map_Enamurations;
};

#endif // ENUMHANDLER_H
