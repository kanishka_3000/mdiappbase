#include "EntityTableDataHandler.h"

EntityTableDataHandler::EntityTableDataHandler()
{

}

void EntityTableDataHandler::GetRow(const WindowConfig *pWindowConfig, std::shared_ptr<void> pData, QVector<QVariant> &rData)
{
    std::shared_ptr<Entity> pEntity = std::static_pointer_cast<Entity>(pData);
    int iSize = pWindowConfig->GetFieldCount();
    for(int i = 0 ;i < iSize; i++)
    {
        std::shared_ptr<FieldConfig> pField = pWindowConfig->GetFieldConfig(i);
        QString sFieldName = pField->s_FiledName;
        QVariant oData = pEntity->GetData(sFieldName);
        rData.insert(i,oData);
    }

}

