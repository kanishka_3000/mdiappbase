#include "GeneralPreferenceTab.h"
#include <Application.h>
#include <GenericWnd.h>
#include <ActionStore.h>
#include <QListWidget>
#include <Util.h>
#include <GenericWnd.h>
GeneralPreferenceTab::GeneralPreferenceTab()
{

}

void GeneralPreferenceTab::OnCreateCtrl()
{
    QListWidget* pAllItems = findChild<QListWidget*>("list_AllLaunch");
    QMap<QString,QAction*> lstActions = p_Parent->GetApplication()->GetActionStore()->GetAllActions();
    QStringList sSelctedList = Util::GetSettingsList(p_Parent->GetApplication()->GetConfig(),LIST_SELECTED_MENU);
    foreach(QAction* pAction, lstActions.values())
    {
        if(sSelctedList.contains(pAction->text()))
                continue;

        QListWidgetItem* pItem = new QListWidgetItem(pAction->text(),pAllItems);
        pAllItems->addItem(pItem);
    }

}

