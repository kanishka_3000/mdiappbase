/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ADDUSERCTRL_H
#define ADDUSERCTRL_H

#include <QWidget>
#include <GenericCtrl.h>
#include <User.h>
#include <ui_AddUserCtrl.h>
#include <QSqlTableModel>
#include <EntityManipulaterCtrl.h>
class AddUserCtrl : public EntityManipulaterCtrl,public Ui::AddUserCtrl
{
    Q_OBJECT

public:
    explicit AddUserCtrl(QWidget *parent = 0);
    ~AddUserCtrl();
    virtual void FillData(std::shared_ptr<User> pUser);
    virtual bool CustomValidations(QString& sError);
    virtual void OnCreateCtrl();
private:
    bool b_Edit;
    std::shared_ptr<User> p_User;
    QSqlTableModel* p_Roles;
public slots:
    //virtual void OnSave();
    virtual void OnClear();


};

#endif // ADDUSERCTRL_H
