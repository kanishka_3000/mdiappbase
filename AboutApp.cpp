#include "AboutApp.h"
#include <QDate>
AboutApp::AboutApp(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}

void AboutApp::SetAppName(QString sApp, QString sVerion)
{
    lbl_AppName->setText(sApp);
    lbl_Version->setText(sVerion + QString(" - build: ") + QString(__DATE__) + QString(" ") + __TIME__);
}
