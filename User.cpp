#include "User.h"
#include <Application.h>
#include <QDebug>
bool User::b_DefInitlized = false;
User::User()
{
    s_PersistatName = TBLNM_APP_LOGIN;
    s_DefName = ENTITY_USER;
    s_Key = FLD_USERNAME;
    p_Role = NULL;
}

User::~User()
{

}

QString User::GetUserName()
{
    return map_Data.value(FLD_USERNAME).toString();
}

bool User::IsPasswordCorrect(QString sTest)
{
    QString sPass = map_Data.value(FLD_PASSWORD).toString();
    return (sTest == sPass);
}

void User::ChangePassword(QString sPassword)
{
    //QMap<QString,QVariant> mapDt;
    //mapDt.insert(FLD_PASSWORD,sPassword);
    SetUpdateData(FLD_PASSWORD,sPassword);
    UpdateData();
}

void User::InitlizeDefs()
{

    if(!b_DefInitlized)
    {
        Initlize();
        b_DefInitlized = true;
    }
}

void User::ResolveReferences()
{
    //Application::GetApplication()->GetEntityFactory()->CreateEntity(ENTITY_ROLE);
    p_Role = std::dynamic_pointer_cast<Role>(Role::FindInstance(GetData(FLD_ROLENAME).toString(),ENTITY_ROLE));
    p_Role->ResolveReferences();
}

