/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef DATASETUPMANAGER_H
#define DATASETUPMANAGER_H
#include <Connection.h>
#include <Logger.h>
#include <QSet>
#include <QString>
class DataSetupManager
{
public:
    DataSetupManager();
    virtual void Setup(Connection* pConnection, Logger* pLogger);
    void ResetData();
    void AddUserDataTable(QString sTableName);
protected:
    bool IsUserDataTable(QString sTableName);

private:
    Connection* p_Connection;
    Logger* p_Logger;
    QSet<QString> set_UserDataData;
};

#endif // DATASETUPMANAGER_H
