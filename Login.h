/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef LOGIN_H
#define LOGIN_H

#include <QString>
#include <User.h>
class Login
{
public:
    Login();
    bool Authenticate(QString sUsername, QString sPassword, std::shared_ptr<User> &prUser);
};

#endif // LOGIN_H
