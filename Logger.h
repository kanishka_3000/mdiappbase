/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef LOGGER_H
#define LOGGER_H
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QDateTime>
#include <QDir>
class Logger
{
public:
    Logger(QString* sLogLocation);
    bool OpenLogFile();
    void WriteLog(QString sLogString);
    void WriteLog(const char* fmt, ...);
    int ClearLogs(int iPastDays);
protected:
    QFile* p_File;
    QTextStream* p_TextStream;
    QDateTime o_CurrentDate;
    QString s_Location;
    bool b_LogFileOpened;
};

#endif // LOGGER_H
