#include "GenericTableModel.h"
#include <QDebug>
GenericTableModel::GenericTableModel(QObject *pParent, WindowConfig *pWindowConfig)
    :QAbstractTableModel(pParent)
{
    o_Config = *pWindowConfig;
    i_RowCount = 0;
    i_CurrentRow = -1;
    b_ReadOnly = false;
}

QVariant GenericTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation != Qt::Horizontal)
        return QVariant();
    if(role != Qt::DisplayRole)
        return QVariant();

    return o_Config.GetFieldConfig(section)->s_DisplayName;
}

int GenericTableModel::columnCount(const QModelIndex &parent) const
{
    (void)parent;
    return o_Config.GetFieldCount();
}

QVariant GenericTableModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::CheckStateRole && index.column() == 0 && (!b_ReadOnly))
    {
        return vec_CheckedItems.at(index.row());
    }

    if(role != Qt::DisplayRole)
    {
        return QVariant();
    }
    if(!p_DataCallback)
    {
        return QVariant();
    }
    if(index.row() != i_CurrentRow)
    {
        i_CurrentRow = index.row();
        QString sKey = lst_DataIndex.value(index.row());
        std::shared_ptr<void> pData = map_Data.value(sKey);
        vec_CurrentData.clear();
        p_DataCallback->GetRow(&o_Config,pData,vec_CurrentData);
    }

    if(index.column() > vec_CurrentData.size())
        return QVariant();

    return vec_CurrentData.at(index.column());

}

bool GenericTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role == Qt::CheckStateRole && index.column() == 0)
    {

        vec_CheckedItems[index.row()] = value.toInt();
        emit dataChanged(index,index);
        return true;
    }
    else if(role == Qt::EditRole)
    {

        QString sKey = lst_DataIndex.at(index.row());
        std::shared_ptr<void> pData = map_Data.value(sKey);
        p_DataCallback->OnDataChanged(&o_Config,index.column(),pData,value);
        i_CurrentRow = -1;
        emit dataChanged(index,index);
    }
    return false;

}

Qt::ItemFlags GenericTableModel::flags(const QModelIndex &index) const
{
    if(b_ReadOnly)
    {
        //No checkbox or spin box updates are allowd during readonly mode
        return QAbstractTableModel::flags(index);
    }
    std::shared_ptr<FieldConfig> pField = o_Config.GetFieldConfig(index.column());
    if(pField->e_Editability == FieldConfig::NOT_EDITABLE)
    {
         return QAbstractTableModel::flags(index);
    }
    else if (pField->e_Editability == FieldConfig::CHECk_BOX)
    {
         return Qt::ItemFlag::ItemIsUserCheckable|QAbstractTableModel::flags(index);
    }
    else if(pField->e_Editability == FieldConfig::SPIN_BOX)
    {
         return Qt::ItemFlag::ItemIsEditable|QAbstractTableModel::flags(index);
    }

    return QAbstractTableModel::flags(index);


}



void GenericTableModel::AddNewRow(QString sKey, std::shared_ptr<void> pData)
{
    beginInsertRows(QModelIndex(),i_RowCount,i_RowCount);

    map_Data.insert(sKey,pData);
    lst_DataIndex.push_back(sKey);
    map_Index.insert(sKey,i_RowCount);
    i_RowCount++;
    vec_CheckedItems.push_back(Qt::Unchecked);


    endInsertRows();
}

void GenericTableModel::SetReadOnly(bool value)
{
    b_ReadOnly = value;
}

void GenericTableModel::Clear()
{
    beginResetModel();
    map_Data.clear();

    lst_DataIndex.clear();
    map_Index.clear();
    vec_CheckedItems.clear();
    i_RowCount = 0;
    endResetModel();
}

void GenericTableModel::InsertRow(QString sKey, std::shared_ptr<void> pData)
{
    bool bExist = map_Data.contains(sKey);
    if(!bExist)
    {
        AddNewRow(sKey, pData);
    }
    else
    {
        //To DO;; For the moment table model will not handle duplicates
        //map_Data.insert(sKey,pData);
    }
}

void GenericTableModel::RemoveRow(QString sKey)
{
    int iRow = map_Index.value(sKey);
    beginResetModel();
    //beginRemoveRows(QModelIndex(),iRow,iRow);
    map_Data.remove(sKey);
    int iIndex = lst_DataIndex.indexOf(sKey);
    lst_DataIndex.removeAt(iIndex);
    map_Index.remove(sKey);
    vec_CheckedItems.removeAt(iRow);
    i_RowCount--;
    //endRemoveRows();
    endResetModel();

}

std::shared_ptr<void> GenericTableModel::GetData(int iRow)
{
    QString sKey =  lst_DataIndex.at(iRow);
    if(sKey.isEmpty())
        return NULL;
    return map_Data.value(sKey);
}

void GenericTableModel::GetCheckedKeys(QSet<QString> &setData)
{
    int iCount = 0;
    for(QVector<int>::iterator itr = vec_CheckedItems.begin(); itr != vec_CheckedItems.end(); itr++,iCount++)
    {
        if(*itr == Qt::Checked)
        {
            setData.insert(lst_DataIndex.at(iCount));
        }
    }
}

void GenericTableModel::GetCheckedData(QList<std::shared_ptr<void> > &lstData)
{
    int iCount = 0;
    for(QVector<int>::iterator itr = vec_CheckedItems.begin(); itr != vec_CheckedItems.end(); itr++,iCount++)
    {
        if(*itr == Qt::Checked)
        {
            QString sKey = lst_DataIndex.at(iCount);
            lstData.push_back(map_Data.value(sKey));
        }
    }
}

int GenericTableModel::rowCount(const QModelIndex &parent)const
{
    (void)parent;
    return i_RowCount;
}

