/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENTITYFACTORY_H
#define ENTITYFACTORY_H

#include <entity/Entity.h>
#include <Entitydef.h>
#include <QMap>
#include <QString>
#include <memory>
class BaseEntityCreator
{
public:
	virtual ~BaseEntityCreator(){}
    virtual Entity* Create() = 0;
};

template <class T>
class EntityCreator: public BaseEntityCreator
{
    Entity* Create(){return new T();}
};

class EntityFactory
{
public:
    EntityFactory();
    virtual ~EntityFactory();
    virtual void AddDef(QString sDefName, EntityDef* pDef);
    EntityDef* GetDef(QString sDefName);

    std::shared_ptr<Entity> CreateEntity(QString sDef);
    template<class T>
    std::shared_ptr<T> CreateEntityEx(QString sDef)
    {
        std::shared_ptr<Entity> pEntity = CreateEntity(sDef);
        return std::static_pointer_cast<T>(pEntity);
    }

    virtual void CreateCustomEntity(QString sDef, Entity*& prEntity){(void)sDef;(void)prEntity;}
    QMap<QString,EntityDef*> map_Defs;
    virtual void Initialize();

    template<class clName>
    void SetHandler(QString sEntity)
    {
        EntityCreator<clName>* pCr = new EntityCreator<clName>();
        map_Creators.insert(sEntity,pCr);
    }


protected:
    virtual void RegisterEntities();
    QMap<QString, BaseEntityCreator*> map_Creators;
    QMap<QString,QString> map_EntityNames;
};

#endif // ENTITYFACTORY_H
