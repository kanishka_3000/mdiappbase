/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "EntityDataSummerizer.h"

entity::EntityDataSummerizer::EntityDataSummerizer(SummeryFacilitator &rFactory, QObject *parent) :
    QObject(parent),r_Facilitator(rFactory)
{

}

void entity::EntityDataSummerizer::Summerize(QList<std::shared_ptr<Entity> >& lstEntities,
                                      QMap<QString, std::shared_ptr<EntitySummery> > &mapSummeries)
{

    std::shared_ptr<EntitySummery> pSummery = nullptr;
    foreach(std::shared_ptr<Entity> pEntity, lstEntities)
    {
        QString sSumData;
        int iCount;
        r_Facilitator.GetNameCount(pEntity, sSumData, iCount);
        if(!mapSummeries.contains(sSumData))
        {
            pSummery = r_Facilitator.Create();
            pSummery->SetTitle(sSumData);
            mapSummeries.insert(sSumData, pSummery);

        }
        else
        {
            pSummery = mapSummeries.value(sSumData);
        }
        pSummery->IncreaseCount(iCount);

    }
}



void entity::EntitySummery::SetCount(int iCount)
{
    i_Count = iCount;
}

void entity::EntitySummery::SetTitle(QString sTitle)
{
    s_Title = sTitle;
}

void entity::EntitySummery::IncreaseCount(int iCount)
{
    i_Count+= iCount;
}

int entity::EntitySummery::GetCount()
{
    return i_Count;
}
