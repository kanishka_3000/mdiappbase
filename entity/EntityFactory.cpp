#include "EntityFactory.h"
#include <globaldefs.h>
#include <User.h>
#include <Role.h>
EntityFactory::EntityFactory()
{
    map_EntityNames.insert("Login",FLD_USERNAME);
    map_EntityNames.insert("Role",FLD_ROLENAME);
    map_EntityNames.insert("Priviledge",FLD_PRICODE);

}

EntityFactory::~EntityFactory()
{
	for(QMap<QString,EntityDef*>::iterator itr = map_Defs.begin();
	    itr != map_Defs.end(); itr++)
	{
		EntityDef* pDef = itr.value();
		delete pDef;
	}
    for(auto itr = map_Creators.begin(); itr != map_Creators.end(); itr++)
    {
       BaseEntityCreator* pCr = itr.value();
       delete pCr;
    }
}

void EntityFactory::AddDef(QString sDefName, EntityDef *pDef)
{
    map_Defs.insert(sDefName,pDef);
}

EntityDef *EntityFactory::GetDef(QString sDefName)
{
    return map_Defs.value(sDefName);
}

std::shared_ptr<Entity> EntityFactory::CreateEntity(QString sDef)
{
    Entity* pEntity = NULL;
    BaseEntityCreator* pCreator = map_Creators.value(sDef);
    pEntity = pCreator->Create();

    if(pEntity)
    {
         pEntity->InitlizeDefs();
    }
    EntityDef* pDef = map_Defs.value(sDef);
    pEntity->s_DefName = sDef;
    pEntity->p_Def = pDef;
    return std::shared_ptr<Entity>(pEntity);
}

void EntityFactory::RegisterEntities()
{
    SetHandler<Entity>(ENTITY_GENERIC);
    SetHandler<User>(ENTITY_USER);
    SetHandler<Role>(ENTITY_ROLE);
    SetHandler<Priviledge>(ENTITY_PRIVILEDGE);
}

void EntityFactory::Initialize()
{
    for(QMap<QString,QString>::iterator itr = map_EntityNames.begin();
        itr != map_EntityNames.end(); itr++)
    {
        QString  sPersistantName = itr.key();
        QString sKey = itr.value();
        EntityDef* pDef = new EntityDef();
        pDef->InitiaizeDefsFromDB(sPersistantName,sKey);
        AddDef(sPersistantName,pDef);
    }
    RegisterEntities();
}

