/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENTITY_H
#define ENTITY_H
#include <QString>
#include <Entitydef.h>
#include <QMap>
#include <QVariant>
#include <globaldefs.h>
#include <Logger.h>
#include <QSet>
#include <memory>
#include <DBFields.h>
#include <cstddef>
#include <Connection.h>
#define ERR_EXISTS -3
#define DEC_ETY_INITILIZER  static bool b_DefInitlized;
#define DEF_ETY_INITILIZER b_DefInitlized = false;
#define IMPL_ETY_INITILIZER    if(!b_DefInitlized)\
{\
    Initlize();\
    b_DefInitlized = true;\
}\

class Entity
{
public:
    class EntityDataHander : public Connection::ConnectionDataHander
    {
    public:
        EntityDataHander(QString sEntityName, QList<std::shared_ptr<Entity>>& lstEntities):
            s_EntityName(sEntityName),lst_Entities(lstEntities){}

        void CreateNewItem();


        QList<QString>  GetFieldList();
        void AddDataColumn(QString sColumn, QVariant vData);

    private:
        QString s_EntityName;
        std::shared_ptr<Entity> p_CurrentEntity;
        QList<std::shared_ptr<Entity>>& lst_Entities;

    };

    Entity();
    virtual ~Entity(){}
    Entity(Entity& rEntity);
    //Add new data to a newly created object//Use to save a new object to database
    virtual void AddData(QString sFieldName, QVariant oData);
    virtual void AddData(QMap<QString,QVariant> mapData);
    //Commit changes of the enity to database
    //Add updated fields separatly// Functions will store data only if they differ the original data
    //Internally used for update of the database
    virtual bool SetUpdateData(QString sFieldName, QVariant oData);
    virtual void SetUpdateData(QMap<QString,QVariant> mapData);

    //Returns data of the field.
    virtual QVariant GetData(QString sFieldName);
    virtual int UpdateData();

    static QList<std::shared_ptr<Entity> > FindRelatedEntities(QString sField, QString sValue, QString sKeyValue = "");
    static QString BuildRelationshipQuery(QString sEntity1, QString sEntity2, QString sKeyValue);
    //Override to add the deifinition to the definition set//
    //Essential for each entity type that this is done during first object creation//
    //***Expected to use a static boolean to check that initilization happens only once.***
    virtual void InitlizeDefs() {}

    //save the entity to database
    virtual int Save();
    //static function to return any entity from the database
   static std::shared_ptr<Entity>FindInstance(QString sKeyValue,QString sEntity);
   static QList<std::shared_ptr<Entity>> FindInstances(QString sSearchKey, QString sSearchValue, QString sEntity);
   static QList<std::shared_ptr<Entity>> FindInstances(QString sSearchKey, QString sSearchValue1,QString sSearchValue2, QString sEntity);
   static QList<std::shared_ptr<Entity>> FindAllInstances(QString sEntity);

   template <class T>
   static void FindInstancesEx(QString sSearchKey, QString sSearchValue, QString sEntity, QList<std::shared_ptr<T>>& lstItems)
   {
       QList<std::shared_ptr<Entity>> lstEntities = FindInstances(sSearchKey, sSearchValue, sEntity);
       foreach(std::shared_ptr<Entity> pEntity, lstEntities)
       {
           std::shared_ptr<T> pCastEnt = std::static_pointer_cast<T>(pEntity);
           lstItems.push_back(pCastEnt);
       }
   }

   template <class T>
   static void FindInstancesEx(QString sSearchKey, QString sSearchValue1,QString sSearchValue2, QString sEntity, QList<std::shared_ptr<T>>& lstItems)
   {
       QList<std::shared_ptr<Entity>> lstEntities = FindInstances(sSearchKey, sSearchValue1,sSearchValue2, sEntity );
       foreach(std::shared_ptr<Entity> pEntity, lstEntities)
       {
           std::shared_ptr<T> pCastEnt = std::static_pointer_cast<T>(pEntity);
           lstItems.push_back(pCastEnt);
       }
   }

   template<class T>
   static std::shared_ptr<T> FindInstaceEx (QString sKeyValue, QString sEntity)
   {
       return std::dynamic_pointer_cast<T>(FindInstance(sKeyValue, sEntity));
   }

   void SetUpdate(bool bIsUpdate);
   virtual void Initlize();
   void SetKeyAutoIncrement(bool value);

   QString GetDebugString();

    virtual void ResolveReferences();

   void ResetPrimaryKey();
   bool IsFieldUnique(QString sField);
   bool IsValidField(QString sField);
    QString s_DefName;
    //Database table name
    QString s_PersistatName;
    //Name of the key field// Mainly used during the first initlization of the definition//
    //After that redundant with the def name residing inside the defintion.
    QString s_Key;
    EntityDef* p_Def;

    //Stores data of a new object
    QMap<QString,QVariant> map_Data;

    bool b_Update;

protected:
    //Updated data of fields//
    QMap<QString,QVariant> map_UpdatedData;

    bool b_KeyAutoIncrement;

};

#endif // ENTITY_H
