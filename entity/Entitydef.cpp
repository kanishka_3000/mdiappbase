#include "Entitydef.h"
#include <QStringList>
#include <Connection.h>
void EntityDef::AddFieldName(QString sName)
{
    set_Fields.insert(sName);
}

void EntityDef::InitiaizeDefsFromDB(QString sPersisname, QString sKeyField)
{
    s_PersistantName = sPersisname;
    s_Key = sKeyField;

    QStringList oFields = Connection::GetInstance()->GetTableColumns(s_PersistantName);
    foreach(QString sField, oFields)
    {
        AddFieldName(sField);
    }
}
