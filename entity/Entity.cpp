#include "Entity.h"
#include <Connection.h>
#include <Logger.h>
#include <Application.h>
#include <QDebug>
Entity::Entity()
{
    p_Def = NULL;
    s_PersistatName = "";
    s_Key = "";
    b_Update = false;
    b_KeyAutoIncrement = false;
}

Entity::Entity(Entity &rEntity)
{
    map_Data = rEntity.map_Data;
}

void Entity::AddData(QString sFieldName, QVariant oData)
{
    if(!IsValidField(sFieldName))
        return;
    map_Data.insert(sFieldName,oData);
}

void Entity::AddData(QMap<QString, QVariant> mapData)
{
    map_Data = mapData;
}

void Entity::SetUpdateData(QMap<QString, QVariant> mapData)
{
    for(QMap<QString,QVariant>::iterator itr = mapData.begin();
        itr!= mapData.end(); itr++)
    {
        SetUpdateData(itr.key(),itr.value());
    }
}

QVariant Entity::GetData(QString sFieldName)
{
    return map_Data.value(sFieldName);
}

int Entity::UpdateData()
{
    QString sKeyValue = map_Data.value(s_Key).toString();
    return Connection::GetInstance()->UpdateQuery(s_PersistatName,map_UpdatedData,s_Key,sKeyValue);
}

QString Entity::BuildRelationshipQuery(QString sEntity1, QString sEntity2, QString sKeyValue)
{
    EntityDef* pDef1 = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity1);
    EntityDef* pDef2 = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity2);

    QString sRelationshipTable = QString("%1_%2").arg(pDef1->s_PersistantName).arg(pDef2->s_PersistantName);
    QString sQuery = QString("select * from %1 inner join %2 on %3.%4 = %5.%6 where %7.%8 = '%9'").arg(sRelationshipTable).arg(pDef2->s_PersistantName).arg(sRelationshipTable).arg(pDef2->s_Key).arg(pDef2->s_PersistantName).arg(pDef2->s_Key).arg(sRelationshipTable).arg(pDef1->s_Key).arg(sKeyValue);

    return sQuery;
}

QList<std::shared_ptr<Entity>> Entity::FindRelatedEntities(QString sEntity1, QString sEntity2, QString sKeyValue)
{
    QString sQuery = BuildRelationshipQuery(sEntity1, sEntity2, sKeyValue);

    QList<std::shared_ptr<Entity> >lstEntities;
    EntityDataHander oDataHanlder( sEntity2, lstEntities );
    Connection::GetInstance()->GerRecords(sQuery,oDataHanlder);

    return lstEntities;

}



bool Entity::SetUpdateData(QString sFieldName, QVariant oData)
{
    if(!map_Data.contains(sFieldName))
        return false;//not a valid original field


    QVariant oOriginalData = map_Data.value(sFieldName);
    if(oOriginalData == oData)
        return false; //Data is same

    map_UpdatedData.insert(sFieldName,oData);
    return true;

}

int Entity::Save()
{
    if(b_Update)
    {
        return UpdateData();
    }
    else
    {
        if(Connection::GetInstance()->DoesDBFieldExists(s_PersistatName,s_Key, map_Data.value(s_Key)))
        {
            return ERR_EXISTS;
        }
        int iID = Connection::GetInstance()->InsertQuery(s_PersistatName, map_Data);
        if(b_KeyAutoIncrement && s_Key != "")
        {
            AddData(s_Key, iID);
        }
        return iID;
    }

}

std::shared_ptr<Entity> Entity::FindInstance(QString sKeyValue ,QString sEntity)
{
    EntityDef* pDef = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity);
    QList<std::shared_ptr<Entity>> oList;
    QString sQuery = QString("select * from %1 where %2 = '%3'").arg(pDef->s_PersistantName).arg(pDef->s_Key).arg(sKeyValue);
    EntityDataHander oDataHanlder( sEntity, oList );
    Connection::GetInstance()->GerRecords(sQuery, oDataHanlder );
    if(oList.size() > 1)
    {
        qDebug() << "Error more than one instance";
    }
    else if(oList.size() == 0)
    {
        return nullptr;
    }
    std::shared_ptr<Entity> pEntity = oList.first();
    return pEntity;

}

QList<std::shared_ptr<Entity> > Entity::FindInstances(QString sSearchKey, QString sSearchValue, QString sEntity)
{
     EntityDef* pDef = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity);
     QList<std::shared_ptr<Entity>> oList;

     EntityDataHander oDataHanlder( sEntity, oList );
     QString sQuery = QString("select * from %1 where %2 = '%3'").arg(pDef->s_PersistantName).arg(sSearchKey).arg(sSearchValue);
     Connection::GetInstance()->GerRecords(sQuery, oDataHanlder);

     return oList;
}

QList<std::shared_ptr<Entity> > Entity::FindInstances(QString sSearchKey, QString sSearchValue1, QString sSearchValue2, QString sEntity)
{
    EntityDef* pDef = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity);
    QList<std::shared_ptr<Entity>> oList;
    QString sQuery = QString("select * from %1 where %2 BETWEEN '%3' AND '%4'").arg(pDef->s_PersistantName).arg(sSearchKey).arg(sSearchValue1).arg(sSearchValue2);
    EntityDataHander oDataHanlder( sEntity, oList );
    Connection::GetInstance()->GerRecords(sQuery,oDataHanlder);

    return oList;
}

QList<std::shared_ptr<Entity> > Entity::FindAllInstances(QString sEntity)
{
    EntityDef* pDef = Application::GetApplication()->GetEntityFactory()->GetDef(sEntity);
    QList<std::shared_ptr<Entity>> oList;
    QString sQuery = QString("select * from %1 ").arg(pDef->s_PersistantName);
    EntityDataHander oDataHanlder( sEntity, oList );
    Connection::GetInstance()->GerRecords(sQuery, oDataHanlder);

    return oList;
}

void Entity::SetUpdate(bool bIsUpdate)
{
    b_Update = bIsUpdate;
}

void Entity::Initlize()
{
    EntityDef* pDef = NULL;
    //User
    pDef = new EntityDef();

    pDef->InitiaizeDefsFromDB(s_PersistatName,s_Key);
    Application::GetApplication()->AddDef(s_DefName,pDef);
}

QString Entity::GetDebugString()
{
    QString sDebugString;
    for(auto itr = map_Data.begin(); itr != map_Data.end(); itr++)
    {
        sDebugString += (itr.key() + itr.value().toString());
    }
    return sDebugString;
}

void Entity::ResolveReferences()
{

}

void Entity::ResetPrimaryKey()
{
    map_Data.remove(s_Key);
}

bool Entity::IsFieldUnique(QString sField)
{
    return !(Connection::GetInstance()->DoesDBFieldExists(s_PersistatName,sField, map_Data.value(sField)));
}

bool Entity::IsValidField(QString sField)
{
    return p_Def->set_Fields.contains(sField);
}
void Entity::SetKeyAutoIncrement(bool value)
{
    b_KeyAutoIncrement = value;
}


void Entity::EntityDataHander::CreateNewItem()
{
    p_CurrentEntity = (Application::GetApplication()->GetEntityFactory())->CreateEntity(s_EntityName);
     lst_Entities.push_back(p_CurrentEntity);
}

QList<QString> Entity::EntityDataHander::GetFieldList()
{
    if(p_CurrentEntity == nullptr)
        return QList<QString>();

    EntityDef* pDef = p_CurrentEntity->p_Def;
    return pDef->set_Fields.toList();
}

void Entity::EntityDataHander::AddDataColumn(QString sColumn, QVariant vData)
{
    p_CurrentEntity->AddData(sColumn, vData);
}
