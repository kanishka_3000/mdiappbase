/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef DATACOLLECTOR
#define DATACOLLECTOR
#include <memory>
#include <Entity.h>
#include <QList>
#include <QVector>
namespace entity {


class DataCollector
{
public:
    virtual ~DataCollector(){}
    virtual void CollectEntityData( QVector<QString> vecInput, QList<std::shared_ptr<Entity>>& lstData ) = 0;
};

}
#endif // DATACOLLECTOR

