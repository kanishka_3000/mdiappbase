/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTITYDATASUMMERIZER_H
#define ENTITYDATASUMMERIZER_H

#include <QObject>
#include <Entity.h>
#include <memory>
namespace  entity {


class EntitySummery
{
public:
    virtual ~EntitySummery(){}
    void SetCount(int iCount);
    void SetTitle(QString sTitle);
    void IncreaseCount(int iCount);

    int GetCount();
protected:
    int i_Count;
    QString s_Title;
};

class SummeryFacilitator
{
public:
    virtual std::shared_ptr<EntitySummery> Create() = 0;
    virtual void GetNameCount(std::shared_ptr<Entity> pEntity, QString& sKey,int& iCount) = 0;
};

class EntityDataSummerizer : public QObject
{
    Q_OBJECT
public:
    explicit EntityDataSummerizer(SummeryFacilitator& rFactory, QObject *parent = 0);
    void Summerize(QList<std::shared_ptr<Entity> >& lstEntities, QMap<QString, std::shared_ptr<EntitySummery> > &mapSummeries);
signals:

public slots:
private:
    SummeryFacilitator& r_Facilitator;
};
}
#endif // ENTITYDATASUMMERIZER_H
