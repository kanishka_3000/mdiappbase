/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENTITYDEF_H
#define ENTITYDEF_H

#include <QMap>
#include <QVariant>
#include <QString>
#include <QSet>


class EntityDef
{
public:
    EntityDef(){}
    virtual ~EntityDef(){}
    virtual void SetFieldNames(QSet<QString> lstFileds){set_Fields = lstFileds;}
    virtual void AddFieldName(QString sName);


    virtual void InitiaizeDefsFromDB(QString sPersisname, QString sKeyField);
public:
    QSet<QString> set_Fields;
     QString s_PersistantName;
     QString s_Key;
protected:

};

#endif // ENTITYDEF_H
