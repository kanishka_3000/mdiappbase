/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef GENERALPREFERENCETAB_H
#define GENERALPREFERENCETAB_H

#include <QObject>
#include <BasePreferenceTab.h>
class GeneralPreferenceTab:public BasePreferenceTab
{
    Q_OBJECT

public:
    GeneralPreferenceTab();
        virtual void OnCreateCtrl();
};

#endif // GENERALPREFERENCETAB_H
