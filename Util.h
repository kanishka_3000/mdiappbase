/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef UTIL_H
#define UTIL_H
#include <QMap>
#include <QVariant>
#include <QList>
#include <QWidget>
#include <QCheckBox>
#include <QSettings>
#include <QSet>
class Util
{
public:
    Util();
    static QMap<QString,QVariant> CollectFields(QWidget* pParent);
    static QStringList GetSettingsList(QSettings* pSettings, QString sProperty);

    //Creates a string by apending series of pairs of each item in the list
    QString CreatePersistString(QList<QPair<int,int>> lstData);
    //Creates a list from a string created by CreatePersistString
    QList<QPair<int,int>> LoadFromPeristData(QString sData);
    static QString DoubleToString(double dValue);

    static void CollectCheckState(QCheckBox* pCheckbox, int iVal, QSet<int>& setCollecter);
    static qint64 GetUnique();
};

#endif // UTIL_H
