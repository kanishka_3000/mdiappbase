/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LabelAnimator.h"

LabelAnimator::LabelAnimator(QLabel *pLabel,QObject* pParent):
    p_Aniatee(pLabel),p_Parent(pParent)
{

}

void LabelAnimator::Animate(LabelAnimator::AnimationType eAnimationType)
{
    switch (eAnimationType) {
    case AnimationType::FADEOUT:
    {
        QGraphicsOpacityEffect *eff = new QGraphicsOpacityEffect(p_Parent);
        p_Aniatee->setGraphicsEffect(eff);
        QPropertyAnimation *a = new QPropertyAnimation(eff,"opacity");
        a->setDuration(5000);
        a->setKeyValueAt(0,1);

        a->setKeyValueAt(1,0);
        //a->setEndValue(1);
        a->setEasingCurve(QEasingCurve::InBack);
        a->start(QPropertyAnimation::DeleteWhenStopped);

        break;
    }
    default:
    {
        break;
    }
    }

}

