#include "SystemPreferenceTab.h"

#include <GenericWnd.h>
#include <QToolButton>
#include <QDesktopServices>
SystemPreferenceTab::SystemPreferenceTab(QWidget *pParent)
    :BasePreferenceTab(pParent)
{
    p_cDBLocation = NULL;
}

SystemPreferenceTab::~SystemPreferenceTab()
{

}

void SystemPreferenceTab::OnCreateCtrl()
{
    const QToolButton* pButtn = findChild<QToolButton*>("btn_OpenLogLocation");
    connect(pButtn, SIGNAL(clicked()),this, SLOT(OpenLogsLocation()));
    p_cDBLocation = findChild<QLineEdit*>("txt_RepDBLocation");
}

void SystemPreferenceTab::OnOpenSecDbConfig()
{
    QString sDirect = QFileDialog::getExistingDirectory(this, tr("DB Replicate Location"),
                                                    QDir::homePath(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);


    p_cDBLocation->setText(sDirect);
    p_cDBLocation->setToolTip(sDirect);
}

void SystemPreferenceTab::OnCopyDB()
{
    if(p_cDBLocation->text().isEmpty())
    {
        QMessageBox::information(this, "Empty", "Please select a secondary DB Location");
        return;
    }
    QSettings* pSet = p_Parent->GetApplication()->GetConfig();
    QString sDBLocation = pSet->value(DB_LOCATION).toString();
    sDBLocation += QString(QDir::separator()) += QString(DB_NAME);
            ;
    QFile oFile(sDBLocation);

    QString sNewPath = p_cDBLocation->text() += QString(QDir::separator()) += QString(DB_NAME);
    QString sMsg = QString("Replicating Data to %1 from %2").arg(sNewPath).arg(sDBLocation);
    QMessageBox::information(this,"Replicate Data",sMsg);
    if(!oFile.copy(sNewPath))
    {
        QString sError = oFile.errorString();
        QMessageBox::information(this, "Error", sError);
    }

}

void SystemPreferenceTab::OpenLogsLocation()
{
    QLineEdit* pLineEdit = findChild<QLineEdit*>("txt_LogLocation");
    QDesktopServices::openUrl(QUrl(pLineEdit->text()));
}

