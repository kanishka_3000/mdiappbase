/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENTITYMANIPULATERCTRL_H
#define ENTITYMANIPULATERCTRL_H

#include <QObject>
#include <GenericCtrl.h>
#include <entity/Entity.h>
#include <QLineEdit>
#include <QComboBox>
class EntityManipulaterSaveDislpayer
{
public:
    virtual void EntitySaved(bool bEdit){(void)bEdit;}
    virtual void CustomizeExistMsg(QString& sMsg){(void)sMsg;}
};

class EntityManipulaterCtrl: public GenericCtrl
{
    Q_OBJECT
public:
    enum  PopulationFeedback
    {
        EMPTY
    };

    class PopulationCallback
    {
    public:
        friend EntityManipulaterCtrl;
        PopulationCallback()
        {
            p_Publisher = nullptr;
        }

        virtual void OnPopulation(PopulationFeedback eType, QString sFieldName, QWidget* pWidget) = 0;
        virtual ~PopulationCallback()
        {
            if(p_Publisher == nullptr)
                return;
            foreach(PopulationFeedback eType, set_Types)
            {
                p_Publisher->UnsubscribeForFeedback(eType ,this);
            }
        }

    private:
        virtual void SetPublisher(EntityManipulaterCtrl* pPublisher,PopulationFeedback eType)
        {
            p_Publisher = pPublisher;
            set_Types.insert(eType);
        }
        void RemovePublisher()
        {
            p_Publisher = nullptr;
        }

        QSet<PopulationFeedback> set_Types;
        EntityManipulaterCtrl* p_Publisher;
    };

    EntityManipulaterCtrl(QWidget* pParent);
    ~EntityManipulaterCtrl();
    void SetEntityType(QString sEntityType){s_EntityType = sEntityType;}
     virtual void OnCreateCtrl();

    virtual void OnPostCreateCtrl();

    virtual void FillEntityWithData(QVariant oData, std::shared_ptr<Entity> pEntity, bool bUpdate, QString sField);
    //Provide an entity in so that fields will be populated with its data
    //couples the class with entity object, simplifies the design for a small project
    virtual void PopulateWithEntity(std::shared_ptr<Entity> pEntity, bool bEditMode = true);
    //Get Data from the control and set data as update data on to the entity.
    //couples the class with entity object, simplifies the design for a small project
    virtual void PopulateEntityWithData(std::shared_ptr<Entity> pEntity, bool bUpdate);

    virtual bool CustomValidations(QString& sError){(void)sError;return true;}


    //Calling will prepend//Copy of// text to all text fields
    virtual void AdjustFieldsAsCopy();

    void SetDefaultDisplayer(EntityManipulaterSaveDislpayer* pDefautlSaveDisplayer)
    {
        p_DefaultDisplayer = pDefautlSaveDisplayer;
    }
    virtual void SubscribeForFeedback(PopulationFeedback eType, PopulationCallback* pSubscriber);
    virtual void UnsubscribeForFeedback(PopulationFeedback eType, PopulationCallback* pSubscriber);
    void SetClearFieldsWhenSave(bool value);

signals:
    void NotifyItemSaved(bool bEdit);
public slots:
    virtual bool OnSave(EntityManipulaterSaveDislpayer* pSaveDisplayer = nullptr);
protected:
    virtual void StoreDefaultData();
    virtual void RestoreDefaultData();
    virtual void OnPreSave(){}
    virtual void OnPostSave(){}
    std::shared_ptr<Entity> p_Entity;
    bool b_EditMode;
    QString s_EntityType;
    bool b_DeleteWhenDone;
    bool b_ClearFieldsWhenSave;
    QMap<QWidget*,QString> map_DefaultData;
    EntityManipulaterSaveDislpayer* p_DefaultDisplayer;

    QMap<PopulationFeedback, QSet<PopulationCallback*> > map_FeedbackSubscribers;
};

#endif // ENTITYMANIPULATERCTRL_H
