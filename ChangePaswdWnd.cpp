#include "ChangePaswdWnd.h"
#include "ui_ChangePaswdWnd.h"
#include <Application.h>
#include <User.h>
#include <QMessageBox>
ChangePaswdWnd::ChangePaswdWnd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangePaswdWnd)
{
    ui->setupUi(this);
}

ChangePaswdWnd::~ChangePaswdWnd()
{
    delete ui;
}

void ChangePaswdWnd::OnChangePw()
{
    QString sOldPassowrd =  ui->txt_OldPassword->text();
   bool bPass = Application::GetApplication()->GetUser()->IsPasswordCorrect(sOldPassowrd);
   if(!bPass)
   {
       QMessageBox::warning(this,"Invalid","Invalid origianl password");
       return;
   }
   QString sPass1 = ui->txt_NewPassword->text();
   QString sPass2 = ui->txt_NewPassword2->text();

   if(sPass1 != sPass2)
   {
       QMessageBox::warning(this,"No match","Two passwords do not match");
       return;
   }
   Application::GetApplication()->ChangePassword(sPass1);
   deleteLater();
}
