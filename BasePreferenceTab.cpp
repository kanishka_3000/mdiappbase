#include "BasePreferenceTab.h"
#include <globaldefs.h>
#include <QLineEdit>
#include <QCheckBox>
#include <QListWidget>
#include <QDebug>
#include <Util.h>
#include <GenericWnd.h>
BasePreferenceTab::BasePreferenceTab(QWidget *parent) :
    GenericCtrl(parent)
{

}

void BasePreferenceTab::Initialize(QSettings *pSettings)
{
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pChild, lstChildren)
    {
        QString sPrefName = pChild->property(PREF_NAME).toString();
        if(sPrefName == "")
        {
            continue;
        }

       // if(!pSettings->contains(sPrefName))
        //{
          //  continue;
        //}
        QVariant oData = pSettings->value(sPrefName);
        //Populate each item
/*        if(pChild->metaObject()->className() == QString("QLineEdit"))
        {
            QLineEdit* pLineEdit = static_cast<QLineEdit*>(pChild);
            pLineEdit->setText(oData.toString());
            pLineEdit->setToolTip(oData.toString());
        }
        else*/ if(pChild->metaObject()->className() == QString("QCheckBox"))
        {
            QCheckBox* pCheckbox = static_cast<QCheckBox*>(pChild);
            bool bCheck = oData.toBool();
            pCheckbox->setChecked(bCheck);
        }
        else if(pChild->metaObject()->className() == QString("QListWidget"))
        {
            QListWidget* pWidget = static_cast<QListWidget*>(pChild);
            QStringList sList = Util::GetSettingsList(pSettings,sPrefName);
            foreach(QString sValue, sList)
            {
                if(sValue.isEmpty())
                    continue;

                QListWidgetItem* pItem = new QListWidgetItem(pWidget);
                pItem->setText(sValue);
                //pWidget->addItem(pItem);
            }

        }
        else
        {
            SetWidgetText(pChild,oData.toString());
            pChild->setToolTip(oData.toString());
        }

    }
}

void BasePreferenceTab::OnSave(QSettings *pSettings)
{

    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pChild, lstChildren)
    {
        QString sPrefName = pChild->property(PREF_NAME).toString();

        if(sPrefName == "")
        {
            continue;
        }
        p_Parent->GetApplication()->GetLogger()->WriteLog("Saving preference for %s", qPrintable(sPrefName));
        QVariant oData = pSettings->value(sPrefName);
        QString sText;
        //Populate each item
   /*     if(pChild->metaObject()->className() == QString("QLineEdit"))
        {
            QLineEdit* pLineEdit = static_cast<QLineEdit*>(pChild);
            QString sText = pLineEdit->text();
            if(sText == oData.toString())
                continue;

            pSettings->setValue(sPrefName,sText);
        }
        else */if(pChild->metaObject()->className() == QString("QCheckBox"))
        {
            QCheckBox* pCheckbox = static_cast<QCheckBox*>(pChild);
            bool bCheck = (pCheckbox->checkState() == Qt::Checked);
           pSettings->setValue(sPrefName,bCheck);
        }
        else if(pChild->metaObject()->className() == QString("QListWidget"))
        {
            QListWidget* pWidget = static_cast<QListWidget*>(pChild);
            int iSize = pWidget->count();
            pSettings->beginWriteArray(sPrefName,iSize);
            for(int i = 0 ;i < iSize; i++)
            {
                pSettings->setArrayIndex(i);
                QString sKey = QString("%1").arg(i);
                QString sValue = pWidget->item(i)->text();

                pSettings->setValue(sKey,sValue);
            }
            pSettings->endArray();
        }
        else
        {
            sText = GetWidgetText(pChild);
            if(sText == oData.toString())
                continue;

             pSettings->setValue(sPrefName,sText);
        }
    }

}

void BasePreferenceTab::SetPrefName(QWidget *pWidget, QString sPrefName)
{
    pWidget->setProperty(PREF_NAME, sPrefName);
}

