/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef BASEPREFERENCETAB_H
#define BASEPREFERENCETAB_H

#include <QWidget>
#include <QSettings>
#include <GenericCtrl.h>
class BasePreferenceTab : public GenericCtrl
{
    Q_OBJECT
public:
    explicit BasePreferenceTab(QWidget *parent = 0);
    void Initialize(QSettings* pSettings);
    void OnSave(QSettings* pSettings);

protected:
    void SetPrefName(QWidget* pWidget, QString sPrefName);
signals:

public slots:
};

#endif // BASEPREFERENCETAB_H
