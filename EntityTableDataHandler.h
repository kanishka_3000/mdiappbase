/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ENTITYTABLEDATAHANDLER_H
#define ENTITYTABLEDATAHANDLER_H

#include <GenericTableModel.h>
#include <entity/Entity.h>
class EntityTableDataHandler: public GenericTableDataCalback
{
public:
    EntityTableDataHandler();
     virtual void GetRow(const WindowConfig* pWindowConfig, std::shared_ptr<void> pData, QVector<QVariant>& rData);
};

#endif // ENTITYTABLEDATAHANDLER_H
