#include "Login.h"
#include <Connection.h>
#include <globaldefs.h>
#include <User.h>
#include <memory>
#include <entity/Entity.h>
Login::Login()
{

}

bool Login::Authenticate(QString sUsername, QString sPassword, std::shared_ptr<User>& prUser)
{
    QString sQuery = QString("select * from login where username = '%1' and password = '%2'  ").arg(sUsername).arg(sPassword);
    QList<std::shared_ptr<Entity>> oLogin;
    Entity::EntityDataHander oDataHanlder( ENTITY_USER, oLogin );
    Connection::GetInstance()->GerRecords(sQuery, oDataHanlder);

    if(oLogin.size()  != 1)
    {
        return false;
    }

    std::shared_ptr<User> pUsr  = std::dynamic_pointer_cast<User>(oLogin.first());
    prUser = pUsr;
    if(!pUsr)
    {
        Q_ASSERT(false);
    }
    if(pUsr->GetUserName() != sUsername)
    {
        return false;
    }
    if(!pUsr->IsPasswordCorrect(sPassword))
    {
        return false;
    }
    pUsr->ResolveReferences();
    return true;
}

