/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef REPORTMANAGER_H
#define REPORTMANAGER_H
#include <QSettings>
#include <QObject>
#include <QSet>
#include <memory>
#include <assert.h>
#include <reporting/ReportableModel.h>
#define __ENABLE_REPORTING_
namespace reporting{
class ReportLogger
{
public:
    virtual void LogReportMsg(QString sLog) = 0;
};
}

namespace reporting{

class ReportManager : public QObject
{
    Q_OBJECT
public:
    explicit ReportManager(QString sFileName, QSettings* pConfig,QObject *parent = 0);
    void RegisterConfigField(QString sConfigField);
    void RegisterExternalField(QString sExternalField);

    void RegisterModel(ReportableModel* pReporableModel);
    void Initialize();
    void HandleModelData(const QString constext, const int iRowcount, QVariant &vData);

    void SetLogger(ReportLogger* pLogger);
    void SetExternlaDataProvider(ReportDataProvider* pDataProvider);
protected:
    void LogMsg(QString sLogMsg);
    void InitializeFileFields();
signals:

public slots:
        void GetReportData(const int iRowcount, const QString constext, QVariant& vData, const int ival);
        void GetValueImage(const int val1, const QString svalue, QImage& image, const int val2);
private:
    QSet<QString> set_ConfigFields;
    QSet<QString> set_ExternalFields;

    QSettings* p_Config;
    QString s_ReportName;
    ReportableModel* p_Model;
    ReportDataProvider* p_DataProvider;
    std::shared_ptr<ReportData> p_CurrentReportData;
    QMap<QString,QString> map_FileFieldData;
    ReportLogger* p_Logger;
    int i_CurrentRow;
};
}
#endif // REPORTMANAGER_H
