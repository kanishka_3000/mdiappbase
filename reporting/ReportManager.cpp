/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "ReportManager.h"
#ifdef __ENABLE_REPORTING_
#include <qtrpt.h>
#endif
reporting::ReportManager::ReportManager(QString sFileName, QSettings *pConfig, QObject *parent)
    : QObject(parent),p_Config(pConfig),s_ReportName(sFileName),i_CurrentRow(-1)
{
    p_Model = nullptr;
    p_DataProvider = nullptr;
    p_Logger = nullptr;
    i_CurrentRow = -1;
}

void reporting::ReportManager::RegisterConfigField(QString sConfigField)
{
    set_ConfigFields.insert(sConfigField);
}

void reporting::ReportManager::RegisterExternalField(QString sExternalField)
{
    set_ExternalFields.insert(sExternalField);
}

void reporting::ReportManager::RegisterModel(reporting::ReportableModel *pReporableModel)
{
    p_Model = pReporableModel;
}

void reporting::ReportManager::Initialize()
{
    #ifdef __ENABLE_REPORTING_


    std::unique_ptr<QtRPT> pRpt(new QtRPT(this));
    QString sReportName = QString("Reports/%1.xml").arg(s_ReportName);
    pRpt->loadReport(sReportName);
    if(p_Model)
        pRpt->recordCount << p_Model->GetRowCount();


    connect(pRpt.get(),SIGNAL(setValue(int,QString,QVariant&,int)),this,SLOT(GetReportData(int,QString,QVariant&,int)));
    connect(pRpt.get(), SIGNAL(setValueImage(const int, const QString, QImage&, const int)),this, SLOT(GetValueImage(int,QString,QImage&,int)));

    pRpt->printExec();
#endif
}

void reporting::ReportManager::HandleModelData(const QString constext, const int iRowcount, QVariant &vData)
{
    if(i_CurrentRow != iRowcount)
    {
        i_CurrentRow = iRowcount;
        p_CurrentReportData = p_Model->GetReportDataRow(iRowcount);

    }
    QString sData;
    QVariant vInterData = p_CurrentReportData->GetReportDataItem(constext);
    if(vInterData.type() == QVariant::Double)
    {
        double dData = vInterData.toDouble();
        sData = sData.sprintf("%.2f",dData);
    }
    else
    {
        sData = vInterData.toString();
    }
    vData = sData;
}

void reporting::ReportManager::SetExternlaDataProvider(reporting::ReportDataProvider *pDataProvider)
{
    p_DataProvider = pDataProvider;
}

void reporting::ReportManager::LogMsg(QString sLogMsg)
{
    if(p_Logger == nullptr)
        return;

    p_Logger->LogReportMsg(sLogMsg);
}

void reporting::ReportManager::InitializeFileFields()
{
    QString sReportName = QString("Reports/%1.txt").arg(s_ReportName);
    QFile oFile(sReportName);
    if(!oFile.open(QIODevice::ReadOnly))
    {
        return;
    }
    QString sKey;
    QString sValue;
    QString sLine;
    while(oFile.canReadLine())
    {
          sLine = oFile.readLine();
          QStringList oList = sLine.split("=");
          sKey = oList.at(0);
          sValue = oList.at(1);
          map_FileFieldData.insert(sKey, sValue);
    }
}

void reporting::ReportManager::GetReportData(const int iRowcount, const QString constext, QVariant &vData, const int ival)
{
    (void)ival;
    if(set_ConfigFields.contains(constext))
    {
        vData = p_Config->value(constext);
    }
    else if(map_FileFieldData.contains(constext))
    {
        vData = map_FileFieldData.value(constext);
    }
    else if(set_ExternalFields.contains(constext))
    {
        assert(p_DataProvider);
        vData = p_DataProvider->ProvideReportData(constext);
    }
    else
    {
        HandleModelData(constext, iRowcount, vData);
    }
}

void reporting::ReportManager::GetValueImage(const int val1, const QString svalue, QImage &image, const int val2)
{
    (void)val1;(void)svalue;
    (void)val2;
    QImage* pImage = new QImage("Reports/Title.png");
    image = *pImage;
}

