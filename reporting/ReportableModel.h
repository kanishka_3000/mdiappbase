/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef REPORTABLEMODEL_H
#define REPORTABLEMODEL_H

#include <QVariant>
#include <QString>
#include <memory>
namespace reporting {
class ReportData
{
public:
    virtual QVariant GetReportDataItem(QString sColumn) = 0;

};
class ReportDataProvider
{
public:
    virtual QVariant ProvideReportData(QString sFieldName) = 0;
    virtual QImage* ProvideImageData(QString sFieldName) {(void)sFieldName;return nullptr;}
};

class ReportableModel
{
public:
    ReportableModel(){}
    virtual std::shared_ptr<ReportData> GetReportDataRow(int iRow) = 0;
    virtual int GetRowCount() = 0;
};

}
#endif // REPORTABLEMODEL_H
