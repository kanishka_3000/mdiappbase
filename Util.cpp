#include "Util.h"
#include <globaldefs.h>
#include <QDateEdit>
#include <QTextEdit>
#include <QComboBox>
#include <QLineEdit>
#include <QDateTime>
#include <QPair>
Util::Util()
{

}

QMap<QString, QVariant> Util::CollectFields(QWidget *pParent)
{
    QMap<QString,QVariant> mapValues;
    QList<QWidget*> lstChildren = pParent->findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

        if(pWidget->metaObject()->className() == QString("QLineEdit"))
        {
            QLineEdit* pLIneEdit = static_cast<QLineEdit*>(pWidget);
            mapValues.insert(sField, pLIneEdit->text());
        }
    }

    return mapValues;
}

QStringList Util::GetSettingsList(QSettings *pSettings, QString sProperty)
{
    QStringList sList;
    int iSize = pSettings->beginReadArray(sProperty);
    for(int i = 0 ;i < iSize; i++)
    {
        pSettings->setArrayIndex(i);
        QString sKey = QString("%1").arg(i);
        QString sValue = pSettings->value(sKey).toString();
        sList.append(sValue);
    }
    pSettings->endArray();
    return sList;
}

QString Util::CreatePersistString(QList<QPair<int, int> > lstData)
{
    QStringList oItemList;
    for(auto itr = lstData.begin(); itr != lstData.end();itr++)
    {
        QPair<int,int> oDataItem = *itr;
        QString sCouple = QString("%1_%2").arg(oDataItem.first).arg(oDataItem.second);
        oItemList.push_back(sCouple);
    }
    QString sReturnString = oItemList.join("|");
    return sReturnString;


}

QList<QPair<int, int> > Util::LoadFromPeristData(QString sData)
{
    QList<QPair<int, int>> oItems;
    QStringList oItemList = sData.split("|");
    foreach(QString sCouple, oItemList)
    {
        QStringList oItem = sCouple.split("_");
        int iFirst = oItem.at(0).toInt();
        int iSecond = oItem.at(1).toInt();

        QPair<int, int> oPair(iFirst,iSecond);
        oItems.push_back(oPair);
    }
    return oItems;
}

QString Util::DoubleToString(double dValue)
{
    QString sData;
    sData = sData.sprintf("%.2f",dValue);
    return sData;
}

void Util::CollectCheckState(QCheckBox *pCheckbox, int iVal, QSet<int> &setCollecter)
{
    if(pCheckbox->isChecked())
    {
        setCollecter.insert(iVal);
    }
}

qint64 Util::GetUnique()
{
    return (QDateTime::currentMSecsSinceEpoch()/1000);
}



