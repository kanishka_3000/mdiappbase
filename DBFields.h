/***************************This is auto generated************************************
***************Kanishka Weerasekara (c)-- kkgweerasekara@gmail.com*******************
*************************************************************************************/
#ifndef __DB_FIELDS__ 
#define __DB_FIELDS__ 
//Table name for ROLE
#define TBLNM_APP_ROLE	"Role" 
//Table fields for ROLE 
#define FDNM_APP_ROLE_ROLENAME 	 "rolename"
#define FDNM_APP_ROLE_ROLEDESCRIPTION 	 "roledescription"
//Table name for PRIVILEDGE
#define TBLNM_APP_PRIVILEDGE	"Priviledge" 
//Table fields for PRIVILEDGE 
#define FDNM_APP_PRIVILEDGE_PRICODE 	 "pricode"
#define FDNM_APP_PRIVILEDGE_NAME 	 "name"
#define FDNM_APP_PRIVILEDGE_PREVDESCRIPTION 	 "prevdescription"
//Table name for LOGIN
#define TBLNM_APP_LOGIN	"Login" 
//Table fields for LOGIN 
#define FDNM_APP_LOGIN_USERNAME 	 "username"
#define FDNM_APP_LOGIN_PASSWORD 	 "password"
#define FDNM_APP_LOGIN_NAME 	 "name"
#define FDNM_APP_LOGIN_ROLENAME 	 "rolename"
//Table name for ROLE_PRIVILEDGE
#define TBLNM_APP_ROLE_PRIVILEDGE	"Role_Priviledge" 
//Table fields for ROLE_PRIVILEDGE 
#define FDNM_APP_ROLE_PRIVILEDGE_ROLENAME 	 "rolename"
#define FDNM_APP_ROLE_PRIVILEDGE_PRICODE 	 "pricode"
#endif //  __DB_FIELDS__ 
