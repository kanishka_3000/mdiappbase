/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "EntityGroupTableWnd.h"
#include "ui_EntityGroupTableWnd.h"
#include <entity/Entity.h>
#include <EntityGroupTableCtrl.h>
EntityGroupTableWnd::EntityGroupTableWnd(MainWnd* pWnd, QString sTitle) :
   GenericWnd(pWnd,sTitle)
{
    setupUi(this);
    b_AcceptChanges = true;
}

EntityGroupTableWnd::~EntityGroupTableWnd()
{

}

void EntityGroupTableWnd::OnCreate()
{
    connect(btn_Select,SIGNAL(clicked(bool)),this,SLOT(OnSelect()));
    connect(btn_SelectAndClose,SIGNAL(clicked(bool)),this,SLOT(OnSelectAndClose()));
}

void EntityGroupTableWnd::Initialize(GroupingParameters &oGroupingParams, bool bAcceptChange)
{
    b_AcceptChanges = bAcceptChange;
    QList<std::shared_ptr<Entity>> oList;
    oList = Entity::FindAllInstances(oGroupingParams.s_GroupEntity);
    foreach(std::shared_ptr<Entity> pEntity, oList)
    {
        QString sDesctiption = pEntity->GetData(oGroupingParams.s_FLDGroupDescription).toString();
        int iID = pEntity->GetData(oGroupingParams.s_FLDGroupID).toInt();
        map_Groups.insert(iID,pEntity);

        EntityGroupTableCtrl* pCtrl = new EntityGroupTableCtrl(this);

        OnNewChild(pCtrl);
        connect(pCtrl,SIGNAL(NotifyAddNew(int)),this,SLOT(OnAddNew(int)));
        connect(pCtrl,SIGNAL(NotifyEdit(std::shared_ptr<Entity>)),this, SLOT(OnEdit(std::shared_ptr<Entity>)));
        connect(pCtrl,SIGNAL(NotifyCopy(std::shared_ptr<Entity>)),this, SLOT(OnCopy(std::shared_ptr<Entity>)));
        pCtrl->Initialize(oGroupingParams,iID,b_AcceptChanges);
        p_Tab->addTab(pCtrl,sDesctiption);

    }

    if(!b_AcceptChanges)
    {
        //This is opened from the order entry
    }
    else
    {
        //opened to add items
        btn_SelectAndClose->hide();
        btn_Select->setText(tr("Close"));
    }
}

QList<std::shared_ptr<Entity>> EntityGroupTableWnd::GetChekedItems()
{
    QList<std::shared_ptr<Entity>> lstData;
    QList<EntityGroupTableCtrl*> lstChildren = findChildren<EntityGroupTableCtrl*>();
    foreach(EntityGroupTableCtrl* pCtrl, lstChildren)
    {
        pCtrl->GetCheckedEntities(lstData);
    }

    return lstData;
}

void EntityGroupTableWnd::Reload()
{
    QList<EntityGroupTableCtrl*> lstChildren = findChildren<EntityGroupTableCtrl*>();
    foreach(EntityGroupTableCtrl* pCtrl, lstChildren)
    {
        pCtrl->LoadData();
    }
}

void EntityGroupTableWnd::OnSelect()
{


    if(!b_AcceptChanges)
    {
        QList<std::shared_ptr<Entity>> lstChildren = GetChekedItems();
        NotifiyDataSelected(lstChildren,false);
    }
    else
    {
        //opened to add items
       RemoveWindow();
    }

}

void EntityGroupTableWnd::OnSelectAndClose()
{
    QList<std::shared_ptr<Entity>> lstChildren = GetChekedItems();
    NotifiyDataSelected(lstChildren,true);
    DeleteWindow();
}

void EntityGroupTableWnd::OnAddNew(int iGrouID)
{
    OnAddNew(map_Groups.value(iGrouID));
}

void EntityGroupTableWnd::OnEdit(std::shared_ptr<Entity> pEntity)
{
    (void)pEntity;
}

void EntityGroupTableWnd::OnCopy(std::shared_ptr<Entity> pEntity)
{
    (void)pEntity;
}
