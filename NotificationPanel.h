/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONPANEL_H
#define NOTIFICATIONPANEL_H

#include <QWidget>
#include "ui_NotificationPanel.h"
#include <GenericCtrl.h>
class NotificationPanel : public GenericCtrl, public Ui::NotificationPanel
{
    Q_OBJECT

public:
    explicit NotificationPanel(QWidget *parent = 0);
    ~NotificationPanel();
    void SetNotification(QString sNotificaion);
private:

};

#endif // NOTIFICATIONPANEL_H
