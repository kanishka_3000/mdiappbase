/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTITYGROUPTABLECTRL_H
#define ENTITYGROUPTABLECTRL_H

#include <QWidget>
#include <ui_EntityGroupTableCtrl.h>
#include <GenericCtrl.h>
#include <EntityGroupTableWnd.h>
#include <GenericTableModel.h>
#include <EntityTableDataHandler.h>
class EntityGroupTableCtrl : public GenericCtrl, public Ui::EntityGroupTableCtrl
{
    Q_OBJECT

public:
    explicit EntityGroupTableCtrl(QWidget *parent = 0);
    ~EntityGroupTableCtrl();

    void OnCreateCtrl();
    virtual void Initialize(GroupingParameters& oParams, int ID, bool bAceptChanges = true);
    int GetGroupID(){return i_GroupID;}

    void GetCheckedEntities(QList<std::shared_ptr<Entity> > &lstEntities);
    void LoadData();
signals:
    void NotifyAddNew(int iGroupID);
    void NotifyEdit(std::shared_ptr<Entity> pEntity);
    void NotifyCopy(std::shared_ptr<Entity> pEntity);
protected:
    void contextMenuEvent(QContextMenuEvent *event) ;
    std::shared_ptr<Entity> GetSelectedData();
    bool b_AcceptChanges;
protected slots:
    virtual void OnAddNew();
    virtual void OnEdit();
    virtual void OnCopy();
private:
    GenericTableModel* p_TableModel;
    EntityTableDataHandler o_TableHandler;
    WindowConfig o_TableConfig;
    int i_GroupID;
    GroupingParameters* p_Params;
};

#endif // ENTITYGROUPTABLECTRL_H
