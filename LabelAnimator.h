/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef LABELANIMATOR_H
#define LABELANIMATOR_H
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QLabel>
class LabelAnimator
{
public:
    enum class AnimationType
    {
        FADEOUT
    };

    LabelAnimator(QLabel* pLabel,QObject* pParent);
    void Animate(AnimationType eAnimationType);
private:
    QLabel* p_Aniatee;
    QObject* p_Parent;
};

#endif // LABELANIMATOR_H
