/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CHANGEPASWDWND_H
#define CHANGEPASWDWND_H

#include <QDialog>

namespace Ui {
class ChangePaswdWnd;
}

class ChangePaswdWnd : public QDialog
{
    Q_OBJECT

public:
    explicit ChangePaswdWnd(QWidget *parent = 0);
    ~ChangePaswdWnd();
public slots:
    virtual void OnChangePw();
private:
    Ui::ChangePaswdWnd *ui;
};

#endif // CHANGEPASWDWND_H
