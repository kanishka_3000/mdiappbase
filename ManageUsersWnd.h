/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef MANAGEUSERSWND_H
#define MANAGEUSERSWND_H

#include <QWidget>
#include "ui_ManageUsersWnd.h"
#include <GenericWnd.h>

class ManageUsersWnd : public GenericWnd,public Ui::ManageUsersWnd
{
    Q_OBJECT

public:
    explicit ManageUsersWnd(MainWnd *parent ,QString sTitle);
    ~ManageUsersWnd();
    void OnChildRemoveRequest(GenericCtrl *pChild);
public slots:
    virtual void OnEditRequest(QString sKeyValue);
    virtual void AddNewUser();

private:
    AddUserCtrl* p_EditCtrl;
};

#endif // MANAGEUSERSWND_H
