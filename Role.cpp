#include "Role.h"
#include <QDebug>
#include <Application.h>

Role::Role()
{
    s_PersistatName = TBLNM_APP_ROLE;
    s_DefName = ENTITY_ROLE;
    s_Key = FDNM_APP_ROLE_ROLENAME;
}



void Role::ResolveReferences()
{
    QList<std::shared_ptr<Entity>> lstPriviledges= FindRelatedEntities(p_Def->s_PersistantName,ENTITY_PRIVILEDGE,GetRoleName());

    foreach(std::shared_ptr<Entity> pEt,lstPriviledges)
    {
        std::shared_ptr<Priviledge> pPriviledge = std::dynamic_pointer_cast<Priviledge>(pEt);
        map_Priviledges.insert(pPriviledge->GetData(FLD_PRICODE).toInt(),pPriviledge);
    }
    Application::GetApplication()->GetLogger()->WriteLog("%d number of priviledges were loaded",map_Priviledges.size());

}

QString Role::GetRoleName()
{
    return GetData(FDNM_APP_ROLE_ROLENAME).toString();
}

bool Role::HasPriviledge(int iPreviCode)
{
    return map_Priviledges.contains(iPreviCode);
}

