/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef USER_H
#define USER_H
#include <entity/Entity.h>
#include <globaldefs.h>
#include <Role.h>

class User: public Entity
{
public:
    User();
    virtual ~User();
    QString GetUserName();
    bool IsPasswordCorrect(QString sTest);
    void ChangePassword(QString sPassword);
    virtual void InitlizeDefs();
    virtual void ResolveReferences();
    std::shared_ptr<Role> p_Role;
protected:
    static bool b_DefInitlized;
};

#endif // USER_H
