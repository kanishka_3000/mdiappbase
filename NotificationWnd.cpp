/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "NotificationWnd.h"
#include <QVBoxLayout>

NotificationWnd::NotificationWnd(MainWnd *pMainWnd, QString sTitle):
    GenericWnd(pMainWnd, sTitle),p_NotificationCtrl(nullptr)
{
    setLayout(new QVBoxLayout(this));

    layout()->setContentsMargins(1,1,1,1);
    layout()->setSpacing(1);

    p_NotificationCtrl = new NotificationCtrl(this);
    layout()->addWidget(p_NotificationCtrl);

}

void NotificationWnd::Initialize(NotificationPopulator *pPopulator)
{
    p_NotificationCtrl->Initialize(pPopulator);
}

