/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "EntityGroupTableCtrl.h"
#include "ui_EntityGroupTableCtrl.h"
#include <entity/Entity.h>
#include <QMenu>
#include <QContextMenuEvent>
EntityGroupTableCtrl::EntityGroupTableCtrl(QWidget *parent) :
  GenericCtrl(parent)
{
    setupUi(this);
    b_AcceptChanges = true;
    i_GroupID = -1;
    p_Params = nullptr;
}

EntityGroupTableCtrl::~EntityGroupTableCtrl()
{

}

void EntityGroupTableCtrl::OnCreateCtrl()
{
    o_TableConfig = p_Parent->GetWindowTableConfiguration();

    p_TableModel = new GenericTableModel(this, &o_TableConfig);
    p_TableModel->SetDataCalback(&o_TableHandler);
    p_View->setModel(p_TableModel);
    connect(p_ActAdd_New,SIGNAL(triggered(bool)),this,SLOT(OnAddNew()));
    connect(p_ActEdit_Item,SIGNAL(triggered(bool)),this,SLOT(OnEdit()));
    connect(p_ActCopy_Item,SIGNAL(triggered(bool)),this,SLOT(OnCopy()));
    connect(btn_AddNew,SIGNAL(clicked(bool)),this,SLOT(OnAddNew()));
    connect(btn_Edit,SIGNAL(clicked(bool)),this, SLOT(OnEdit()));
    connect(btn_CopyItem,SIGNAL(clicked(bool)),this, SLOT(OnCopy()));
}

void EntityGroupTableCtrl::LoadData()
{
    QList<std::shared_ptr<Entity>> oList = Entity::FindInstances(p_Params->s_GroupColumn, QVariant(i_GroupID).toString(), p_Params->s_DataTable);
    foreach(std::shared_ptr<Entity> pEntity, oList)
    {
         p_TableModel->InsertRow(pEntity->GetData(p_Params->s_DataTableKey).toString(),pEntity);
    }
}

void EntityGroupTableCtrl::Initialize(GroupingParameters &oParams, int ID, bool bAceptChanges)
{
    i_GroupID = ID;
    p_Params = &oParams;
    b_AcceptChanges = bAceptChanges;
    p_TableModel->SetReadOnly(b_AcceptChanges);
    LoadData();

    if(!b_AcceptChanges)
    {
       p_EditablePanel->setVisible(false);
    }
    else
    {
        //opened to add items

    }
}

void EntityGroupTableCtrl::GetCheckedEntities(QList<std::shared_ptr<Entity> > &lstEntities)
{
    QList<std::shared_ptr<void>> lstData;
    p_TableModel->GetCheckedData(lstData);
    foreach(std::shared_ptr<void> pData, lstData)
    {
        std::shared_ptr<Entity> pEntity = std::static_pointer_cast<Entity>(pData);
        lstEntities.push_back(pEntity);
    }
}

void EntityGroupTableCtrl::contextMenuEvent(QContextMenuEvent *event)
{
     QMenu oMenu(this);
    if(b_AcceptChanges)
    {

        oMenu.addAction(p_ActAdd_New);
        if(p_View->selectionModel()->selectedRows(0).size() > 0)
        {
            oMenu.addAction(p_ActEdit_Item);
            oMenu.addAction(p_ActCopy_Item);
        }

    }
     oMenu.exec(event->globalPos());

}

std::shared_ptr<Entity> EntityGroupTableCtrl::GetSelectedData()
{
    QModelIndexList oList =p_View->selectionModel()->selectedRows(0);
    if(oList.size() == 0)\
        return NULL;

    QModelIndex oSelectedIndex = oList.first();
    int iRow = oSelectedIndex.row();

    std::shared_ptr<void> pData = p_TableModel->GetData(iRow);
    if(!pData)
    {
        return NULL;
    }
    std::shared_ptr<Entity> pEntity = std::static_pointer_cast<Entity>(pData);
    return pEntity;

}

void EntityGroupTableCtrl::OnAddNew()
{
    NotifyAddNew(i_GroupID);
}

void EntityGroupTableCtrl::OnEdit()
{
    std::shared_ptr<Entity> pEntity = GetSelectedData();
    emit NotifyEdit(pEntity);
}

void EntityGroupTableCtrl::OnCopy()
{
     std::shared_ptr<Entity> pEntity = GetSelectedData();
     emit NotifyCopy(pEntity);

}
