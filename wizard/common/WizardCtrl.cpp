/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "WizardCtrl.h"

wizard::common::WizardCtrl::WizardCtrl(QWidget *parent) : GenericCtrl(parent)
{

}

void wizard::common::WizardCtrl::OnForward(QMap<QString, QVariant> mapData)
{
    map_Data = mapData;
}

void wizard::common::WizardCtrl::OnRetreat()
{

}

QMap<QString, QVariant> wizard::common::WizardCtrl::GetData()
{
    return QMap<QString,QVariant>();
}

