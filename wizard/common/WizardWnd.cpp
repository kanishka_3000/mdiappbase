/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "WizardWnd.h"
#include "ui_WizardWnd.h"

WizardWnd::WizardWnd(MainWnd *pParent, QString sTitle) :
    GenericWnd(pParent, sTitle),
    ui(new Ui::WizardWnd),i_CurrentIndex(0),i_TotalItems(0)
{
    ui->setupUi(this);
}

WizardWnd::~WizardWnd()
{
    delete ui;
}

void WizardWnd::OnCreate()
{
    connect(ui->btn_Next, SIGNAL(clicked(bool)), this, SLOT(OnNext()));
    connect(ui->btn_Prev, SIGNAL(clicked(bool)), this, SLOT(OnPrev()));
    connect(ui->btn_Cancel, SIGNAL(clicked(bool)),this , SLOT(deleteLater()));
}

void WizardWnd::AddWizardCtrl(wizard::common::WizardCtrl *pCtrl)
{
    ui->st_Stack->addWidget(pCtrl);
    vec_Ctrls.push_back(pCtrl);
    i_TotalItems++;
}

void WizardWnd::OnNext()
{
    if(i_CurrentIndex == i_TotalItems)
        return;

    QMap<QString, QVariant> mapData = vec_Ctrls.at(i_CurrentIndex)->GetData();
    i_CurrentIndex++;
    vec_Ctrls.at(i_CurrentIndex)->OnForward(mapData);
    ui->st_Stack->setCurrentIndex(i_CurrentIndex);

}

void WizardWnd::OnPrev()
{

}
