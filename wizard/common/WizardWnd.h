/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef WIZARDWND_H
#define WIZARDWND_H

#include <QWidget>
#include <QVariant>
#include <GenericWnd.h>
#include <wizard/common/WizardCtrl.h>
namespace Ui {
class WizardWnd;
}

class WizardWnd : public GenericWnd
{
    Q_OBJECT

public:
    explicit WizardWnd(MainWnd* pParent, QString sTitle);
    ~WizardWnd();
    virtual void OnCreate();
    void AddWizardCtrl(wizard::common::WizardCtrl* pCtrl);
public slots:
    void OnNext();
    void OnPrev();

private:
    Ui::WizardWnd *ui;
    int i_CurrentIndex;
    int i_TotalItems;
    QVector<wizard::common::WizardCtrl*> vec_Ctrls;
};

#endif // WIZARDWND_H
