/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef WIZARDCTRL_H
#define WIZARDCTRL_H

#include <QObject>
#include <QWidget>
#include <QMap>
#include <QVariant>
#include <GenericCtrl.h>
namespace wizard { namespace common {

class WizardCtrl : public GenericCtrl
{
    Q_OBJECT
public:
    explicit WizardCtrl(QWidget *parent = 0);
    virtual void OnForward(QMap<QString, QVariant> mapData);
    virtual void OnRetreat();
    QMap<QString, QVariant> GetData();
signals:

public slots:

private:
    QMap<QString, QVariant> map_Data;
};

}}
#endif // WIZARDCTRL_H
