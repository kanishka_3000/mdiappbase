/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef SYSTEMPREFERENCETAB_H
#define SYSTEMPREFERENCETAB_H

#include <QObject>
#include <QFileDialog>
#include <BasePreferenceTab.h>
#include <QLineEdit>
#include <QDir>
#include <QSettings>
#include <QMessageBox>
class SystemPreferenceTab: public BasePreferenceTab
{
    Q_OBJECT

public:
    SystemPreferenceTab(QWidget* pParent = NULL);
    virtual ~SystemPreferenceTab();
    void OnCreateCtrl();
protected:
     QLineEdit* p_cDBLocation ;
public slots:
    virtual void OnOpenSecDbConfig();
    virtual void OnCopyDB();
    virtual void OpenLogsLocation();
};

#endif // SYSTEMPREFERENCETAB_H
