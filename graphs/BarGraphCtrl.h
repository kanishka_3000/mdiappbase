/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BARGRAPHCTRL_H
#define BARGRAPHCTRL_H

#include "ui_BarGraphCtrl.h"
#include <graphs/BarGraph.h>
#include <qwt_plot_barchart.h>
#include <qwt_scale_draw.h>
#define BAR_BOTTOM_SIZE 7
class BarGraphScale : public QwtScaleDraw
{
public:
    BarGraphScale(QVector<std::shared_ptr<graphs::BarGraphsPoint>>& vecPoints);
    virtual QwtText 	label (double value) const;
private:
    QVector<std::shared_ptr<graphs::BarGraphsPoint>>& vec_Points;
};

class BarGraphCtrl : public QWidget,public graphs::BarGraph, private Ui::BarGraphCtrl
{
    Q_OBJECT

public:
    explicit BarGraphCtrl(QWidget *parent = 0);
    virtual ~BarGraphCtrl();
    // BarGraph interface
public:
    virtual void AddGraphPoints(QVector<std::shared_ptr<graphs::BarGraphsPoint> > &vecPoints);
    virtual void SetTitle(QString sTitle);
    void Reset();
private:
    QwtPlotBarChart* p_BarChart;
    BarGraphScale* p_Scale;
     QVector<double> vec_Data;

};

#endif // BARGRAPHCTRL_H
