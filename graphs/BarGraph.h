/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef BARGRAPH
#define BARGRAPH
#include <QString>
#include <QVector>
#include <memory>
namespace graphs
{
class BarGraphsPoint
{
public:
    virtual ~BarGraphsPoint(){}
    virtual int GetCount() = 0;
    virtual QString GetTitle() = 0;
};

class BarGraph
{
public:
    virtual void AddGraphPoints(QVector<std::shared_ptr<BarGraphsPoint> > &vecPoints) = 0;
    virtual void SetTitle(QString sTitle) = 0;
};

}
#endif // BARGRAPH

