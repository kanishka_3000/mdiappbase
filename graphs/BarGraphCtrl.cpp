/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "BarGraphCtrl.h"
#include <math.h>
BarGraphCtrl::BarGraphCtrl(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    p_BarChart = new QwtPlotBarChart("Bar");
    QFont font = p_Plot->axisFont(QwtPlot::xBottom);
    font.setPointSize(BAR_BOTTOM_SIZE);
    p_Plot->setAxisFont(QwtPlot::xBottom, font);
    p_BarChart->attach(p_Plot);
    p_Scale = nullptr;
}

BarGraphCtrl::~BarGraphCtrl()
{
    delete p_BarChart;
   // delete p_Scale;
}


void BarGraphCtrl::AddGraphPoints(QVector<std::shared_ptr<graphs::BarGraphsPoint> > &vecPoints)
{

    p_Plot->setAxisAutoScale(QwtPlot::xBottom, false);
    p_Plot->setAxisScale(QwtPlot::xBottom, 0, vecPoints.size(), 1);

    p_Scale = new BarGraphScale(vecPoints);
    p_Plot->setAxisScaleDraw(QwtPlot::xBottom, p_Scale);
    vec_Data.clear();
    for(auto itr = vecPoints.begin(); itr != vecPoints.end(); itr++)
    {
        std::shared_ptr<graphs::BarGraphsPoint> pt = *itr;
        vec_Data.push_back(pt->GetCount());
    }
    p_BarChart->setSamples(vec_Data);



    p_Plot->updateAxes();
    p_Plot->replot();
}


void BarGraphCtrl::SetTitle(QString sTitle)
{
    p_BarChart->setTitle(sTitle);
}

void BarGraphCtrl::Reset()
{
  delete p_Scale;
}


BarGraphScale::BarGraphScale(QVector<std::shared_ptr<graphs::BarGraphsPoint> > &vecPoints):
    vec_Points(vecPoints)
{

}

QwtText BarGraphScale::label(double value) const
{
    QString sReturn("");
    double fractpart, intpart;
    fractpart = modf (value , &intpart);
    if ( qFuzzyCompare(fractpart, 0.0 ))
    {
        int iPart = intpart;
        if(iPart >= vec_Points.size())
        {
            return QString("");
        }

        sReturn = vec_Points[iPart]->GetTitle();
        sReturn.replace(" ", "<br/>");
        return sReturn;
    }
    else
    {
        return QString("");
    }
}
