/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef NOTIFICATIONCTRL_H
#define NOTIFICATIONCTRL_H

#include <QWidget>
#include <GenericCtrl.h>
#include "ui_NotificationCtrl.h"
class NotificationPanel;
class NotificationPopulator
{
public:
    virtual void GetData(int iYear, int iMonth, QList<QDate>& lstDates, QList<QString>& lstNotifications) = 0;
    virtual void OnDateSelected(QDate& oDate) = 0;
};

class NotificationCtrl : public GenericCtrl, public Ui::NotificationCtrl
{
    Q_OBJECT

public:
    explicit NotificationCtrl(QWidget *parent = 0);
    ~NotificationCtrl();
    void OnCreateCtrl();
    virtual void Initialize(NotificationPopulator* pPopulator);
    void Populate();
    void RemoveCurrentPanels();
public slots:
    void OnPageChanged(int iYear, int iMonth);
    void OnDateSelected(QDate date);
private:
    NotificationPopulator* p_Populator;
    int i_CurrentYear;
    int i_CurrentMonth;
    QList<NotificationPanel*> lst_CurrentPanels;
    QSpacerItem* p_SpacerItem;
};

#endif // NOTIFICATIONCTRL_H
