/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef PREFERENEWND_H
#define PREFERENEWND_H

#include <QWidget>
#include <GenericWnd.h>
#include <ui_PrefereneWnd.h>
class PrefereneWnd : public GenericWnd,public Ui::PrefereneWnd
{
    Q_OBJECT

public:
    explicit PrefereneWnd(MainWnd *parent,QString sTitle );
    ~PrefereneWnd();

     void OnPreCreate(Application *pApplication)override;
     virtual void OnCreate();
protected:
    void SetPrefName(QWidget* pWidget, QString sPrefName);
    virtual void AddCustomTabs(){}
    void AddTab(BasePreferenceTab* pTab, QString sTitle);
public slots:
      virtual void OnSave();
      virtual void OnLoadStyle();
      virtual void OnClearData();
    void OnChangeHeader();
private:



};

#endif // PREFERENEWND_H
