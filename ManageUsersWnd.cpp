#include "ManageUsersWnd.h"
#include <globaldefs.h>
#include <User.h>
#include <QMessageBox>
#include <AddUserCtrl.h>
//*************************************************************************************
ManageUsersWnd::ManageUsersWnd(MainWnd *parent, QString sTitle) :
    GenericWnd(parent,sTitle)
{
    setupUi(this);
    p_QueryCtrl->SetTableName("Login");
    p_QueryCtrl->SetKeyName(FLD_USERNAME);
    connect(p_QueryCtrl,SIGNAL(NotifyEditRequest(QString)),this,SLOT(OnEditRequest(QString)));
    connect(p_QueryCtrl, SIGNAL(NotifyNewRequest()),this,SLOT(AddNewUser()));
    p_EditCtrl = NULL;
}
//*************************************************************************************
ManageUsersWnd::~ManageUsersWnd()
{

}

void ManageUsersWnd::OnChildRemoveRequest(GenericCtrl *pChild)
{
    if(pChild == p_EditCtrl)
    {
        p_EditCtrl->deleteLater();
        p_EditCtrl = NULL;
        p_Tab->setCurrentIndex(0);
    }
}
//*************************************************************************************
void ManageUsersWnd::OnEditRequest(QString sKeyValue)
{
   // GenericWnd* pWnd = p_Application->CreateWnd(WND_AD_USR);
    if(p_EditCtrl)
    {
        delete p_EditCtrl;
    }
    p_EditCtrl = new AddUserCtrl(this);
    OnNewChild(p_EditCtrl);

    std::shared_ptr<Entity> pEntity = Entity::FindInstance(sKeyValue,ENTITY_USER);
    std::shared_ptr<User> pUser = std::dynamic_pointer_cast<User>(pEntity);
    if(!pUser)
    {
        QMessageBox::warning(this, tr("Data Stale"), tr("Reload the table before the action."));
        delete p_EditCtrl;
        p_EditCtrl = nullptr;
        return;
    }
    p_EditCtrl->FillData(pUser);
    //AddUserWnd* pAddWnd = static_cast<AddUserWnd*>(pWnd);
    //pAddWnd->PopulateWithData(pUser);
    p_Tab->addTab(p_EditCtrl,tr("Edit Users"));
    p_Tab->setCurrentWidget(p_EditCtrl);
}

void ManageUsersWnd::AddNewUser()
{
    p_Tab->setCurrentIndex(1);
    //p_Application->CreateWnd(WND_AD_USR);
}



