BEGIN TRANSACTION;
CREATE TABLE "Role_Priviledge" (
	`rolename`	TEXT,
	`pricode`	INTEGER
);
INSERT INTO `Role_Priviledge` VALUES('Admin',1);
INSERT INTO `Role_Priviledge` VALUES('Admin',2);
CREATE TABLE `Role` (
	`rolename`	TEXT,
	`roledescription`	TEXT
);
INSERT INTO `Role` VALUES('General','General User');
INSERT INTO `Role` VALUES('Admin','Administrator user');
CREATE TABLE `Priviledge` (
	`pricode`	INTEGER,
	`name`	TEXT,
	`prevdescription`	TEXT
);
INSERT INTO `Priviledge` VALUES(2,'ManageUsers','Update Existing Users');
INSERT INTO `Priviledge` VALUES(1,'AddUsers','Add New Users');
CREATE TABLE "Login" (
	`username`	TEXT,
	`password`	TEXT,
	`name`	TEXT,
	`rolename`	TEXT,
	PRIMARY KEY(username)
);
INSERT INTO `Login` VALUES('Admin',12,'gayan','Admin');
INSERT INTO `Login` VALUES('kan',123,'Kanishka','General');
;
COMMIT;
