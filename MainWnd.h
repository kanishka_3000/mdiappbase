/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef MAINWND_H
#define MAINWND_H

#include <QtWidgets/QMainWindow>
#include <ui_MainWnd.h>
#include <QMdiSubWindow>
#include <AboutApp.h>
#define USERGUIDE_PATH "resources"
class GenericWnd;
class Application;
class MainWnd : public QMainWindow,public Ui::MDI_ApplicationClass
{
	Q_OBJECT

public:
	MainWnd(QWidget *parent = 0);
	~MainWnd();

    virtual void Initilize(Application* pApplication);
friend GenericWnd;

public slots:
    //virtual void OnMainToolBarClicked(QAction* pAction);
    virtual void OnMenuChangePassword();
    virtual void OnMenuAboutBox();
    virtual void OnMenuPreferenceWnd();
    virtual void OnMenuAddUser();
    virtual void OnMenuManageUsers();
    virtual void OnCascadeWindows();
    virtual void OnMenuCloseAllWindows();
    virtual void OnUserGuide();
protected:
	QMdiSubWindow* _AddSubWindow(GenericWnd* pWnd);
    void _RemoveSubWindow(GenericWnd *pWnd);
    void closeEvent(QCloseEvent* event);
    Application* p_Application;
     AboutApp o_About;
};

#endif // MAINWND_H
