/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef QUERYDATASOURCE_H
#define QUERYDATASOURCE_H
#include <feature/CSVExporter.h>
#include <QSqlTableModel>
#include <QSqlRecord>
class QueryDataItem: public feature::CSVExporter::CSVDataRow
{
public:

    QueryDataItem(QSqlRecord pRecord);
    // CSVDataRow interface
public:
    virtual QString GetData(int iColumn);
private:
    QSqlRecord p_Record;
    QStringList o_List;
};

class QueryDataSource: public feature::CSVExporter::CSVDataSource
{
public:
    QueryDataSource(QSqlQueryModel* pModel);

    // CSVDataSource interface
public:
    virtual QStringList GetHeader() const;
    virtual int GetItemCount() const;
    virtual std::shared_ptr<feature::CSVExporter::CSVDataRow> GetDataRow(int iRow) const;
private:
    QSqlQueryModel* p_Model;
};

#endif // QUERYDATASOURCE_H
