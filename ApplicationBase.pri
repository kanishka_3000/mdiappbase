#-------------------------------------------------
#
# Project created by QtCreator 2015-12-28T15:53:39
#
#-------------------------------------------------

QT       += core gui
QT += sql xml
QT += script
QT += printsupport
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ApplicationBase
TEMPLATE = app
INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/entity
INCLUDEPATH += $$PWD/graphs
INCLUDEPATH += $$PWD/core
INCLUDEPATH += $$PWD/feature
##comment bellow lines upto <<<<< to disable reporting
INCLUDEPATH += ../QtRptProject/QtRPT/
INCLUDEPATH += ../QtRptProject/CommonFiles/
INCLUDEPATH += ../QWT/src
#QtRpt libraries should point to debug or release depending on the build
LIBS += -L../QtRptProject/bin/debug/lib -lQtRPT
LIBS += -L../QWT/lib -lqwt

#<<<<<
QMAKE_CXXFLAGS += -ggdb

SOURCES += \
    $$PWD/core/GenericWnd.cpp \
    $$PWD/MainWnd.cpp \
    $$PWD/MyTestWindow.cpp \
    $$PWD/core/Application.cpp \
    $$PWD/LoginWnd.cpp \
    $$PWD/core/Connection.cpp \
    $$PWD/Logger.cpp \
    $$PWD/Login.cpp \
    $$PWD/User.cpp \
    $$PWD/ChangePaswdWnd.cpp \
    $$PWD/BasePreferenceTab.cpp \
    $$PWD/PrefereneWnd.cpp \
    $$PWD/Util.cpp \
    $$PWD/core/GenericCtrl.cpp \
    $$PWD/AddUserCtrl.cpp \
    $$PWD/QueryCtrl.cpp \
    $$PWD/ManageUsersWnd.cpp \
    $$PWD/TableConfigHandler.cpp \
    $$PWD/Role.cpp \
    $$PWD/Priviledge.cpp \
    $$PWD/SystemPreferenceTab.cpp \
    $$PWD/ObjectFactory.cpp \
    $$PWD/core/ActionStore.cpp \
    $$PWD/QuickLaunch.cpp \
    $$PWD/GeneralPreferenceTab.cpp \
    $$PWD/GenericTableModel.cpp \
    $$PWD/EntityTableDataHandler.cpp \
    $$PWD/EntityManipulaterCtrl.cpp \
    $$PWD/core/GenericFilterCtrl.cpp \
    $$PWD/EnumHandler.cpp \
    $$PWD/EntityGroupTableWnd.cpp \
    $$PWD/EntityGroupTableCtrl.cpp \
    $$PWD/SpinBoxDeligate.cpp \
    $$PWD/TableStore.cpp \
    $$PWD/NotificationCtrl.cpp \
    $$PWD/NotificationWnd.cpp \
    $$PWD/NotificationPanel.cpp \
    $$PWD/FieldCompleter.cpp \
    $$PWD/LabelAnimator.cpp \
    $$PWD/reporting/ReportManager.cpp \
    $$PWD/EntitiyReporableModel.cpp \
    $$PWD/DataSetupManager.cpp \
    $$PWD/AboutApp.cpp \
    $$PWD/entity/Entity.cpp \
    $$PWD/entity/Entitydef.cpp \
    $$PWD/entity/EntityFactory.cpp \
    $$PWD/graphs/BarGraphCtrl.cpp \
    $$PWD/entity/EntityDataSummerizer.cpp \
    $$PWD/feature/DirectoryManager.cpp \
    $$PWD/feature/StringDataProcessor.cpp \
    $$PWD/feature/EntityStringListSourceAdapter.cpp \
    $$PWD/feature/CSVExporter.cpp \
    $$PWD/QueryDataSource.cpp

HEADERS  += \
    $$PWD/core/GenericWnd.h \
    $$PWD/MainWnd.h \
    $$PWD/MyTestWindow.h \
    $$PWD/core/Application.h \
    $$PWD/LoginWnd.h \
    $$PWD/core/Connection.h \
    $$PWD/Logger.h \
    $$PWD/Login.h \
    $$PWD/globaldefs.h \
    $$PWD/User.h \
    $$PWD/ChangePaswdWnd.h \
    $$PWD/BasePreferenceTab.h \
    $$PWD/PrefereneWnd.h \
    $$PWD/Util.h \
    $$PWD/core/GenericCtrl.h \
    $$PWD/AddUserCtrl.h \
    $$PWD/QueryCtrl.h \
    $$PWD/ManageUsersWnd.h \
    $$PWD/TableConfigHandler.h \
    $$PWD/Role.h \
    $$PWD/Priviledge.h \
    $$PWD/SystemPreferenceTab.h \
    $$PWD/ObjectFactory.h \
    $$PWD/core/ActionStore.h \
    $$PWD/QuickLaunch.h \
    $$PWD/GeneralPreferenceTab.h \
    $$PWD/GenericTableModel.h \
    $$PWD/EntityTableDataHandler.h \
    $$PWD/EntityManipulaterCtrl.h \
    $$PWD/core/GenericFilterCtrl.h \
    $$PWD/EnumHandler.h \
    $$PWD/EntityGroupTableWnd.h \
    $$PWD/EntityGroupTableCtrl.h \
    $$PWD/DBFields.h \
    $$PWD/SpinBoxDeligate.h \
    $$PWD/TableStore.h \
    $$PWD/NotificationCtrl.h \
    $$PWD/NotificationWnd.h \
    $$PWD/NotificationPanel.h \
    $$PWD/FieldCompleter.h \
    $$PWD/LabelAnimator.h \
    $$PWD/reporting/ReportManager.h \
    $$PWD/reporting/ReportableModel.h \
    $$PWD/EntitiyReporableModel.h \
    $$PWD/DataSetupManager.h \
    $$PWD/AboutApp.h \
    $$PWD/entity/Entity.h \
    $$PWD/entity/Entitydef.h \
    $$PWD/entity/EntityFactory.h \
    $$PWD/graphs/BarGraph.h \
    $$PWD/graphs/BarGraphCtrl.h \
    $$PWD/entity/EntityDataSummerizer.h \
    $$PWD/entity/DataCollector.h \
    $$PWD/feature/DirectoryManager.h \
    $$PWD/feature/StringDataProcessor.h \
    $$PWD/feature/EntityStringListSourceAdapter.h \
    $$PWD/feature/CSVExporter.h \
    $$PWD/QueryDataSource.h
FORMS    += \
    $$PWD/MainWnd.ui \
    $$PWD/MyTestWindow.ui \
    $$PWD/LoginWnd.ui \
    $$PWD/ChangePaswdWnd.ui \
    $$PWD/PrefereneWnd.ui \
    $$PWD/AddUserCtrl.ui \
    $$PWD/QueryCtrl.ui \
    $$PWD/ManageUsersWnd.ui \
    $$PWD/QuickLaunch.ui \
    $$PWD/EntityGroupTableWnd.ui \
    $$PWD/EntityGroupTableCtrl.ui \
    $$PWD/NotificationCtrl.ui \
    $$PWD/NotificationPanel.ui \
    $$PWD/AboutApp.ui \
    $$PWD/graphs/BarGraphCtrl.ui
RESOURCES += \
    $$PWD/mdi_application.qrc
