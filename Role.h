/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef ROLE_H
#define ROLE_H

#include <entity/Entity.h>
#include <Priviledge.h>
class Role:public Entity
{
public:
    Role();


    virtual void ResolveReferences();
    QString GetRoleName();
    bool HasPriviledge(int iPreviCode);
protected:
    DEC_ETY_INITILIZER

    QMap<int, std::shared_ptr<Priviledge>> map_Priviledges;
};

#endif // ROLE_H
