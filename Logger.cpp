#include "Logger.h"
#include <QDebug>
Logger::Logger(QString *sLogLocation)
{
    o_CurrentDate = QDateTime::currentDateTime();
    s_Location = *sLogLocation;
    QString sLogName = o_CurrentDate.toString("yyyyMMddHHmmss");
    sLogName+="_ApplicationLog.log";
    (*sLogLocation)+="/";
    (*sLogLocation)+=sLogName;
    p_File = new QFile(*sLogLocation);
    b_LogFileOpened = false;

}

bool Logger::OpenLogFile()
{
    b_LogFileOpened = p_File->open(QIODevice::ReadWrite);

    p_TextStream = new QTextStream(p_File);
    return b_LogFileOpened;
}

void Logger::WriteLog(QString sLogString)
{
    QDateTime oDate = QDateTime::currentDateTime();
    QString sLogName = oDate.toString("yyyyMMddHHmmss - ");
    *p_TextStream << sLogName << sLogString << "\n";
    p_TextStream->flush();
}

void Logger::WriteLog(const char *fmt,...)
{
    va_list args;
    QString sLog;
    va_start(args, fmt);
    sLog = sLog.vsprintf(fmt,args);
    va_end(args);
    WriteLog(sLog);
}

int Logger::ClearLogs(int iPastDays)
{
    if(!b_LogFileOpened)
        return 0;

    QDateTime oLastDateTime = o_CurrentDate.addDays(iPastDays * (-1));
    QDir oDir(s_Location,"*.log");
    QFileInfoList oAllFiles = oDir.entryInfoList();
    WriteLog("Deleting Log files before %s",qPrintable(oLastDateTime.toString("yyyyMMdd")));
    WriteLog("No of total logs %d",oAllFiles.size());
    int iDeleteLogCount = 0;
    foreach(QFileInfo oInfo, oAllFiles)
    {
        QDateTime oCreatedDate = oInfo.created();
         QString sPath = oInfo.filePath();
        if(oCreatedDate < oLastDateTime)
        {

           WriteLog("Deleting log file %s - created on %s", qPrintable(sPath), qPrintable(oCreatedDate.toString("yyyyMMdd")));
           oDir.remove(sPath);
           iDeleteLogCount++;
        }
        else
        {
            WriteLog("Ignoring log file %s - created on %s", qPrintable(sPath), qPrintable(oCreatedDate.toString("yyyyMMdd")));
        }
    }
    return iDeleteLogCount;
}

