#include "QuickLaunch.h"
#include "ui_QuickLaunch.h"
#include <QPushButton>
#include <Util.h>
#include <QSize>
#include <QToolButton>
QuickLaunch::QuickLaunch(MainWnd *parent, QString sTitle) :
    GenericWnd(parent,sTitle)
{
    setupUi(this);
}

QuickLaunch::~QuickLaunch()
{

}

void QuickLaunch::OnCreate()
{

    QMap<QString,QAction*> mapActions = p_Application->GetActionStore()->GetAllActions();
    QStringList sSelctedList = Util::GetSettingsList(p_Application->GetConfig(),LIST_SELECTED_MENU);

    foreach(QString sLaucnKey, sSelctedList)
    {
        QAction* pAction = mapActions.value(sLaucnKey);
        if(!pAction)
            continue;
        QToolButton* pButtn = new QToolButton(this);
        pButtn->setMinimumSize(50,50);
        pButtn->setMaximumSize(50,50);
        pButtn->setIconSize(QSize(30,30));
        pButtn->setIcon(pAction->icon());

        pButtn->setText(sLaucnKey);
        connect(pButtn,SIGNAL(clicked(bool)), pAction,SLOT(trigger()));
        p_MainLayout->addWidget(pButtn,Qt::AlignCenter);
    }

     QSpacerItem *verticalSpacer=  new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
     p_MainLayout->addSpacerItem(verticalSpacer);
}
