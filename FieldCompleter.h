/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef FIELDCOMPLETER_H
#define FIELDCOMPLETER_H

#include <QObject>
#include <QLineEdit>
#include <Application.h>
class FieldCompleter : public QObject
{
    Q_OBJECT
public:
    explicit FieldCompleter(Application *parent, QString sTableName);
    void SetSource(QLineEdit* pSource, QString sFieldName);
    void SetDestination(QLineEdit* pDestination, QString sFieldName);
signals:

public slots:
    void OnSelected(QModelIndex oIndex);
private:
    QString s_TableName;
    TableStore* p_TableStore;
    QSqlTableModel* p_Table;
    int i_DesIndex;
    QLineEdit* p_Destination;
    QCompleter* p_Completer ;
};

#endif // FIELDCOMPLETER_H
