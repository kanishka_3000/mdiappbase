/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef OBJECTFACTORY_H
#define OBJECTFACTORY_H

class MainWnd;
class EntityFactory;
class DataSetupManager;
class ObjectFactory
{
public:
    ObjectFactory();
    virtual MainWnd* CreateMainWnd();
    virtual EntityFactory* CreateEntityFactory();
    virtual DataSetupManager* GetSetupManager();
};

#endif // OBJECTFACTORY_H
