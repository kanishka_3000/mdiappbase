#include "EntityManipulaterCtrl.h"
#include <QMessageBox>
#include <GenericWnd.h>
#include <QDateEdit>
#include <QTextEdit>
#include <Util.h>
EntityManipulaterCtrl::EntityManipulaterCtrl(QWidget* pParent):
    GenericCtrl(pParent),p_Entity(NULL),b_EditMode(false),b_DeleteWhenDone(true),b_ClearFieldsWhenSave(true),p_DefaultDisplayer(nullptr)
{

}

EntityManipulaterCtrl::~EntityManipulaterCtrl()
{
    for(auto itr = map_FeedbackSubscribers.begin(); itr != map_FeedbackSubscribers.end(); itr++)
    {
        QSet<PopulationCallback*>& setData = *itr;
        foreach(PopulationCallback* pCallback, setData)
        {
            pCallback->RemovePublisher();
        }
    }
}

void EntityManipulaterCtrl::OnCreateCtrl()
{


}

void EntityManipulaterCtrl::OnPostCreateCtrl()
{
    StoreDefaultData();
}

bool EntityManipulaterCtrl::OnSave(EntityManipulaterSaveDislpayer *pSaveDisplayer)
{
    QString sError;
    if(!BasicEntryValidation(sError))
    {
        QMessageBox::warning(this,tr("Incomplete"),tr("Some Fields are not complete") + sError);
        return false;
    }

    if(!p_Entity)
    {
        p_Entity = p_Parent->GetApplication()->GetEntityFactory()->CreateEntity(s_EntityType);
    }
    PopulateEntityWithData(p_Entity, b_EditMode);

    if(!CustomValidations(sError))
    {
        QMessageBox::warning(this, tr("Incomplete"),sError);
        return false;
    }
    QString sUpdate = "Data Save Complete";
    OnPreSave();
    if(!b_EditMode)
    {
        int iReturn = p_Entity->Save();

        if(iReturn == ERR_EXISTS || iReturn == -1)
        {
            QString sReturn = "";
            switch(iReturn)
            {
            case -1:
            {
                sReturn = "Save failed, Unknown issue!!";
                break;
            }
            case ERR_EXISTS:
            {
                sReturn = "Save failed, Data exists";
                break;
            }

            }
            if(pSaveDisplayer)
            {
                pSaveDisplayer->CustomizeExistMsg(sReturn);
            }
            QMessageBox::warning(this,tr("Fail"), sReturn);
            return false;
        }
    }
    else
    {
        int iReturn = p_Entity->UpdateData();
        switch(iReturn)
        {
        case -1:
        {
        sUpdate = QString("Update Unsuccessful!");
            break;
        }
        case INVALID_UPDATE:
        {
            sUpdate = "Nothing to update";
            break;
        }
        }
    }

    if(b_ClearFieldsWhenSave)
    {
        ClearFields();
        RestoreDefaultData();
    }
    OnPostSave();
    if(pSaveDisplayer)
    {
        pSaveDisplayer->EntitySaved(b_EditMode);
    }
    else if(p_DefaultDisplayer)
    {
        p_DefaultDisplayer->EntitySaved(b_EditMode);
    }
    else{
        QMessageBox::information(this, tr("Complete"), sUpdate);
    }

    //delete p_User;//shared pointers will handle deletes
    //p_Entity = NULL;
    emit NotifyItemSaved(b_EditMode);
    if(b_EditMode)
    {
        p_Parent->OnChildRemoveRequest(this);
        if(b_DeleteWhenDone)
        {
            p_Parent->RemoveWindow();
        }

    }

    return true;
}

void EntityManipulaterCtrl::StoreDefaultData()
{
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;
        map_DefaultData.insert(pWidget, GetWidgetText(pWidget));

    }
}

void EntityManipulaterCtrl::RestoreDefaultData()
{
    for(auto itr = map_DefaultData.begin(); itr != map_DefaultData.end(); itr++)
    {
        QWidget* pWidget = itr.key();
        QString sValue = itr.value();
        SetWidgetText(pWidget,sValue);
    }
}

void EntityManipulaterCtrl::SetClearFieldsWhenSave(bool value)
{
    b_ClearFieldsWhenSave = value;
}


void EntityManipulaterCtrl::FillEntityWithData(QVariant oData, std::shared_ptr<Entity> pEntity, bool bUpdate, QString sField)
{
    if(!bUpdate)
    {
        pEntity->AddData(sField,oData);
    }
    else
    {
        pEntity->SetUpdateData(sField, oData);
    }
}

void EntityManipulaterCtrl::PopulateWithEntity(std::shared_ptr<Entity> pEntity, bool bEditMode)
{
    b_EditMode = bEditMode;
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

         QVariant oData = pEntity->GetData(sField);
         QString sData = oData.toString();
         if(sData.isEmpty())
         {
             QSet<PopulationCallback*>& setEmpty =
                     map_FeedbackSubscribers[PopulationFeedback::EMPTY];
             foreach(PopulationCallback* pEmptyCallback, setEmpty)
             {
                 pEmptyCallback->OnPopulation(PopulationFeedback::EMPTY, sField, pWidget);
             }
         }
         SetWidgetText(pWidget, sData);
    }
    p_Entity = pEntity;
}

void EntityManipulaterCtrl::PopulateEntityWithData(std::shared_ptr<Entity> pEntity, bool bUpdate)
{
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

        QVariant oData;
        if(pWidget->isEnabled())
        {
            QString sData = GetWidgetText(pWidget);
            sData.replace("'", "''");
            oData = sData;
        }
         else
        {
            oData = QVariant();
        }

        FillEntityWithData(oData, pEntity, bUpdate, sField);

    }
}


void EntityManipulaterCtrl::AdjustFieldsAsCopy()
{
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        if(!IsTextWidget(pWidget))
            continue;
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

        QString sText = GetWidgetText(pWidget);
        sText = tr("Copy of ") + sText;
        SetWidgetText(pWidget,sText);

    }
}

void EntityManipulaterCtrl::SubscribeForFeedback(EntityManipulaterCtrl::PopulationFeedback eType, EntityManipulaterCtrl::PopulationCallback *pSubscriber)
{
    QSet<PopulationCallback*>& setCallbacks = map_FeedbackSubscribers[eType];
    setCallbacks.insert(pSubscriber);
    pSubscriber->SetPublisher(this, eType);
}

void EntityManipulaterCtrl::UnsubscribeForFeedback(EntityManipulaterCtrl::PopulationFeedback eType, EntityManipulaterCtrl::PopulationCallback *pSubscriber)
{
    QSet<PopulationCallback*>& setCallbacks = map_FeedbackSubscribers[eType];
    setCallbacks.remove(pSubscriber);
    pSubscriber->RemovePublisher();
}


