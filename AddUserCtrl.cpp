#include "AddUserCtrl.h"
#include "ui_AddUserCtrl.h"
#include <Util.h>
#include <User.h>
#include <QMessageBox>
#include <GenericWnd.h>

AddUserCtrl::AddUserCtrl(QWidget *parent) :
    EntityManipulaterCtrl(parent)
{
    setupUi(this);
    b_Edit = false;
    p_User = NULL;
    p_Roles = NULL;
    b_DeleteWhenDone = false;
    s_EntityType = ENTITY_USER;
}

AddUserCtrl::~AddUserCtrl()
{
    delete p_Roles;
}

void AddUserCtrl::FillData(std::shared_ptr<User> pUser)
{
    b_Edit = true;
    p_User = pUser;
    txt_Password1->setEnabled(false);
    txt_Password2->setEnabled(false);
    btn_Clear->setEnabled(false);
    PopulateWithEntity(pUser);

}

bool AddUserCtrl::CustomValidations(QString &sError)
{
    if((!b_Edit) && txt_Password1->text() != txt_Password2->text())
    {

        sError = tr("Passwords do not match");
            return false;
    }


    return true;

}

void AddUserCtrl::OnCreateCtrl()
{
    p_Roles = new QSqlTableModel(this,*Connection::GetInstance()->GetPrimaryDb());
    p_Roles->setTable(ENTITY_ROLE);

    cmb_Role->setModel(p_Roles);
    p_Roles->select();

}

//void AddUserCtrl::OnSave()
//{



//    if(!BasicEntryValidation())
//    {
//         QMessageBox::warning(this,"Entry incomplete","Some fields are empty");
//        return ;
//    }


//     if(!p_User)
//     {
//         p_User = std::dynamic_pointer_cast<User> (p_Parent->GetApplication()->GetEntityFactory()->CreateEntity(ENTITY_USER));
//     }

//     PopulateEntityWithData(p_User,b_Edit);

//    if(!b_Edit)
//    {
//        if(!p_User->Save())
//        {
//            QMessageBox::warning(this,"Fail","User save failed, user exists.");
//            return;
//        }
//    }
//    else
//    {
//        p_User->UpdateData();
//    }

//    QMessageBox::information(this,"Added","User details saved");

//    ClearFields();
//    //delete p_User;//shared pointers will handle deletes
//    p_User = NULL;

//    if(b_Edit)
//    {
//        p_Parent->OnChildRemoveRequest(this);
//        //p_Parent->RemoveWindow();
//    }
//}

void AddUserCtrl::OnClear()
{
    ClearFields();
}
