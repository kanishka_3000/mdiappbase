#include "EnumHandler.h"

EnumHandler::~EnumHandler()
{
	for(auto itr = map_Enamurations.begin(); itr != map_Enamurations.end(); itr++)
	{
		delete itr.value();
	}
}

void EnumHandler::AddEnumData(QString sEnum, QString sValue, int iValue)
{
    Enamuration* pEnum = NULL;
    if(map_Enamurations.contains(sEnum))
    {
        pEnum = map_Enamurations.value(sEnum);
    }
    else
    {
        pEnum = new Enamuration;//Systemwide no need to delete
        map_Enamurations.insert(sEnum,pEnum);
    }
    pEnum->AddEnumData(sValue,iValue);
}

Enamuration *EnumHandler::GetEnum(QString sEnum)
{
    return map_Enamurations.value(sEnum);
}

void EnumHandler::FillInitialData()
{
    //Custom Enum State
    AddEnumData(E_STATE,"Active",0);
    AddEnumData(E_STATE,"Inactive",1);

    AddEnumData(E_ORDER_STATE, "Open",0);
    AddEnumData(E_ORDER_STATE, "Delivered",1);
    AddEnumData(E_ORDER_STATE, "Cancelled",2);
}

EnumHandler::EnumHandler()
{

}



void Enamuration::AddEnumData(QString sValue, int iValue)
{
    map_intToSt.insert(iValue,sValue);
    map_StToint.insert(sValue,iValue);
}

int Enamuration::GetValue(QString sValue)
{
    return map_StToint.value(sValue);
}

QString Enamuration::GetValue(int iValue)
{
    return map_intToSt.value(iValue);
}
