/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef CONNECTION_H
#define CONNECTION_H
#include <QSqlDatabase>
#include <QList>
#include <QStringList>
#include <memory>
#include <QVariant>
#include <QMap>
//#define _LOG_SQL_ ""

#define INVALID_UPDATE  -2

class Connection
{
public:
    //Logger class for the connection
    class ConnectionLogger
    {
    public :
        virtual void PrintOutput(QString sData) = 0;
    };

    class ConnectionDataHander
    {
    public:
        virtual void CreateNewItem() = 0;
        virtual QList<QString>  GetFieldList() = 0;
        virtual void AddDataColumn(QString sColumn, QVariant vData) = 0;
    };
    //Database connection information for independant database managemnt system
    class ConnectionInfo
    {
    public:
        QString s_UserName;
        QString s_Password;
        QString s_Server;
        QString s_DBName;
    };

    static void CreateInstance();
    bool Initialize(QString sLocation, QString sSecondaryLocation, bool bReplicate, ConnectionLogger *pLogger = nullptr);
    bool Initialize(ConnectionInfo oConnectionInfo, ConnectionLogger* pLogger);
    static void CloseDB();
    static Connection* GetInstance(){return p_Connection;}
    virtual ~Connection();
    //Sql query that will not return a data set: ex: insert query
    int RunQuery(QString sQuery);
    //SQL Query that will return data: ex: select query
    //function is bound with the entity object for simplicity
    //Idial implementation would be to use an intermediate object to translate between Entity and the database structures
    void GerRecords(QString sQuery, ConnectionDataHander& rDataHander);

    void GetSummery(QString sQuery, QVariant& oData);
    int UpdateQuery(QString sTableName, QMap<QString, QVariant> mapData, QString sKeyField = "", QString sValue = "");

    int InsertQuery(QString sTableName, QMap<QString, QVariant> mapData);

    bool DoesDBFieldExists(QString sTableName, QString sFieldName, QVariant oValue);
    //Return column names of a table
    QStringList GetTableColumns(QString sTableName);

    QStringList GetTables();

    void EmptyTable(QString sTableName);
    //Delete
    void DeleteRow(QString sTable, QString sKey, QString sValue);
    //Verify that both databases contain all tables and that row counts are similar//
    //Verification does not check each and every data item.
    bool VerifyDBs();
    //*** Not recomanded to use for writing since this will not write data to both databases.
    QSqlDatabase* GetPrimaryDb(){return &db_Primary;}
    QString AppendDataToQuery(QString sFieldName, QVariant oValue, QString sTableName);
    static int i_LastID;
private:
    Connection();
    static Connection* p_Connection;
    ConnectionLogger* p_ConnectionLogger;
     QSqlDatabase db_Primary;
     QSqlDatabase db_Replica;
     bool b_ReplicateDB;


};

#endif // CONNECTION_H
