/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef GENERICFILTERCTRL_H
#define GENERICFILTERCTRL_H

#include <QObject>
#include <GenericCtrl.h>
#include <QList>
#include <QCheckBox>
#include <globaldefs.h>
#include <tuple>
#include <QPushButton>
typedef std::tuple<QString,QString,int> FilterCrieteria;

class GenericFilterCtrl : public GenericCtrl
{
    Q_OBJECT
public:
    explicit GenericFilterCtrl(QWidget *parent=0);
    void OnPreCreateWnd(GenericWnd *pParent);
    void SetApplyButton(QPushButton* pButton);
    void SetClearButton(QPushButton* pButton);
signals:
    //Connect this signal to get the filter pack when filter is invoked
    void NotifyFilter(QList<FilterCrieteria> lstFilter);
public slots:
    virtual void OnFilter();
    void SetFilter(QMap<QString,QString> mapData);
    void OnClear();
protected:
    void AddCheckBuddies(QWidget* pDesination, QCheckBox* pBuddy);
    virtual void CollectFields(QList<QWidget*>& lstWidgets,  QList<FilterCrieteria>& lstFieldData);
    void ManageBuddyText(QList<QWidget *> &lstWidgets, QString& sBuddyText, QString sBuddyWidget, bool bSet = false);
    void SetAsFilterBuddy(QWidget* pOwner, QWidget *pBuddy);

    QList<FilterCrieteria> lt_FieldData;
    QMap<QWidget*,QCheckBox*> map_Buddies;
};

#endif // GENERICFILTERCTRL_H
