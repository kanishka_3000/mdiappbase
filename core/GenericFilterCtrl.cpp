#include "GenericFilterCtrl.h"
#include <Util.h>
GenericFilterCtrl::GenericFilterCtrl(QWidget *parent) :
    GenericCtrl(parent)
{

}

void GenericFilterCtrl::OnPreCreateWnd(GenericWnd *pParent)
{
    GenericCtrl::OnPreCreateWnd(pParent);
    SetCurrentDateToAllFields();
}

void GenericFilterCtrl::SetApplyButton(QPushButton *pButton)
{
    connect(pButton, SIGNAL(clicked(bool)),this, SLOT(OnFilter()));
}

void GenericFilterCtrl::SetClearButton(QPushButton *pButton)
{
    connect(pButton, SIGNAL(clicked(bool)), this, SLOT(OnClear()));
}

void GenericFilterCtrl::OnFilter()
{
    lt_FieldData.clear();
    QList<QWidget*> lstWidgets = findChildren<QWidget*>();

    CollectFields(lstWidgets, lt_FieldData);
    NotifyFilter(lt_FieldData);


}

void GenericFilterCtrl::SetFilter(QMap<QString, QString> mapData)
{
    QList<QWidget*> lstWidgets = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstWidgets)
    {

        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;
        QString sData = mapData.value(sField);

        if(sData.isEmpty())
            continue;

        QString sBuddyWidget = pWidget->property(BUDDY).toString();
        if(!sBuddyWidget.isEmpty())
        {
            ManageBuddyText(lstWidgets, sData, sBuddyWidget, true);
        }

        pWidget->setEnabled(true);
        if(map_Buddies.contains(pWidget))
        {
            QCheckBox* pBuddy = map_Buddies.value(pWidget);
            pBuddy->setChecked(true);
        }


        SetWidgetText(pWidget,sData);
    }
}

void GenericFilterCtrl::AddCheckBuddies(QWidget *pDesination, QCheckBox *pBuddy)
{
    map_Buddies.insert(pDesination, pBuddy);
}

void GenericFilterCtrl::ManageBuddyText(QList<QWidget *> &lstWidgets, QString& sBuddyText, QString sBuddyWidget, bool bSet)
{
    foreach(QWidget* pWidgetIn, lstWidgets)
    {
        QString sObectName = pWidgetIn->objectName();
        if(sObectName == sBuddyWidget)
        {
            if(!bSet)
            {
                sBuddyText = GetWidgetText(pWidgetIn);
            }
            else
            {
                SetWidgetText(pWidgetIn, sBuddyText);
            }
            break;
        }
    }
}

void GenericFilterCtrl::SetAsFilterBuddy(QWidget *pOwner, QWidget* pBuddy)
{
    pOwner->setProperty(BUDDY, pBuddy->objectName());
}

void GenericFilterCtrl::OnClear()
{
    ClearFields();
}

void GenericFilterCtrl::CollectFields(QList<QWidget *> &lstWidgets, QList<FilterCrieteria> &lstFieldData)
{
    foreach(QWidget* pWidget, lstWidgets)
    {
        if(!pWidget->isEnabled())
            continue;

        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

        int iFilterType = pWidget->property(FILTER_TYPE).toInt();
        QString sData = GetWidgetText(pWidget);
        if(sData.isEmpty())
            continue;

        QString sBuddyWidget = pWidget->property(BUDDY).toString();
        if(!sBuddyWidget.isEmpty())
        {
            QString sBuddyText;
            ManageBuddyText(lstWidgets, sBuddyText, sBuddyWidget);
            sData = QString("%1|%2").arg(sData).arg(sBuddyText);
            iFilterType = FILTER_TYPE_BETWEEN;
        }
        FilterCrieteria oCri(sField,sData,iFilterType);

        lstFieldData.push_back(oCri);
    }
}

