#include "ActionStore.h"

ActionStore::ActionStore()
{

}

void ActionStore::AddAction(QString sName, QAction *pAct)
{
    if(sName.isEmpty())
        return;

    map_Actions.insert(sName, pAct);
}

QAction *ActionStore::GetAction(QString sName)
{
    return map_Actions.value(sName);
}

QMap<QString,QAction *> ActionStore::GetAllActions()
{
    return map_Actions;
}

QIcon ActionStore::GetIcon(QString sName)
{
    auto itr = map_Actions.find(sName);
    if(itr == map_Actions.end())
        return QIcon();

    QAction* pAction = itr.value();

    return pAction->icon();

}

