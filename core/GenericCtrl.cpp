#include "GenericCtrl.h"
#include <GenericWnd.h>
#include <globaldefs.h>
#include <QLineEdit>
#include <QComboBox>
#include <QDateEdit>
#include <QDoubleSpinBox>
#include <QCompleter>
#include <QPlainTextEdit>
GenericCtrl::GenericCtrl(QWidget *parent) :
    QWidget(parent)
{
    p_Parent = NULL;
    p_Logger = nullptr;
}

void GenericCtrl::OnPreCreateWnd(GenericWnd *pParent)
{
    p_Parent = pParent;
    p_Logger = p_Parent->GetApplication()->GetLogger();
}

void GenericCtrl::WriteLog(const char *zfmt,...)
{
    va_list args;
    QString sLog;
    va_start(args, zfmt);
    p_Parent->GetApplication()->GetLogger()->WriteLog(zfmt, args);
    va_end(args);

}

QMap<QString, QVariant> GenericCtrl::CollectFields()
{
    QMap<QString,QVariant> mapValues;
    QList<QWidget*> lstChildren = findChildren<QWidget*>();
    foreach(QWidget* pWidget, lstChildren)
    {
        QString sField = pWidget->property(FIELD).toString();
        if(sField == "")
            continue;

        if(pWidget->metaObject()->className() == QString("QLineEdit"))
        {
            QLineEdit* pLIneEdit = static_cast<QLineEdit*>(pWidget);
            mapValues.insert(sField, pLIneEdit->text());
        }

    }

    return mapValues;
}

void GenericCtrl::ClearFields()
{
    QList<QLineEdit*> lstLineEdits = findChildren<QLineEdit*>();
    foreach(QLineEdit* pLineEdits , lstLineEdits)
    {
        if(!pLineEdits->isEnabled())
            continue;

        if(pLineEdits->isReadOnly())
            continue;

        pLineEdits->clear();
    }
    QList<QComboBox*> lstComboBox = findChildren<QComboBox*>();
    foreach(QComboBox* pCombo, lstComboBox)
    {
        pCombo->setCurrentIndex(-1);
    }

    //Add anyother fields with development
}

void GenericCtrl::SetCurrentDateToAllFields()
{
    QDate oData = QDate::currentDate();
    QList<QDateEdit*> lstDateEdits = findChildren<QDateEdit*>();
    foreach(QDateEdit* pEdit, lstDateEdits)
    {
        pEdit->setDisplayFormat(DATE_FORMAT);
        pEdit->setDate(oData);
    }
}

bool GenericCtrl::BasicEntryValidation(QString& sError)
{
    QList<QLineEdit*> lstLineEdits = findChildren<QLineEdit*>();
    foreach(QLineEdit* pLineEdits , lstLineEdits)
    {
        if(!pLineEdits->isEnabled())
            continue;

        if(pLineEdits->isReadOnly())
            continue;
        QString sField = GetPropertyName(pLineEdits);

        if(sField.isEmpty())
            continue;

       QString sText = pLineEdits->text();
       if(sText == "")
       {
           sError = sField;
           return false;
       }
    }
    QList<QPlainTextEdit*> lstPlaintext = findChildren<QPlainTextEdit*>();
    foreach(QPlainTextEdit* pTextEdit, lstPlaintext)
    {
        if(!pTextEdit->isEnabled())
            continue;

        if(pTextEdit->isReadOnly())
            continue;
        QString sField = GetPropertyName(pTextEdit);

        if(sField.isEmpty())
            continue;

       QString sText = pTextEdit->toPlainText();
       if(sText == "")
       {
           sError = sField;
           return false;
       }
    }

    return true;
}

QString GenericCtrl::GetWidgetText(QWidget *pWidget)
{
    QString sReturnText("");
    if(pWidget->metaObject()->className() == QString("QLineEdit"))
    {
        QLineEdit* pLIneEdit = static_cast<QLineEdit*>(pWidget);

       sReturnText = pLIneEdit->text();

    }
    else if(pWidget->metaObject()->className() == QString("QComboBox"))
    {
        QComboBox* pQComboBox = static_cast<QComboBox*>(pWidget);
         QString sEnumName = pQComboBox->property(ENUM_NAME).toString();
        if(sEnumName.isEmpty())
        {
            sReturnText = pQComboBox->currentText();
        }
        else
        {
            QVariant oData = pQComboBox->itemData(pQComboBox->currentIndex());
            sReturnText = oData.toString();
        }

    }
    else if(pWidget->metaObject()->className() == QString("QDateEdit"))
    {
        QDateEdit* pDateEdit = static_cast<QDateEdit*>(pWidget);

        sReturnText = pDateEdit->text();

    }
    else if(pWidget->metaObject()->className() == QString("QTextEdit"))
    {
        QTextEdit* pTextEdit = static_cast<QTextEdit*>(pWidget);

       sReturnText = pTextEdit->toPlainText();

    }
    else if(pWidget->metaObject()->className() == QString("QDoubleSpinBox"))
    {
        QDoubleSpinBox* pDblSpinBox = static_cast<QDoubleSpinBox*>(pWidget);
        sReturnText = QVariant(pDblSpinBox->value()).toString();
    }
    else if(pWidget->metaObject()->className() == QString("QSpinBox"))
    {
        QSpinBox* pDblSpinBox = static_cast<QSpinBox*>(pWidget);
        sReturnText = QVariant(pDblSpinBox->value()).toString();
    }
    else if(pWidget->metaObject()->className() == QString("QPlainTextEdit"))
    {
        QPlainTextEdit* pPlanTextEdit = static_cast<QPlainTextEdit*>(pWidget);
        sReturnText = pPlanTextEdit->toPlainText();
    }
    else
    {
        Q_ASSERT_X(false, "GetWidgetText", "Unsupported widget");
    }
    return sReturnText;
}

void GenericCtrl::SetWidgetText(QWidget *pWidget, QString sText)
{
    if(pWidget->metaObject()->className() == QString("QLineEdit"))
    {
        QLineEdit* pLIneEdit = static_cast<QLineEdit*>(pWidget);
        pLIneEdit->setText(sText);

    }
    else if(pWidget->metaObject()->className() == QString("QComboBox"))
    {
        QComboBox* pQComboBox = static_cast<QComboBox*>(pWidget);
        QString sEnumName = pQComboBox->property(ENUM_NAME).toString();
        if(sEnumName.isEmpty())
        {
              pQComboBox->setCurrentText(sText);
        }
        else
        {
            Enamuration* pEnum = p_Parent->GetApplication()->GetEnumHandler()->GetEnum(sEnumName);
            QString sData = pEnum->GetValue(sText.toInt());
            pQComboBox->setCurrentText(sData);
        }


    }
    else if(pWidget->metaObject()->className() == QString("QDateEdit"))
    {
        if(sText.isEmpty())
            return;
        //No known way to handle this for the date edit
        QDateEdit* pDateEdit = static_cast<QDateEdit*>(pWidget);
        QDate oDate = QDate::fromString(sText,DATE_FORMAT);
        pDateEdit->setDate(oDate);
    }
    else if(pWidget->metaObject()->className() == QString("QTextEdit"))
    {
        QTextEdit* pTextEdit = static_cast<QTextEdit*>(pWidget);
        pTextEdit->setText(sText);
    }
    else if(pWidget->metaObject()->className() == QString("QDoubleSpinBox"))
    {
        QDoubleSpinBox* pDblSpinBox = static_cast<QDoubleSpinBox*>(pWidget);
        double dValue = sText.toDouble();
        pDblSpinBox->setValue(dValue);
    }
    else if(pWidget->metaObject()->className() == QString("QSpinBox"))
    {
        QSpinBox* pDblSpinBox = static_cast<QSpinBox*>(pWidget);
        int iValue = sText.toInt();
        pDblSpinBox->setValue(iValue);
    }
    else if(pWidget->metaObject()->className() == QString("QPlainTextEdit"))
    {
         QPlainTextEdit* pPlanTextEdit = static_cast<QPlainTextEdit*>(pWidget);
         pPlanTextEdit->setPlainText(sText);
    }
    else
    {
        Q_ASSERT_X(false, "SetWidgetText", "Unsupported widget");
    }
}

bool GenericCtrl::IsTextWidget(QWidget *pWidget)
{
    return pWidget->metaObject()->className() != QString("QDoubleSpinBox");
}

QVariant GenericCtrl::GetComboboxDataFromModel(QWidget *pWidget, int iIndex)
{
    if(!(pWidget->metaObject()->className() == QString("QComboBox")))
    {
        return QVariant();
    }
    QComboBox* pComboBox = qobject_cast<QComboBox*>(pWidget);
    int iRow = pComboBox->currentIndex();
    QModelIndex oMIndex = pComboBox->model()->index(iRow,iIndex);
    return pComboBox->model()->data(oMIndex);
}

void GenericCtrl::SetComboCompleter(QComboBox *pComboBox)
{
    QCompleter* pCompleter = pComboBox->completer();
    pCompleter->setMaxVisibleItems(10);
    pCompleter->setCompletionMode(QCompleter::PopupCompletion);
}

void GenericCtrl::SetLineCompleter(QLineEdit *pLineEdit, QString sTable, int iIndex)
{
    QAbstractItemModel* pModel = p_Parent->GetApplication()->GetTableStore()->GetTableModel(sTable);
    QCompleter* pCompeter = new QCompleter(pModel,this);
    pCompeter->setMaxVisibleItems(10);
    pCompeter->setCompletionMode(QCompleter::PopupCompletion);
    pCompeter->setCompletionColumn(iIndex);
    //pCompeter->setModel(pModel);
    pLineEdit->setCompleter(pCompeter);
}

QString GenericCtrl::GetPropertyName(QWidget *pWidget)
{
    return  pWidget->property(FIELD).toString();
}
void GenericCtrl::SetFieldProperty(QWidget* pWidget, QString sFieldName)
{
    if(!pWidget->property(FIELD).toString().isEmpty())
    {
        Q_ASSERT(false);//You are resetting a field value
    }
    pWidget->setProperty(FIELD, sFieldName);
}

void GenericCtrl::SetEnumProperty(QWidget *pWidget, QString sEnumName)
{
    pWidget->setProperty(ENUM_NAME, sEnumName);
}

//template<class T>
//void GenericCtrl::AdjustWidgetWidthToSame(QWidget *pWidget)
//{

//}
 void GenericCtrl::FillComboWithEnum(QComboBox* pComboBox, QString sEnum)
{
     Enamuration* pEnum = p_Parent->GetApplication()->GetEnumHandler()->GetEnum(sEnum);
     if(!pEnum)
         return;

     QMap<QString,int> mapEnumData = pEnum->GetDataValuePair();
     for(auto itr = mapEnumData.begin(); itr != mapEnumData.end(); itr++)
     {
         pComboBox->addItem(itr.key(),itr.value());
     }
 }

 void GenericCtrl::AddComboDataWithTooltip(QComboBox *pComboBox, QString sValue)
 {
     pComboBox->addItem(sValue);
     int iIndex = pComboBox->findText(sValue);

     pComboBox->setItemData(iIndex, sValue, Qt::ToolTipRole);
 }

 void GenericCtrl::SelectEnumValue(QComboBox *pComboBox, int ienum)
 {
     int iIndex = pComboBox->findData(ienum);
     if(iIndex != -1)
         pComboBox->setCurrentIndex(iIndex);
 }
