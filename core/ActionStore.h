/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef ACTIONSTORE_H
#define ACTIONSTORE_H
#include <QMap>
#include <QAction>
#include <QIcon>
class ActionStore
{
public:
    ActionStore();
    void AddAction(QString sName, QAction* pAct);
    QAction* GetAction(QString sName);
    QMap<QString,QAction *>  GetAllActions();
    QIcon GetIcon(QString sName);
private:
    QMap<QString,QAction*> map_Actions;
};

#endif // ACTIONSTORE_H
