#include "GenericWnd.h"
#include <MainWnd.h>
#include <QDebug>
#include <GenericCtrl.h>
#include <QTimer>
GenericWnd::GenericWnd(MainWnd *parent,QString sTitle )
    : QWidget(),s_Title(sTitle)
{
    _p_MDISubWindow = NULL;
	setContextMenuPolicy(Qt::ActionsContextMenu);
	_p_DockWidget = new QDockWidget(sTitle,parent);
    setWindowTitle(sTitle);
	p_MainWnd = parent;
	
    s_Title = sTitle;
	setAttribute(Qt::WA_DeleteOnClose);
	p_ActDock = new QAction("Dock",this);
	p_ActMDI = new QAction("MDI",this);
	connect(p_ActDock, SIGNAL(triggered(bool )),this, SLOT(OnDock()));
	connect(p_ActMDI, SIGNAL(triggered(bool )),this, SLOT(OnMDI()));
    //addAction(p_ActDock);
    //addAction(p_ActMDI);
	p_ActMDI->setEnabled(false);
    b_Initialized = false;
    i_WndID = -1;
}

GenericWnd::~GenericWnd()
{


    //delete _p_MDISubWindow;
    delete _p_DockWidget;
    delete p_ActDock;
    delete p_ActMDI;
   p_Application->OnWindowDistroyed(this);
   p_MainWnd->_RemoveSubWindow(this);
}
void GenericWnd::OnDock(Qt::DockWidgetArea eDocArea)
{
    if(_p_MDISubWindow)
    p_MainWnd->_RemoveSubWindow(this);
	
	delete _p_MDISubWindow;
	_p_MDISubWindow = NULL;
	_p_DockWidget->setWidget(this);
    //_p_DockWidget->setMinimumSize(this->minimumSize());
    p_MainWnd->addDockWidget(eDocArea, _p_DockWidget);

	_p_DockWidget->show();
	p_ActDock->setEnabled(false);
	p_ActMDI->setEnabled(true);
}

void GenericWnd::OnMDI()
{
	
	p_MainWnd->removeDockWidget(_p_DockWidget);

	_p_MDISubWindow = p_MainWnd->_AddSubWindow(this);
	_p_MDISubWindow->show();
	p_ActDock->setEnabled(true);
    p_ActMDI->setEnabled(false);
}

void GenericWnd::closeEvent(QCloseEvent *event)
{
    QSettings * pWndowSettings = p_Application->GetWindowSettings();
    pWndowSettings->setValue(s_Title,saveGeometry());
//    pWndowSettings->setValue(s_Title+"_1", saveGeometry();
    QWidget::closeEvent(event);

}
bool GenericWnd::isInitialized() const
{
    return b_Initialized;
}

void GenericWnd::Initialize()
{
    b_Initialized = true;
    _p_MDISubWindow = p_MainWnd->_AddSubWindow(this);
}

void GenericWnd::ReSize(int iWidth, int iHeight)
{
    if(!_p_MDISubWindow)
        return;

    _p_MDISubWindow->resize(iWidth, iHeight);
}

void GenericWnd::Raise()
{
    if(!_p_MDISubWindow)
        return;

    _p_MDISubWindow->mdiArea()->setActiveSubWindow(_p_MDISubWindow);

}

void GenericWnd::SetWindowFlags(Qt::WindowFlags flags)
{
    if(!_p_MDISubWindow)
        return;
    _p_MDISubWindow->setWindowFlags(flags);
}

void GenericWnd::SetWindowModality(Qt::WindowModality windowModality)
{
    if(!_p_MDISubWindow)
        return;

    _p_MDISubWindow->setWindowModality(windowModality);
}

void GenericWnd::OnPreCreate(Application *pApplication)
{
    p_Application = pApplication;
    QSettings* pWndSett = p_Application->GetWindowSettings();
    if(pWndSett->contains(s_Title))
    {

        if(restoreGeometry(pWndSett->value(s_Title).toByteArray()))
        {
            p_Application->GetLogger()->WriteLog("Profile applied successfully");
        }else
        {
            p_Application->GetLogger()->WriteLog("Unable to apply profile");
        }
    }
    QList<GenericCtrl*> lstChildren = findChildren<GenericCtrl*>();
    foreach(GenericCtrl* pChild, lstChildren)
    {
        pChild->OnPreCreateWnd(this);
        pChild->OnCreateCtrl();
        pChild->OnPostCreateCtrl();
    }
    QIcon oIcon = pApplication->GetActionStore()->GetIcon(s_Title);

    setWindowIcon(oIcon);

}

void GenericWnd::OnNewChild(GenericCtrl *pChild)
{
    //Under normal initilization all the children of this control
    //will be handed by the window, but during forced include shouldbe
    //explicitly handled.
    QList<GenericCtrl*> lstChildren = pChild->findChildren<GenericCtrl*>();
    foreach(GenericCtrl* pGChild, lstChildren)
    {
        pGChild->OnPreCreateWnd(this);
        pGChild->OnCreateCtrl();
        pGChild->OnPostCreateCtrl();
    }
    pChild->OnPreCreateWnd(this);
    pChild->OnCreateCtrl();
    pChild->OnPostCreateCtrl();
}

void GenericWnd::DeleteWindow()
{
    p_MainWnd->removeDockWidget(_p_DockWidget);
    p_MainWnd->_RemoveSubWindow(this);
    deleteLater();
    delete _p_MDISubWindow;
   _p_MDISubWindow = nullptr;
}

void GenericWnd::RemoveWindow()
{
    QTimer::singleShot(100,this,SLOT(DeleteWindow()));

}

void GenericWnd::DistributeKeytoChildren(QString sKey)
{
    QList<GenericCtrl*> lstChildren = findChildren<GenericCtrl*>();
    foreach(GenericCtrl* pChild, lstChildren)
    {
        pChild->OnKey(sKey);
    }
}

WindowConfig GenericWnd::GetWindowTableConfiguration()
{
   return GetApplication()->GetTableConfigHander()->GetWindowConfig(s_Title);
}
