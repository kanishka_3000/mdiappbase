#include "Connection.h"
#include <QDebug>
#include <QSqlQuery>
#include <Application.h>
#include <EntityFactory.h>
#include <QSqlRecord>
#include <QSqlField>
#include <Logger.h>
#include <QSqlIndex>
#include <QSqlError>
#include <iostream>
Connection* Connection::p_Connection;
int Connection::i_LastID = -1;
//QSqlDatabase Connection::db;

void Connection::CreateInstance()
{
     p_Connection = new Connection();

}

bool Connection::Initialize(QString sLocation, QString sSecondaryLocation, bool bReplicate, ConnectionLogger* pLogger)
{
    b_ReplicateDB = bReplicate;
    p_ConnectionLogger = pLogger;
    db_Primary = QSqlDatabase::addDatabase("QSQLITE","Primary");
    sLocation += QString(QDir::separator()) += "Application";

    db_Primary.setDatabaseName(sLocation);
    bool db_ok = db_Primary.open();
    std::cout << "DB Open" << db_ok << qPrintable(sLocation) << std::endl;
    bool db_Rep_Ok = true;
    if(b_ReplicateDB)
    {
        db_Replica = QSqlDatabase::addDatabase("QSQLITE","Secondary");
        sSecondaryLocation+="/Application";
        db_Replica.setDatabaseName(sSecondaryLocation);
        bool db_Rep_Ok = db_Replica.open();
        qDebug() << "Rep DB open" << db_Rep_Ok;
    }

    return (db_ok && db_Rep_Ok);
}

bool Connection::Initialize(Connection::ConnectionInfo oConnectionInfo, ConnectionLogger *pLogger)
{
     p_ConnectionLogger = pLogger;
     db_Primary = QSqlDatabase::addDatabase("QMYSQL");
     db_Primary.setHostName(oConnectionInfo.s_Server);
     db_Primary.setDatabaseName(oConnectionInfo.s_DBName);
     db_Primary.setUserName(oConnectionInfo.s_UserName);
     db_Primary.setPassword(oConnectionInfo.s_Password);
     bool bOpen = db_Primary.open();
     if(!bOpen)
     {
         if(pLogger)
             pLogger->PrintOutput(db_Primary.lastError().text());
         qDebug() << db_Primary.lastError();
     }
     return bOpen;
}

void Connection::CloseDB()
{
    //db.removeDatabase("QSQLITE");
    //db.close();
}

Connection::~Connection()
{

    db_Primary.removeDatabase("QSQLITE");
    db_Primary.close();

    if(b_ReplicateDB)
    {
        db_Replica.removeDatabase("QSQLITE");
        db_Replica.close();
    }
qDebug() << "DB closed";
    //db.close();

}

int Connection::RunQuery(QString sQuery)
{
    if(p_ConnectionLogger)
        p_ConnectionLogger->PrintOutput(sQuery);

    QSqlQuery oQuery(sQuery,db_Primary);

    if(oQuery.lastError().text() != QString(" "))
    {
        if(p_ConnectionLogger)
            p_ConnectionLogger->PrintOutput(oQuery.lastError().text());

        qDebug() << oQuery.lastError().text();
        return -1;
    }
    QVariant oReturn = oQuery.lastInsertId();
    i_LastID = oReturn.toInt();
    if(b_ReplicateDB)
    {
        QSqlQuery oQueryR(sQuery,db_Replica);
        //oQueryR.exec();
    }
    return i_LastID;
}

void Connection::GerRecords(QString sQuery, ConnectionDataHander &rDataHander)
{
   // bool db_ok = db.open();
   if(p_ConnectionLogger)
       p_ConnectionLogger->PrintOutput(sQuery);

    QSqlQuery oQuery(sQuery,db_Primary);

    while (oQuery.next())
    {
        rDataHander.CreateNewItem();

        QList<QString> sFileds = rDataHander.GetFieldList();
        foreach(QString sField,sFileds)
        {
             int fieldNo = oQuery.record().indexOf(sField);
             QVariant oData = oQuery.record().value(fieldNo);
             rDataHander.AddDataColumn(sField, oData);
        }

    }
    // db.close();
}

void Connection::GetSummery(QString sQuery, QVariant &oData)
{
    if(p_ConnectionLogger)
        p_ConnectionLogger->PrintOutput(sQuery);

     QSqlQuery oQuery(sQuery,db_Primary);

     if (oQuery.next())
     {

         oData = oQuery.record().value(0);
     }
}

int Connection::UpdateQuery(QString sTableName, QMap<QString, QVariant> mapData, QString sKeyField, QString sValue)
{
    if(mapData.size() < 1)
        return INVALID_UPDATE;

    QSqlRecord oRecorLogi = db_Primary.record(sTableName);
    QString sQuery= QString("update %1 set  ").arg(sTableName);
    int iSize = mapData.size();
    int i = 1;
    for(QMap<QString,QVariant>::iterator itr = mapData.begin(); itr != mapData.end(); itr++,i++)
    {
        QString sField = itr.key();
        QVariant oData = itr.value();
        QSqlField oField = oRecorLogi.field(sField);
        QVariant::Type enType = oField.type();
        sQuery += sField ;
        sQuery+= " = ";
        if(enType == QVariant::String)
        {
            sQuery+=" '";
        }
        sQuery+= oData.toString();
        if(enType == QVariant::String)
        {
            sQuery+="' ";
        }
        if(i != iSize)
        {
            sQuery+= " , ";
        }
    }
    if(sKeyField != QString(""))
    {

        sQuery += QString(" where %1 = '%2'").arg(sKeyField).arg(sValue);
    }
    return RunQuery(sQuery);
}

int Connection::InsertQuery(QString sTableName, QMap<QString, QVariant> mapData)
{
    if(mapData.size() < 1)
        return -1;

    QSqlRecord oRecorLogi = db_Primary.record(sTableName);
    QString sQuery= QString("insert into %1  ").arg(sTableName);
    QStringList sColumns;
    QStringList sValues;
    for(QMap<QString,QVariant>::iterator itr = mapData.begin(); itr != mapData.end(); itr++)
    {
        QString sField = itr.key();
        QVariant oData = itr.value();
        QSqlField oField = oRecorLogi.field(sField);
        QVariant::Type enType = oField.type();
        QString sValue;

        if(enType == QVariant::String)
        {
            sValue+=" '";
        }
        sValue+= oData.toString();
        if(enType == QVariant::String)
        {
            sValue+="' ";
        }
        sColumns.push_back(sField);
        sValues.push_back(sValue);
    }
    sQuery+= QString(" ( ") += sColumns.join(",")+= QString(" )");
    sQuery+= "values";
    sQuery+=QString("( ") += sValues.join(",")+=" ) ";
#ifdef _LOG_SQL_
    qDebug() << sQuery;
#endif

    return RunQuery(sQuery);
}

QString Connection::AppendDataToQuery(QString sFieldName, QVariant oValue, QString sTableName)
{
    QSqlRecord oRecord = db_Primary.record(sTableName);
    QSqlField oField = oRecord.field(sFieldName);
    QVariant::Type enType = oField.type();
    QString sValue;

    if(enType == QVariant::String)
    {
        sValue+=" '";
    }
    sValue+= oValue.toString();
    if(enType == QVariant::String)
    {
        sValue+="' ";
    }

    return sValue;
}

bool Connection::DoesDBFieldExists(QString sTableName, QString sFieldName, QVariant oValue)
{
    QString sQuery = QString("select count(*)  from %1 where %2 = ").arg(sTableName).arg(sFieldName);
    QString sValue = AppendDataToQuery(sFieldName, oValue, sTableName);
    sQuery+=sValue;
    QSqlQuery oQuery(sQuery,db_Primary);
    oQuery.first();

    int iPrimarySize = oQuery.value(0).toInt();

     return (iPrimarySize > 0);
}

QStringList Connection::GetTableColumns(QString sTableName)
{
    QSqlRecord oNames = db_Primary.record(sTableName);
    QStringList oList;
    int iCount = oNames.count();
    for(int i = 0 ;i < iCount; i++)
    {
        QString sField;
        QSqlField oField = oNames.field(i);
        oList.push_back(oField.name());
    }
    return oList;
}

QStringList Connection::GetTables()
{
    return db_Primary.tables();
}

void Connection::EmptyTable(QString sTableName)
{
    QString sQuery = QString("delete from %1").arg(sTableName);
    RunQuery(sQuery);
}

void Connection::DeleteRow(QString sTable, QString sKey, QString sValue)
{
    QString sQuery = QString("delete from %1 where %2 = ").arg(sTable).arg(sKey);
    sQuery += AppendDataToQuery(sKey,QVariant(sValue),sTable);
    RunQuery(sQuery);
}

bool Connection::VerifyDBs()
{
    if(!b_ReplicateDB)
        return false;

   QStringList sPrimaryTables =  db_Primary.tables();
   QStringList sSecondaryTables = db_Replica.tables();
   foreach(QString sTable, sPrimaryTables)
   {
       if(!sSecondaryTables.contains(sTable))
       {
           QString sLog = QString("Secondary does not contain table %1").arg(sTable);
           Application::GetApplication()->GetLogger()->WriteLog(sLog);
           return false;
       }
       QString sQuery = QString("select count(*) from %1").arg(sTable);
       QSqlQuery oPrimaryRecords = QSqlQuery(sQuery,db_Primary);
       QSqlQuery oSecondaryRecords = QSqlQuery(sQuery,db_Replica);
       oPrimaryRecords.exec();
       oSecondaryRecords.exec();
       oPrimaryRecords.first();
       oSecondaryRecords.first();
       int iPrimarySize = oPrimaryRecords.value(0).toInt();
       int iSecondarySize = oSecondaryRecords.value(0).toInt();
       if(iPrimarySize != iSecondarySize)
       {
           QString sLog = QString("Secondary does not match in count to primary %1").arg(sTable);
           Application::GetApplication()->GetLogger()->WriteLog(sLog);
           return false;
       }
       else
       {
           QString sLog = QString("primary and secndary contains %1 for table %2").arg(oPrimaryRecords.value(0).toInt()).arg(sTable);
           Application::GetApplication()->GetLogger()->WriteLog(sLog);
       }
   }

   return true;
}

Connection::Connection()
{
     b_ReplicateDB = false;
     p_ConnectionLogger = nullptr;
}

