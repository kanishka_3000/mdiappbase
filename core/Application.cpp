#include "Application.h"
#include <LoginWnd.h>
#include <MainWnd.h>
#include <QSettings>
#include <QDebug>
#include <globaldefs.h>
#include <ChangePaswdWnd.h>
#include <User.h>
#include <PrefereneWnd.h>

#include <ManageUsersWnd.h>
#include <QMessageBox>
#include <QtGlobal>
#include <ObjectFactory.h>
#include <QuickLaunch.h>
#include <QTimer>
#include <TableStore.h>
#include <NotificationWnd.h>
#include <iostream>
#include <assert.h>


Application* Application::p_App = NULL;

Application::Application(QObject *parent) : QObject(parent),
    o_TableStore(this)
{
    p_LoginWindow = NULL;
    p_MainWnd = NULL;

    p_Settings = NULL;
    p_WindowSettings = NULL;
    p_Logger = NULL;
    p_App = this;
    p_User = NULL;
    p_ObjectFactory = NULL;
    p_ActionStore = NULL;
    p_DataSetupManager = nullptr;
    p_LockFile = nullptr;
}

Application::~Application()
{
    delete p_Settings;
    delete p_WindowSettings;
    delete p_ActionStore;
    delete p_Factory;
	for(auto itr = map_WndCreator.begin(); itr != map_WndCreator.end(); itr++)
	{
		BaseWindowCreator* pCr = itr.value();
		delete pCr;
	}
}

void Application::OpenLoginWnd()
{
    p_LoginWindow = new LoginWnd(this);
    if(!GetLoginIcon().isEmpty())
    {
        p_LoginWindow->setWindowIcon(QIcon(GetLoginIcon()));
    }
    p_LoginWindow->show();
}
ObjectFactory *Application::GetObjectFactory() const
{
    return p_ObjectFactory;
}


void Application::CreateSetupManager()
{
    p_DataSetupManager = p_ObjectFactory->GetSetupManager();
    p_DataSetupManager->Setup(Connection::GetInstance(), p_Logger);
}

void Application::ResetData()
{

    p_DataSetupManager->ResetData();
}

QString Application::GetApplicationName()
{
    return "Application";
}

QString Application::GetVersion()
{
    return "v0.1.1";
}


void Application::Initialize(QApplication *pQtApplication,ObjectFactory* pFactory)
{
    std::cout << qPrintable(QDir::currentPath()) << std::endl;
    QString sCurrentApplicationPath = QDir::currentPath();
    p_QtApplication = pQtApplication;
    p_ObjectFactory = pFactory;
    QString sConfigPath  = sCurrentApplicationPath + QString(QDir::separator()) + "ApplicationSettings.cfg";
    if(!QFile(sConfigPath).exists())
    {
        QMessageBox::warning(NULL,tr("FATAL"),tr("Application config does not exist"));
        return;
    }
    p_Settings = new QSettings(sConfigPath, QSettings::IniFormat);

    p_WindowSettings = new QSettings("WindowSettings.dat",QSettings::IniFormat);
    p_ActionStore = new ActionStore();
    o_EnumHandler.FillInitialData();
    FillCustomEnums(o_EnumHandler);
    p_Factory = p_ObjectFactory->CreateEntityFactory();
    QString sDBLocation = p_Settings->value(DB_LOCATION).toString();
    sDBLocation = sCurrentApplicationPath;
    QString sDBRepLocation = p_Settings->value(DB_REP_LOCTION).toString();
    bool bReplicateDB = p_Settings->value(REP_DB).toBool();
    QString sLogLocation = sCurrentApplicationPath += QString(QDir::separator()) += p_Settings->value(LOG_LOCATION).toString();

    p_Logger = new Logger(&sLogLocation);
    if(!p_Logger->OpenLogFile())
    {
        std::cout << "Unable to open log file" << qPrintable(sLogLocation) << std::endl;
        return;
    }
    p_LockFile = new QLockFile("App.lock");
    if(!p_LockFile->tryLock(0))
    {
        QMessageBox::information(NULL,tr("Muliple"),tr("Only One Instance is Allowed"));
        exit(0);
    }

    p_Logger->WriteLog("Application initialized");

    p_Logger->ClearLogs(LOG_FILE_DURATION);
    Connection::CreateInstance();
    SQLLogger*  pSQLLogger = new SQLLogger(p_Logger);
    bool bIsStandAlone = GetConfigData(STANDALONE).toBool();
    bool bDbOpenSuccess = false;
    if(bIsStandAlone)
    {
        bDbOpenSuccess = Connection::GetInstance()->Initialize(sDBLocation,sDBRepLocation,bReplicateDB, pSQLLogger);
    }
    else
    {
        Connection::ConnectionInfo oConnectionInfo;
        oConnectionInfo.s_Server = GetConfigData(SERVER).toString();
        oConnectionInfo.s_DBName =   DB_NAME;
        oConnectionInfo.s_UserName =  GetConfigData(DBUSERNAME).toString();
        oConnectionInfo.s_Password =  GetConfigData(DBPASSWORD).toString();
        bDbOpenSuccess = Connection::GetInstance()->Initialize(oConnectionInfo, pSQLLogger);
    }
    if(bDbOpenSuccess)
    {
        p_Logger->WriteLog("Database opened sucessfully");
    }
    else
    {
        p_Logger->WriteLog("!!!!! Database open failure");
        QMessageBox::information(NULL,"Error","Database open failure");
        return;
    }

    if(bReplicateDB)
    {
        if(!Connection::GetInstance()->VerifyDBs())
        {
            QMessageBox::information(NULL,"Error","Database verification failure");
            p_Logger->WriteLog("Databases do not match");
            return;
        }
    }
    p_Factory->Initialize();
    o_TableConfig.SetLogger(p_Logger);
    o_TableConfig.Initialize("TableConfig.xml");
    //Authentication
    QString sStyle = p_Settings->value(STYLE_SHEET).toString();
    SetStyleSheet(sStyle);

    OpenLoginWnd();
    i_MaxWndInstances = p_Settings->value(MAX_WND_INST).toInt();
    if(i_MaxWndInstances < 1)//UX check available, defensive
    {
        i_MaxWndInstances = 1;
    }

}

GenericWnd* Application::CreateWnd(int iWndID)
{
    QString sLog = QString("Window opened %1").arg(iWndID);
    p_Logger->WriteLog(sLog);
    GenericWnd* pWndow = NULL;
    switch(iWndID)
    {
    case WND_CH_PW:
    {
        ChangePaswdWnd* pWnd = new ChangePaswdWnd(p_MainWnd);
        pWnd->show();
        return NULL;
    }
    }
    BaseWindowCreator* pCr = map_WndCreator.value(iWndID);
    pWndow = pCr->Create(p_MainWnd);
    assert(pWndow);


    if((!pWndow->isInitialized()))
    {
        pWndow->i_WndID = iWndID;
        pWndow->OnPreCreate(this);
        pWndow->Initialize();


        pWndow->OnCreate();

    }
    pWndow->show();

    pWndow->Raise();
    return pWndow;
}

void Application::ChangePassword(QString sNewPassword)
{
    p_User->ChangePassword(sNewPassword);
}

QString Application::GetUserName()
{
    return p_User->GetUserName();
}

void Application::AddDef(QString sDef, EntityDef *pDef)
{
    p_Factory->AddDef(sDef,pDef);
}

void Application::SetStyleSheet(QString sFileName)
{
    if(sFileName.isEmpty())
        return;

    QString sPath = QString("style") + QDir::separator()+sFileName;
    QFile oFile(sPath);
    oFile.open(QIODevice::ReadOnly);
    QString sStyleData = oFile.readAll();
    GetQApplication()->setStyleSheet(sStyleData);
}

QVariant Application::GetConfigData(QString sKey)
{

    Q_ASSERT(p_Settings->contains(sKey));

    return p_Settings->value(sKey);
}

QSettings *Application::GetWindowSettings()
{
    return p_WindowSettings;
}

void Application::OnWindowDistroyed(GenericWnd *pWnd)
{
    int iID = pWnd->i_WndID;
    BaseWindowCreator* pCr = map_WndCreator.value(iID);
    pCr->Remove(pWnd);
}

void Application::RegisterWindows()
{

    SetWindowHandler<ManageUsersWnd>(WND_MG_USERS, "Manage Users", i_MaxWndInstances);
    SetWindowHandler<PrefereneWnd>(WND_PREF, "Preferences", i_MaxWndInstances);
    SetWindowHandler<QuickLaunch>(WND_QLAUNCH, "Quick Launch", i_MaxWndInstances);
    SetWindowHandler<NotificationWnd>(WND_NOTIFICATIONS, "Appointments", i_MaxWndInstances);
}

void Application::StartApplication()
{

    RegisterWindows();
    CreateSetupManager();
    SetupUserDataTables();
    p_MainWnd = p_ObjectFactory->CreateMainWnd();
    p_MainWnd->Initilize(this);
    //QString sTitle = GetConfigData("AppName").toString();
    //p_MainWnd->setWindowTitle(sTitle);
    p_MainWnd->show();

    QTimer::singleShot(100,this,SLOT(OpenDefaultWindows()));
    emit NotifyApplicationReady();
}



void Application::OnLoginSuccess(std::shared_ptr<User> pUser)
{
    p_User = pUser;
    GetLogger()->WriteLog("User %s logged in sucessfully",qPrintable(p_User->GetUserName()));
    StartApplication();
    delete p_LoginWindow;
}

void Application::OpenDefaultWindows()
{
    if(p_Settings->value(SHOW_QL).toBool())
    {
        GenericWnd* pWnd = CreateWnd(WND_QLAUNCH);
        CustomizeDefaultWindows(pWnd,WND_QLAUNCH);
        pWnd->OnDock();
    }
    if(p_Settings->value(CALENDER).toBool())
    {
        GenericWnd* pWnd = CreateWnd(WND_NOTIFICATIONS);
        CustomizeDefaultWindows(pWnd, WND_NOTIFICATIONS );
        pWnd->OnDock(Qt::RightDockWidgetArea);
    }


}



SQLLogger::SQLLogger(Logger *pLogger)
{
    p_Logger = pLogger;
}

void SQLLogger::PrintOutput(QString sData)
{
    QString sLog = QString("[SQL] ") + sData;
    p_Logger->WriteLog(sLog);
}
