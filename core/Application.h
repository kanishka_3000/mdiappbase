/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QtWidgets/QApplication>
#include <QSettings>
#include <Connection.h>
#include <Logger.h>
#include <EntityFactory.h>
#include <TableConfigHandler.h>
#include <memory>
#include <ActionStore.h>
#include <EnumHandler.h>
#include <TableStore.h>
#include <DataSetupManager.h>
#include <QLockFile>
class LoginWnd;
class MainWnd;
class User;
class TableConfigHandler;
class GenericWnd;
class ObjectFactory;
class BaseWindowCreator
{
public:
	virtual ~BaseWindowCreator(){}
    virtual GenericWnd* Create(MainWnd* pWnd) = 0;
    virtual void Remove(GenericWnd* pWnd) = 0;
    QString s_WndName;
};

template<class T>
class WindowCreator:public BaseWindowCreator
{
public:
    WindowCreator(int iWndCount):i_MaxWndCount(iWndCount),i_CurrentWndCount(0){}
     virtual GenericWnd* Create(MainWnd* pWnd)
    {

        if(i_MaxWndCount == i_CurrentWndCount)
        {
            return vec_Wnds.last();
        }
        i_CurrentWndCount++;
        GenericWnd* pGenWnd = new T(pWnd,s_WndName);
        vec_Wnds.push_back(pGenWnd);
        return pGenWnd;
    }
    virtual void Remove(GenericWnd* pWnd)
    {
       vec_Wnds.removeAll(pWnd);
       i_CurrentWndCount--;
    }

private:
    QVector<GenericWnd*> vec_Wnds;

    int i_MaxWndCount;
    int i_CurrentWndCount;
};
class SQLLogger: public Connection::ConnectionLogger
{
public :
     SQLLogger(Logger* pLogger);
    void PrintOutput(QString sData);
private:
    Logger* p_Logger;
};

class Application : public QObject
{
    Q_OBJECT
public:
    explicit Application(QObject *parent = 0);
    virtual ~Application();
    virtual void Initialize(QApplication* pQtApplication, ObjectFactory *pFactory);

    virtual GenericWnd* CreateWnd(int iWndID);

    template<class T>
    T* CreateWndEx(int iWndID)
    {
        return dynamic_cast<T*>(CreateWnd(iWndID));
    }

    EntityFactory* GetEntityFactory(){return p_Factory;}
    virtual void ChangePassword(QString sNewPassword);
    static Application* GetApplication(){return p_App;}
    std::shared_ptr<User> GetUser(){return p_User;}
    QString GetUserName();
    virtual void AddDef(QString sDef, EntityDef* pDef);
    void SetStyleSheet(QString sFileName);

    QVariant GetConfigData(QString sKey);
    QSettings* GetConfig(){return p_Settings;}
    QSettings* GetWindowSettings();
    EnumHandler* GetEnumHandler(){return &o_EnumHandler;}
    Logger* GetLogger(){return p_Logger;}
    TableConfigHandler* GetTableConfigHander(){return &o_TableConfig;}
    ActionStore* GetActionStore(){return p_ActionStore;}
    TableStore* GetTableStore(){return &o_TableStore;}
    template<class T>
     void SetWindowHandler(int iWndID,QString sWndName, int iWndCount = MAX_WND_COUNT)
    {
        WindowCreator<T>* pCr = new WindowCreator<T>(iWndCount);
        pCr->s_WndName = sWndName;
        map_WndCreator.insert(iWndID,pCr);
    }
    QApplication* GetQApplication(){return p_QtApplication;}
    void OnWindowDistroyed(GenericWnd* pWnd);
    ObjectFactory *GetObjectFactory() const;
    void ResetData();
    virtual QString GetLoginIcon(){return "";}
    virtual QString GetApplicationName();
    virtual QString GetVersion();
    virtual void FillCustomEnums(EnumHandler& rEnumHander){(void)rEnumHander;}
    static Application* p_App;
    LoginWnd* p_LoginWindow;

    void CreateSetupManager();
protected:
    virtual void StartApplication();
    virtual void RegisterWindows();
    virtual void CustomizeDefaultWindows(GenericWnd* pWnd, int iType){(void)pWnd;(void)iType;}
     virtual void OpenLoginWnd();
    virtual void SetupUserDataTables(){}

    MainWnd* p_MainWnd;
    Connection* p_Connection;
    Logger* p_Logger;
    EntityFactory* p_Factory;
    TableConfigHandler o_TableConfig;
    ObjectFactory* p_ObjectFactory;
    ActionStore* p_ActionStore;
    EnumHandler o_EnumHandler;
    TableStore o_TableStore;
    int i_MaxWndInstances;
    DataSetupManager* p_DataSetupManager;
private:
    QSettings* p_Settings;
    QSettings* p_WindowSettings;
    std::shared_ptr<User> p_User;
    QMap<int,BaseWindowCreator*> map_WndCreator;
    QApplication *p_QtApplication;
    QLockFile* p_LockFile;
signals:
    void NotifyApplicationReady();
public slots:
    void OnLoginSuccess(std::shared_ptr<User> pUser);
    virtual void OpenDefaultWindows();
};

#endif // APPLICATION_H
