/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef GENERICWND_H
#define GENERICWND_H

#include <QMdiSubWindow>
#include <QDockWidget>
#include <QAction>
#include <Application.h>
#include <TableConfigHandler.h>
class MainWnd;
class GenericCtrl;
class GenericWnd : public QWidget/*,public Ui_GenericWnd*/
{
	Q_OBJECT

public:
	GenericWnd(MainWnd *parent,QString sTitle );
	virtual ~GenericWnd();
	virtual void Initialize();

    void ReSize(int iWidth, int iHeight);
    void Raise();
    void SetWindowFlags(Qt::WindowFlags flags);
    void SetWindowModality(Qt::WindowModality windowModality);
    //Called on every window to intializae and set the application
    //Calls precreate on all sub controls
    //Should call the parent function if overridden
    virtual void OnPreCreate(Application* pApplication);
    //Will be called after intilization// Expected to be overridden
    virtual void OnCreate(){}
    //Generel delete function for the window.

    virtual void OnNewChild(GenericCtrl* pChild);
    virtual void OnChildRemoveRequest(GenericCtrl* pChild){(void)pChild;}
    virtual void RemoveWindow();

    void DistributeKeytoChildren(QString sKey);
    Application* GetApplication(){return p_Application;}

    WindowConfig GetWindowTableConfiguration();
    QString s_Title;
    bool isInitialized() const;
    int i_WndID;

public slots:
    void OnDock(Qt::DockWidgetArea eDocArea =Qt::LeftDockWidgetArea);
		void OnMDI();
        void closeEvent(QCloseEvent *event);
         void DeleteWindow();
protected:
	
	MainWnd* p_MainWnd;

    Application* p_Application;
private:
	QMdiSubWindow* _p_MDISubWindow;
	QDockWidget* _p_DockWidget;
	QAction*	p_ActDock;
	QAction*	p_ActMDI;
    bool b_Initialized;
};

#endif // GENERICWND_H
