/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/
#ifndef GENERICCTRL_H
#define GENERICCTRL_H

#include <QWidget>
#include <QMap>
#include <QString>
#include <QVariant>
#include <entity/Entity.h>
#include <QLineEdit>
#include <QDateEdit>
#include <QTextEdit>
#include <QComboBox>
#include <Logger.h>
class GenericWnd;
class GenericCtrl : public QWidget
{
    Q_OBJECT
public:
    explicit GenericCtrl(QWidget *parent = 0);

    //Not exepcted to be overridden, general intilization
    void OnPreCreateWnd(GenericWnd* pParent);
    //Override to be called during window initilization
    virtual void OnCreateCtrl(){}

    virtual void OnPostCreateCtrl(){}

    virtual void OnKey(QString sKey){(void)sKey;}
    void WriteLog(const char* zfmt, ...);
signals:

public slots:

    //Clears all line edits
    virtual void ClearFields();
protected:
    QMap<QString, QVariant> CollectFields();

    //Stamp current date to all fields
    virtual void SetCurrentDateToAllFields();
    //checks entries for empty input controls//needs to update with new controls
    virtual bool BasicEntryValidation(QString &sError);

    QString GetWidgetText(QWidget* pWidget);
    void SetWidgetText(QWidget* pWidget, QString sText);

    bool IsTextWidget(QWidget* pWidget);
    QVariant GetComboboxDataFromModel(QWidget* pWidget, int iIndex);
    void SetComboCompleter(QComboBox* pComboBox);
    void SetLineCompleter(QLineEdit* pLineEdit, QString sTable, int iIndex = 0);
    QString GetPropertyName(QWidget* pWidget);

     void SetFieldProperty(QWidget* pWidget, QString sFieldName);
     void SetEnumProperty(QWidget* pWidget, QString sEnumName);
    virtual void FillComboWithEnum(QComboBox* pComboBox, QString sEnum);
    void AddComboDataWithTooltip(QComboBox* pComboBox, QString sValue);
    void SelectEnumValue(QComboBox* pComboBox, int ienum);
    template<class T>
    void AdjustWidgetWidthToSame(QWidget* pWidget)
    {
        int iMaxWidth = 0;
        QList<T*> lstChildren = pWidget->findChildren<T*>();
        foreach(T* child, lstChildren)
        {
            int iWidth = child->width();
            if(iWidth > iMaxWidth)
                iMaxWidth = iWidth;
        }

        foreach(T* child, lstChildren)
        {
            int iHeight = child->height();
            QSize oSize(iMaxWidth, iHeight);
            child->setMinimumWidth(iMaxWidth);
        }
    }



    GenericWnd* p_Parent;
    Logger* p_Logger;
};

#endif // GENERICCTRL_H
