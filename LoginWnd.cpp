#include "LoginWnd.h"
#include "ui_LoginWnd.h"
#include <Login.h>
LoginWnd::LoginWnd(Application *pApp, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginWnd)
{
    ui->setupUi(this);
    connect(ui->btn_Login,SIGNAL(clicked(bool)),this,SLOT(OnSubmit()));
    connect(this,SIGNAL(NotifyLoginSucess(std::shared_ptr<User>)),pApp,SLOT(OnLoginSuccess(std::shared_ptr<User>)));
}

LoginWnd::~LoginWnd()
{
    delete ui;
}

void LoginWnd::OnLogin(QString sPassword, QString sUsername)
{
    std::shared_ptr<User> pUser = NULL;
    Login oLogin;
    bool bResult = oLogin.Authenticate(sUsername,sPassword,pUser);

    if(bResult)
    {
        emit NotifyLoginSucess(pUser);
    }
    else
    {
        ui->lbl_Notice->setText("Invalid username or password");
    }
}

void LoginWnd::OnSubmit()
{
    QString sUsername = ui->txt_Username->text();
    QString sPassword = ui->txt_Password->text();

    OnLogin(sPassword, sUsername);
}
